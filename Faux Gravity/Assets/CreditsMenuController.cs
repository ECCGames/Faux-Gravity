﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsMenuController : MonoBehaviour
{
	[SerializeField] private GameObject _menuCanvas;
	[SerializeField] private FireworkSpawner _fireworks;

	private bool _isActive;
	private PlayerInput _playerInput;
	private AudioSource _source;
	
	private void Awake()
	{
		_playerInput = GetComponent<PlayerInput>();
		_source = GetComponent<AudioSource>();
	}

	void Update () 
	{
		if (_isActive && _playerInput.GetBState() == PlayerInput.ActionButtonState.ButtonDown)
			GoBackToMenu();
	}

	public void StartCredits()
	{
		_source.Play();
		_fireworks.StartFireWorks();
		
		Invoke(nameof(ChangeActive), 0.25f);
	}

	private void ChangeActive()
	{
		_isActive = true;
	}

	public void GoBackToMenu()
	{
		_menuCanvas.SetActive(true);
		MainMenuController.Instance.InitialSetup();

		Debug.Log("Going back baby");
		_isActive = false;
		_source.Stop();
		_fireworks.StopFireWorks();
		
		gameObject.SetActive(false);
	}
}
