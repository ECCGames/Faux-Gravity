﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "CharacterData", menuName = "Data/Character", order = 1)]
public class CharacterInfo : ScriptableObject
{
    public List<WeaponCharacterHand> CharacterHands;
    public RuntimeAnimatorController CharacterHandlessAnimator;
    public RuntimeAnimatorController CharacterHandAnimator;
    public RuntimeAnimatorController CharacterDeathAnimator;
    public RuntimeAnimatorController CharacterVictoryAnimator;
    public float TimeToRespawn;
    public Sprite CharacterPortrait;
    public string CharacterName;
    private bool _selected;
    public PlayerStatusEvent OnSelectChange = new PlayerStatusEvent();
    public bool Selected { get { return _selected; } set { _selected = value; OnSelectChange.Invoke(_selected);} }

    public WeaponCharacterHand GetWeaponHands(WeaponName wname)
    {
        for (int i = 0; i < CharacterHands.Count; i++)
        {
            if (CharacterHands[i].weaponName == wname)
                return CharacterHands[i];
        }
        return null;
    }
}

public class PlayerStatusEvent : UnityEvent<bool> {}