﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class PlayerSelector : MonoBehaviour
{
    [SerializeField] private Image backgroundImage;
    [SerializeField] private Image characterImage;
    [SerializeField] private Text characterNameText;
    [SerializeField] private Image status;
    [SerializeField] private Text _readyText;
    [SerializeField] private Image aButtonImage;
    [SerializeField] private Image pressStartImage;
    [SerializeField] private AudioClip activationClipSound;
    [SerializeField] private Color _playerColor;
    [SerializeField] private int _playerNumber;

    private AudioSource _audioSource;
    private bool _occupied;
    private int _id;
    
    public Color PlayerColor => _playerColor;
    public int PlayerNumber => _playerNumber;
    public bool Occupied { get { return _occupied; } }

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    //TODO: Arreglar esto
    //La logica del playerSettings tendria que estar aca y no al reves
    public void ChangeCharacter(Sprite image, string characterName, bool selected = false)
    {
        characterImage.sprite = image;
        characterNameText.text = characterName;
        ChangeCharacterImageAlpha(selected);
    }

    public void ChangeCharacterImageAlpha(bool isSelected)
    {
        if (isSelected)
        {
            characterImage.color = new Color(characterImage.color.r, characterImage.color.g, characterImage.color.b, 0.25f);
        }
        else
        {
            characterImage.color = new Color(characterImage.color.r, characterImage.color.g, characterImage.color.b, 1f);
        }
    }
    
    public void SetPlayerStatus(bool ready)
    {
        if (ready)
        {
            if (activationClipSound != null)
            {
                _audioSource.clip = activationClipSound;
                _audioSource.Play();
            }

            status.gameObject.SetActive(false);
            _readyText.gameObject.SetActive(true);
            //aButtonImage.gameObject.SetActive(false);
        }
        else
        {
            /*
            status.color = Color.red;
            statusText.text = _id != 0 ? "Press   to choose" : "Press space to choose";
            statusText.color = Color.white;
            aButtonImage.gameObject.SetActive(_id != 0);
            */
            
            status.gameObject.SetActive(true);
            _readyText.gameObject.SetActive(false);
        }
    }

    public void OpenSelector(int id)
    {
        _id = id;
        /*
        statusText.text = _id != 0 ? "Press   to choose" : "Press space to choose";
        aButtonImage.gameObject.SetActive(_id != 0);
        */
        _occupied = true;
        pressStartImage.gameObject.SetActive(false);
    }

    public void CloseSelector()
    {
        _occupied = false;
        status.color = Color.red;
        
        status.gameObject.SetActive(true);
        _readyText.gameObject.SetActive(false);
        
        pressStartImage.gameObject.SetActive(true);
    }
}
