﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuContainer : MonoBehaviour
{

	[SerializeField] private Button _firstSelected;
    [SerializeField] private MenuContainer _lastMenu;

	public Button FirstSelected	{ get { return _firstSelected; } }
    public MenuContainer LastMenu { get { return _lastMenu; } }
}
