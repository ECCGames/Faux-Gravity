﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;

public class GameOverMenuController : MonoBehaviour
{
	[SerializeField] private SpriteRenderer _platform;
	[SerializeField] private SpriteRenderer _fire;
	[SerializeField] private float _rangeToMove;
	[SerializeField] private float _speed;
	[SerializeField] private Animator _winnerAnimator;
	[SerializeField] private FireworkSpawner _fireworkSpawner;
	
	[Space(5)] [Header("Audio Options")] 
	[SerializeField] private AudioClip _sweepSound;
	[SerializeField] private AudioClip _selectSound;
	[SerializeField] private AudioClip _music;
	
	[Space(5)] [Header("Canvas Options")] 
	[SerializeField] private float _axisCoolDown;
	[SerializeField] private GameObject _gameOverCanvas;
	[SerializeField] private GameObject _skipImage;
	[SerializeField] private Text _playerWinnerText;
	[SerializeField] private Button _nextMatchButton;
	[SerializeField] private Button _playAgainButton;
	[SerializeField] private Button _menuButton;
	[SerializeField] private List<PlayerScoreMenu> _playerScores;
	
	
	private bool _animating;
	private Vector3 _platformInitialPosition;
	
	private AudioSource _sfxSource;
	private PlayerInput _actualPlayerInput;
	private Button _selectedButton;
	private bool _canMoveAxis = true;

	private void Awake()
	{
		_sfxSource = GetComponent<AudioSource>();
		_actualPlayerInput = GetComponent<PlayerInput>();
	}

	private void Start()
	{
		_nextMatchButton.onClick.AddListener(GameController.Instance.PlayAgain);
		_playAgainButton.onClick.AddListener(GameController.Instance.ReloadScene);
		_menuButton.onClick.AddListener(LevelController.Instance.ReloadGame);
		
		_platformInitialPosition = _platform.transform.position;
		_gameOverCanvas.SetActive(false);
	}
	
	void Update ()
	{
		if (_animating)
		{
			_platform.transform.Translate(transform.up * _speed * Time.deltaTime);

			if (((int) _actualPlayerInput.PlayerIndex) + 1 == 0)
			{
				if(Input.GetKeyDown(KeyCode.Space)) EndAnimation();
			}
			else
			{
				if (_actualPlayerInput.GetAState() == PlayerInput.ActionButtonState.ButtonDown) EndAnimation();
			}

			if(Vector3.Distance(_platformInitialPosition, _platform.transform.position) >= _rangeToMove) EndAnimation();
		}
		
		else
		{
			if (((int) _actualPlayerInput.PlayerIndex) + 1 == 0)
			{
				if (Input.GetKeyDown(KeyCode.DownArrow))
					NavigateMenu("down");
				else if (Input.GetKeyDown(KeyCode.UpArrow))
					NavigateMenu("up");
				else if (Input.GetKeyDown(KeyCode.LeftArrow))
					NavigateMenu("left");
				else if (Input.GetKeyDown(KeyCode.RightArrow))
					NavigateMenu("right");

				if (Input.GetKeyDown(KeyCode.Return))
					ExecuteSelected();
			}
			else
			{
				if (_actualPlayerInput.GetAState() == PlayerInput.ActionButtonState.ButtonDown)
					ExecuteSelected();

				if (!_canMoveAxis) return;

				if (_actualPlayerInput.GetLeftStickValues().y < -0.2f)
					NavigateMenu("down");
				else if (_actualPlayerInput.GetLeftStickValues().y > 0.2f)
					NavigateMenu("up");
				else if (_actualPlayerInput.GetLeftStickValues().x < -0.2f)
					NavigateMenu("left");
				else if (_actualPlayerInput.GetLeftStickValues().x > 0.2f)
					NavigateMenu("right");
			}
		}
	}

	public void SetGameOverMenu(string winnerName, Color winnerColor, RuntimeAnimatorController playerAnimator, PlayerIndex winnerInput)
	{                                            
		_fire.color = winnerColor;
		
		for (int i = 0; i < _fireworkSpawner.FireWorks.Count; i++)
		{
			_fireworkSpawner.FireWorks[i].GetComponent<SpriteRenderer>().color = winnerColor;
		}

		_playerWinnerText.color = winnerColor;
		_playerWinnerText.text = winnerName + " Wins!";

		_fireworkSpawner.StartFireWorks();
		
		_actualPlayerInput = GetComponent<PlayerInput>();
		_winnerAnimator.runtimeAnimatorController = playerAnimator;
		_actualPlayerInput.SetIndex(winnerInput);
		_animating = true;

		_sfxSource.PlayOneShot(_music);
		
		for (int i = 0; i < _playerScores.Count; i++)
		{
			_playerScores[i].gameObject.SetActive(false);
		}
	}

	public void EndAnimation()
	{
		_gameOverCanvas.SetActive(true);
		_animating = false;
		_platform.transform.position = _platformInitialPosition + new Vector3(0, _rangeToMove, 0);
		_gameOverCanvas.SetActive(true);
		_skipImage.SetActive(false);
		
		_selectedButton = _nextMatchButton;
		_selectedButton.Select();
		_selectedButton.OnSelect(null);
		
		for (int i = 0; i < GameController.Instance.OrderedSettings.Count; i++)
		{
			_playerScores[i].gameObject.SetActive(true);
			_playerScores[i].SetPlayerScoreCanvas(GameController.Instance.OrderedSettings[i]);
		}
	}
	
	public void NavigateMenu(string direction)
	{
		switch (direction)
		{
			//TODO:Hacerlo con enum ameo
			case "up":
				if(_selectedButton.navigation.selectOnUp != null) ChangeSelectedButton(_selectedButton.navigation.selectOnUp.gameObject.GetComponent<Button>());
				break;
			case "down":
				if(_selectedButton.navigation.selectOnDown != null) ChangeSelectedButton(_selectedButton.navigation.selectOnDown.gameObject.GetComponent<Button>());
				break;
			case "left":
				if(_selectedButton.navigation.selectOnLeft != null) ChangeSelectedButton(_selectedButton.navigation.selectOnLeft.gameObject.GetComponent<Button>());
				break;
			case "right":
				if(_selectedButton.navigation.selectOnRight != null) ChangeSelectedButton(_selectedButton.navigation.selectOnRight.gameObject.GetComponent<Button>());
				break;
		}
		
		_canMoveAxis = false;
		Invoke("ResetCanMove", _axisCoolDown);
	}
	
	public void ExecuteSelected()
	{
		Time.timeScale = 1;
		_sfxSource.PlayOneShot(_selectSound);
		_selectedButton.onClick.Invoke();	
	}
	
	public void ChangeSelectedButton(Button newSelected)
	{
		if(newSelected == null) return;
		
		_sfxSource.PlayOneShot(_sweepSound);
		_selectedButton.OnDeselect(null);
		_selectedButton = newSelected;
		_selectedButton.Select();
		_selectedButton.OnSelect(null);	
	}
	
	private void ResetCanMove()
	{
		_canMoveAxis = true;
	}
}
