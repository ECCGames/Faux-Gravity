﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;

public class CharacterSelection : MonoBehaviour 
{
    public static CharacterSelection Instance { get; private set; }
    
    [SerializeField] private List<PlayerSelector> _selectors;
    [SerializeField] private Button _playButton;
    [SerializeField] private Button _lifesButton;
    [SerializeField] private List<PlayerInput> _inputs;
    [SerializeField] private PlayerSettings _playerSettingsPrefab;

    private Navigation _playButtonNavigation;
    
    public List<PlayerSettings> MenuPlayers;

    public List<PlayerSelector> Selectors { get { return _selectors; } }

    private void Awake()
    {
        if(Instance != null)
            Destroy(this);

        Instance = this;
        
        MainMenuController.Instance.ChangeMenuEvent.AddListener(DisablePlayers);
        MenuPlayers = new List<PlayerSettings>();
        if (_inputs.Count != 4) Debug.LogWarning("Esta faltando poner los playerInput dentro del Character Selection");
    }

    private void Start()
    {
        _playButton.onClick.AddListener(GameController.Instance.StartGame);
        _lifesButton.onClick.AddListener(GameController.Instance.ChangeMaxPlayerLifes);

        _playButtonNavigation = _playButton.navigation;
    }

    private void OnEnable()
    {
        if (GamePad.GetState((PlayerIndex)0).IsConnected)
        {
            PlayerSelector openSelector = BringOpenSelector();
            if (openSelector != null && MenuPlayers.Count == 0)
            {
                PlayerSettings pMenu = Instantiate(_playerSettingsPrefab, transform);
                pMenu.SetGeneralSettings(1, openSelector);MenuPlayers.Add(pMenu);
            }
        }
    }

    private void DisablePlayers()
    {
        //Si esta en partida no ahce nad
        if(GameController.Instance.InGame) return;
        
        //Saco de ready a los players
        if (MainMenuController.Instance.ActualMenu == MainMenuController.Instance.FirstMenu)
        {
            for (int i = 0; i < MenuPlayers.Count; i++)
            {
                MenuPlayers[i].ChangeStatus(true);
            }
        }
    }

    private void Update()
    {
        //Usuario joystick
        for (int i = 0; i < _inputs.Count; i++)
        {
            if (GamePad.GetState((PlayerIndex)i).IsConnected)
            {
                PlayerSettings playerI = ContainsPlayer(i + 1);
                //Si contiene al jugador
                if (playerI != null)
                {
                    if (playerI.ControllerId == 1 && !_inputs[i].OnPlayerSelector) continue;
                    
                    //Si quiere salir de la pantalla de seleccion
                    if (_inputs[i].GetStartState() == PlayerInput.ActionButtonState.ButtonDown)
                    {
                        playerI.CloseSelector();
                        MenuPlayers.Remove(playerI);
                    }
                    //Si apreta el boton de aceptar
                    if(_inputs[i].GetJumpState() == PlayerInput.ActionButtonState.ButtonDown)
                    {
                        playerI.ChangeStatus();
                    }
                    //Si quiero cambiar de jugador
                    if(_inputs[i].GetLeftStickValues().x > 0.2f)
                    {
                        playerI.ChangeImage(true);
                    }
                    if (_inputs[i].GetLeftStickValues().x < -0.2f)
                    {
                        playerI.ChangeImage(false);
                    }
                }

                //Si no contiene al jugador
                else
                {
                    if (_inputs[i].GetStartState() == PlayerInput.ActionButtonState.ButtonDown)
                    {
                        PlayerSelector openSelector = BringOpenSelector();
                        
                        if (openSelector != null)
                        {
                            PlayerSettings pMenu = Instantiate(_playerSettingsPrefab, transform);
                            pMenu.SetGeneralSettings(i + 1, openSelector);
                            MenuPlayers.Add(pMenu);
                            
                            //TODO: Ver como pija resolver esto aka que highligtee el selector del player en control del menu
                            /*
                            if (pMenu.ControllerId == 1)
                            {
                                Debug.Log(ContainsPlayer(i + 1).ControllerId);
                                Navigation auxNav = new Navigation();
                                auxNav.mode = Navigation.Mode.Explicit;
                                auxNav.selectOnUp = pMenu.Selector.GetComponent<Button>();
                                _playButton.navigation = auxNav;
                            }
                            */
                        }
                    }
                }
            }
        }
        
        //Usuario teclado
        if (Input.GetKeyDown(KeyCode.Return))
        {
            PlayerSettings player0 = ContainsPlayer(0);

            //Si apreto start habiendo estado metido significa que quiere irse
            if (player0 != null)
            {
                //vuelvo todo a como estaba antes
                player0.CloseSelector();
                MenuPlayers.Remove(player0);
            }

            //Ingreso al nuevo jugador
            else
            {
                PlayerSelector openSelector = BringOpenSelector();
                if (openSelector != null)
                {
                    PlayerSettings pMenu = Instantiate(_playerSettingsPrefab, transform);
                    pMenu.SetGeneralSettings(0, openSelector);
                    MenuPlayers.Add(pMenu);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PlayerSettings player0 = ContainsPlayer(0);
            if (player0 != null)
            {
                player0.ChangeStatus();
            }
        }

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            PlayerSettings player0 = ContainsPlayer(0);
            if (player0 != null)
                player0.ChangeImage(true);
        }
        
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            PlayerSettings player0 = ContainsPlayer(0);
            if (player0 != null)
                player0.ChangeImage(false);
        }
    }

    //if (players.Where(x => x.Controllerid == i) == null) players.Add(new PlayerMenu(i, ""));
    PlayerSettings ContainsPlayer(int id)
    {
        for (int i = 0; i < MenuPlayers.Count; i++)
        {
            if (MenuPlayers[i].ControllerId == id) return MenuPlayers[i];
        }
        return null;
    }

    /// <summary>
    /// retorna el primer selector que esta disponible si es que hay uno
    /// </summary>
    /// <returns></returns>
    PlayerSelector BringOpenSelector()
    {
        for (int i = 0; i < _selectors.Count; i++)
        {
            if (!_selectors[i].Occupied) return _selectors[i];
        }
        return null;
    }
}
