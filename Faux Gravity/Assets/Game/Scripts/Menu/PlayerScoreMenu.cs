﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScoreMenu : MonoBehaviour
{
	[SerializeField] private Image _portraitImage;
	[SerializeField] private Image _crownImage;
	[SerializeField] private Text _playerText;
	[SerializeField] private Text _winsText;

	public void SetPlayerScoreCanvas(PlayerSettings playerSettings)
	{
		_portraitImage.sprite = playerSettings.CharacterInfo.CharacterPortrait;
		_playerText.text = "Player " + playerSettings.PlayerNumber;
		_playerText.color = playerSettings.PlayerColor;

		_winsText.text = "Wins: " + playerSettings.AmountOfWins;
		_crownImage.gameObject.SetActive(playerSettings.LastWinner);
	}
}
