﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour 
{
	[SerializeField] private float _timeToDisappear;
	[SerializeField] private Image _backgroundBanner;
	[SerializeField] private Text _rulesText;
	
	private float _timer;
	
	void Awake ()
	{
		Destroy(gameObject, _timeToDisappear);	
	}
	
	void Update () 
	{
		_timer += Time.deltaTime;
		
		_rulesText.color = new Color(_rulesText.color.r, _rulesText.color.g, _rulesText.color.b, Mathf.Lerp(1, 0, _timer / _timeToDisappear));
		_backgroundBanner.color = new Color(_backgroundBanner.color.r, _backgroundBanner.color.g, _backgroundBanner.color.b, Mathf.Lerp(1, 0, _timer / _timeToDisappear));
	}
}
