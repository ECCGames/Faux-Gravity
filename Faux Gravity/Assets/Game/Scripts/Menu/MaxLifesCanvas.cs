﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaxLifesCanvas : MonoBehaviour
{
	private Button _myButton;
	private Text _myText;
	
	void Start ()
	{
		_myButton = GetComponent<Button>();
		//_myButton.onClick.AddListener(GameController.Instance.ChangeMaxPlayerLifes);

		_myText = GetComponentInChildren<Text>();
		
		GameController.Instance.OnMaxLifesChange.AddListener(ChangeLifesText);
	}

	void ChangeLifesText()
	{
		_myText.text = "Max Lifes: " + GameController.Instance.MaxPlayerLifes;
	}
}
