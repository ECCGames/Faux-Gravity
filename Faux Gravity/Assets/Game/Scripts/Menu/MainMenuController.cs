﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using XInputDotNetPure;

public class MainMenuController : MonoBehaviour
{
	public static MainMenuController Instance { get; private set; }
	
	[SerializeField] private float _axisCoolDown;
	[SerializeField] private MenuContainer _firstMenu;
	[SerializeField] private List<Image> _musicButtonImages;
	[SerializeField] private Sprite _onSprite;
	[SerializeField] private Sprite _offSprite;
    [SerializeField] List<CharacterInfo> _characterInfos;
	[SerializeField] private PlayerInput _firstController;

	[Space(5)] [Header("Credits Options")] 
	[SerializeField] private GameObject _menuCanvas;
	[SerializeField] private CreditsMenuController _credits;

	[Space(5)] [Header("Audio Options")] 
	[SerializeField] private AudioSource _sfxSource;
	[SerializeField] private AudioClip _sweepSound;
	[SerializeField] private AudioClip _selectSound;

	private AudioSource _menuMusic;
	private bool _musicMute;
	private bool _canMoveAxis = true;

	private MenuContainer _actualMenu;
	private UnityEvent _changeMenuEvent;
	private Button _selectedButton;

	public MenuContainer ActualMenu => _actualMenu;
	public MenuContainer FirstMenu => _firstMenu;
	public UnityEvent ChangeMenuEvent => _changeMenuEvent;
	public Button SelectedButton { get { return _selectedButton; } set { _selectedButton = value; } }

    public List<CharacterInfo> CharacterInfos { get { return _characterInfos; } }

	private void Awake()
	{
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
		
		_menuMusic = GetComponent<AudioSource>();
		_changeMenuEvent = new UnityEvent();
		//Reseteo en false los selected cada vez que arranca el juego
		for (int i = 0; i < _characterInfos.Count; i++)
		{
			_characterInfos[i].Selected = false;
		}

		InitialSetup();
	}

	public void InitialSetup()
	{
		_actualMenu = _firstMenu;
		_selectedButton = _firstMenu.FirstSelected;
		_selectedButton.Select();
		_selectedButton.OnSelect(null);
	}
	
	private void Start()
	{
		_credits.gameObject.SetActive(false);
	}

	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace))
			ReturnToLastMenu();
		
		//Teclado
		if(Input.GetKeyDown(KeyCode.DownArrow))
			NavigateMenu("down");
		else if(Input.GetKeyDown(KeyCode.UpArrow))
			NavigateMenu("up");
		else if(Input.GetKeyDown(KeyCode.LeftArrow))
			NavigateMenu("left");
		else if(Input.GetKeyDown(KeyCode.RightArrow))
			NavigateMenu("right");
		
		if(Input.GetKeyDown(KeyCode.Return))
			ExecuteSelected();
		
		//Gamepad
		if (GamePad.GetState((PlayerIndex) 0).IsConnected)
		{
			if (_firstController.GetBState() == PlayerInput.ActionButtonState.ButtonDown)
				ReturnToLastMenu();

			if (_selectedButton == null) return;
			
			if (_firstController.GetAState() == PlayerInput.ActionButtonState.ButtonDown)
				ExecuteSelected();
			
				
			if(!_canMoveAxis) return;
			
			if (_firstController.GetLeftStickValues().y < -0.2f)
				NavigateMenu("down");
			else if (_firstController.GetLeftStickValues().y > 0.2f)
				NavigateMenu("up");
			else if (_firstController.GetLeftStickValues().x < -0.2f)
				NavigateMenu("left");
			else if (_firstController.GetLeftStickValues().x > 0.2f)
				NavigateMenu("right");
		}
	}
	
	public void NavigateMenu(string direction)
	{
		if (_selectedButton == null) return;
		
		switch (direction)
		{
			//TODO:Hacerlo con enum ameo
			case "up":
				if(_selectedButton.navigation.selectOnUp != null) ChangeSelectedButton(_selectedButton.navigation.selectOnUp.gameObject.GetComponent<Button>());
				break;
			case "down":
				if(_selectedButton.navigation.selectOnDown != null) ChangeSelectedButton(_selectedButton.navigation.selectOnDown.gameObject.GetComponent<Button>());
				break;
			case "left":
				if(_selectedButton.navigation.selectOnLeft != null) ChangeSelectedButton(_selectedButton.navigation.selectOnLeft.gameObject.GetComponent<Button>());
				break;
			case "right":
				if(_selectedButton.navigation.selectOnRight != null) ChangeSelectedButton(_selectedButton.navigation.selectOnRight.gameObject.GetComponent<Button>());
				break;
		}
		
		_canMoveAxis = false;
		Invoke("ResetCanMove", _axisCoolDown);
	}

	public void ExecuteSelected()
	{
		if (_selectedButton == null) return;
		
		_sfxSource.PlayOneShot(_selectSound);
		_selectedButton.onClick.Invoke();	
	}

	public void ChangeSelectedButton(Button newSelected)
	{
		if (_selectedButton != null) _selectedButton.OnDeselect(null);

		_selectedButton = newSelected;
		
		if(newSelected == null) return;
		
		_sfxSource.PlayOneShot(_sweepSound);
		_firstController.OnPlayerSelector = newSelected.gameObject.CompareTag("PlayerSelector");
		_selectedButton.Select();
		_selectedButton.OnSelect(null);	
	}
	
	public void GoToMenu(MenuContainer nextMenu)
	{
		if (nextMenu == null) return;
		
		
		_actualMenu.gameObject.SetActive(false);
		_actualMenu = nextMenu;
		_actualMenu.gameObject.SetActive(true);
		
		ChangeSelectedButton(_actualMenu.FirstSelected);
		_changeMenuEvent.Invoke();
	}

	public void ReturnToLastMenu()
	{
		if(_actualMenu.LastMenu != null) GoToMenu(_actualMenu.LastMenu);
	}
	
	public void ToggleMusic()
	{
		_musicMute = !_musicMute;
		_menuMusic.mute = _musicMute;
		for (int i = 0; i < _musicButtonImages.Count; i++)
		{
			_musicButtonImages[i].sprite = _musicMute ? _offSprite : _onSprite;
		}
		
	}	
	
	private void ResetCanMove()
	{
		_canMoveAxis = true;
	}

	public void GoToPractice()
	{
		GameController.Instance.ChangeScene("Practice");	
	}

	public void GoToCredits()
	{
		_menuCanvas.SetActive(false);
		_credits.gameObject.SetActive(true);
		_credits.StartCredits();
	}
	
	public void ExitGame()
	{
		Application.Quit();
	}
}
