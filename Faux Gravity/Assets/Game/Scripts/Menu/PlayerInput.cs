﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;
using System.Reflection;
using System;

public class PlayerInput : MonoBehaviour {

    public enum ActionButtonState { Error = -1, ButtonDown, ButtonUp, ButtonPressed, ButtonNotPressed }
    public enum ControllerButtons { A, B, X, Y, RightShoulder, LeftShoulder }
    public enum GamePadSettings { Buttons, DPad, Triggers, ThumbSticks }

    [Header("Action buttons")]
    [SerializeField] private ControllerButtons jump;
    [SerializeField] private ControllerButtons push;
    [SerializeField] private ControllerButtons changePrimaryWeapon;
    [SerializeField] private ControllerButtons changeSecondaryWeapon;
    [SerializeField] private PlayerIndex playerIndex;

    private bool playerIndexSet;
    private bool disconected;


    Type t;
    PropertyInfo info;
    MethodInfo[] mInfo;

    private GamePadState state = new GamePadState();
    private GamePadState prevState = new GamePadState();
    private bool _onPlayerSelector;

    public GamePadState State { get { return state; } }
    public PlayerIndex PlayerIndex { get { return playerIndex; } set { playerIndex = value; } }
    public bool OnPlayerSelector { get { return _onPlayerSelector; } set { _onPlayerSelector = value; } }

    void Update() {

        prevState = state;
        state = GamePad.GetState(playerIndex);

        //Si estaba conectado y me desconecte
        if (playerIndexSet && !prevState.IsConnected)
        {
            Debug.Log("Se desconecto el control " + playerIndex);
            playerIndexSet = false;
            disconected = true;
        }

        //si estoy desconectado
        if (!playerIndexSet && !prevState.IsConnected)
        {
            if (GamePad.GetState(playerIndex).IsConnected)
            {
                playerIndexSet = true;
                disconected = false;
            }
        }
    }

    public void SetIndex(PlayerIndex index)
    {
        playerIndex = index;
    }

    private ActionButtonState CustomGamePadState(string buttonName)
    {
        t = state.Buttons.GetType();
        info = t.GetProperty(buttonName);
        mInfo = info.GetAccessors();

        ButtonState auxPrevState = (ButtonState)mInfo[0].Invoke(prevState.Buttons, new object[] { });
        ButtonState auxState = (ButtonState)mInfo[0].Invoke(state.Buttons, new object[] { });

        if (auxPrevState == ButtonState.Released && auxState == ButtonState.Pressed)
        {
            return ActionButtonState.ButtonDown;
        }
        if (auxPrevState == ButtonState.Pressed && auxState == ButtonState.Released)
        {
            return ActionButtonState.ButtonUp;
        }
        if (auxPrevState == ButtonState.Pressed && auxState == ButtonState.Pressed)
        {
            return ActionButtonState.ButtonPressed;
        }
        if (auxPrevState == ButtonState.Released && auxState == ButtonState.Released)
        {
            return ActionButtonState.ButtonNotPressed;
        }
        return ActionButtonState.Error;
    }

    public ActionButtonState GetJumpState()
    {
        return CustomGamePadState(Enum.GetName(typeof(ControllerButtons), jump));
    }

    public ActionButtonState GetPushState()
    {
        return CustomGamePadState(Enum.GetName(typeof(ControllerButtons), push));
    }

    public ActionButtonState GetChangePrimaryWeaponState()
    {
        return CustomGamePadState(Enum.GetName(typeof(ControllerButtons), changePrimaryWeapon));
    }

    public ActionButtonState GetChangeSecondaryWeaponState()
    {
        return CustomGamePadState(Enum.GetName(typeof(ControllerButtons), changeSecondaryWeapon));
    }

    public ActionButtonState GetStartState()
    {
        return CustomGamePadState("Start");
    }

    public ActionButtonState GetAState()
    {
        return CustomGamePadState("A");
    }

    public ActionButtonState GetBState()
    {
        return CustomGamePadState("B");
    }

    public ActionButtonState GetFWPrimaryFireState()
    {
        return CustomGamePadState("RightShoulder");
    }

    public ActionButtonState GetSWPrimaryFireState()
    {
        return CustomGamePadState("LeftShoulder");
    }

    public Vector2 GetLeftStickValues()
    {
        return new Vector2(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y);
    }

    public Vector2 GetRightStickValues()
    {
        return new Vector2(state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y);
    }

    public float LeftTriggerValue()
    {
        return state.Triggers.Left;
    }

    public float RightTriggerValue()
    {
        return state.Triggers.Right;
    }

    public float TriggerValue(bool index)
    {
        return index ? state.Triggers.Right : state.Triggers.Left; 
    }
}
