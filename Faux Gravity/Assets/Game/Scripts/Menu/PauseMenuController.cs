﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuController : MonoBehaviour
{
	[SerializeField] private float _axisCoolDown;
	[SerializeField] private Text _pausedText;
	[SerializeField] private Button _resumeButton;
	[SerializeField] private Button _restartButton;
	[SerializeField] private Button _menuButton;
	[SerializeField] private Button _exitButton;
	
	[Space(5)] [Header("Audio Options")] 
	[SerializeField] private AudioSource _sfxSource;
	[SerializeField] private AudioClip _sweepSound;
	[SerializeField] private AudioClip _selectSound;
	
	private float _axisTimer;
	private Button _selectedButton;
	private PlayerInput _actualPlayerInput;

	public PlayerInput ActualPlayerInput { get { return _actualPlayerInput; } set { _actualPlayerInput = value; } }

	private bool _canMoveAxis = true;

	private void Start()
	{
		_resumeButton.onClick.AddListener(LevelController.Instance.ResumeGame);
		_restartButton.onClick.AddListener(LevelController.Instance.ReloadScene);
		_menuButton.onClick.AddListener(LevelController.Instance.ReloadGame);
		_exitButton.onClick.AddListener(LevelController.Instance.ExitGame);
		
		_actualPlayerInput = GetComponent<PlayerInput>();
		_selectedButton = _resumeButton;
	}
	
	private void OnEnable()
	{
		_selectedButton = _resumeButton;
		_selectedButton.Select();
		_selectedButton.OnSelect(null);
	}

	private void Update()
	{
		if (!_canMoveAxis)
		{
			_axisTimer += 0.01f;
			if(_axisTimer >= _axisCoolDown) ResetCanMove();
		}

		if ((int)_actualPlayerInput.PlayerIndex + 1 == 0)
		{
			if(Input.GetKeyDown(KeyCode.DownArrow))
				NavigateMenu("down");
			else if(Input.GetKeyDown(KeyCode.UpArrow))
				NavigateMenu("up");
			else if(Input.GetKeyDown(KeyCode.LeftArrow))
				NavigateMenu("left");
			else if(Input.GetKeyDown(KeyCode.RightArrow))
				NavigateMenu("right");
		
			if(Input.GetKeyDown(KeyCode.Return))
				ExecuteSelected();
			
			if(Input.GetKeyDown(KeyCode.Escape))
				_resumeButton.onClick.Invoke();
		}
		else
		{
			if (_actualPlayerInput.GetAState() == PlayerInput.ActionButtonState.ButtonDown)
				ExecuteSelected();

			if (_actualPlayerInput.GetBState() == PlayerInput.ActionButtonState.ButtonDown)
				_resumeButton.onClick.Invoke();
			
			if(_actualPlayerInput.GetStartState() == PlayerInput.ActionButtonState.ButtonDown)
				_resumeButton.onClick.Invoke();
				
			if(!_canMoveAxis) return;
			
			if (_actualPlayerInput.GetLeftStickValues().y < -0.4f)
				NavigateMenu("down");
			else if (_actualPlayerInput.GetLeftStickValues().y > 0.4f)
				NavigateMenu("up");
			else if (_actualPlayerInput.GetLeftStickValues().x < -0.4f)
				NavigateMenu("left");
			else if (_actualPlayerInput.GetLeftStickValues().x > 0.4f)
				NavigateMenu("right");
		}
	}
	
	public void NavigateMenu(string direction)
	{
		if (!_canMoveAxis) return;
		
		_canMoveAxis = false;
		
		switch (direction)
		{
			//TODO:Hacerlo con enum ameo
			case "up":
				if(_selectedButton.navigation.selectOnUp != null) ChangeSelectedButton(_selectedButton.navigation.selectOnUp.gameObject.GetComponent<Button>());
				break;
			case "down":
				if(_selectedButton.navigation.selectOnDown != null) ChangeSelectedButton(_selectedButton.navigation.selectOnDown.gameObject.GetComponent<Button>());
				break;
			case "left":
				if(_selectedButton.navigation.selectOnLeft != null) ChangeSelectedButton(_selectedButton.navigation.selectOnLeft.gameObject.GetComponent<Button>());
				break;
			case "right":
				if(_selectedButton.navigation.selectOnRight != null) ChangeSelectedButton(_selectedButton.navigation.selectOnRight.gameObject.GetComponent<Button>());
				break;
		}
		
	}
	
	public void ExecuteSelected()
	{
		Time.timeScale = 1;
		_sfxSource.PlayOneShot(_selectSound);
		_selectedButton.onClick.Invoke();	
	}
	
	public void ChangeSelectedButton(Button newSelected)
	{
		if(newSelected == null) return;
		
		_sfxSource.PlayOneShot(_sweepSound);
		
		_selectedButton.OnDeselect(null);
		_selectedButton = newSelected;
		_selectedButton.Select();
		_selectedButton.OnSelect(null);	
	}
	
	private void ResetCanMove()
	{
		_canMoveAxis = true;
		_axisTimer = 0;
	}
}
