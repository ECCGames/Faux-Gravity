﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MapContainer : MonoBehaviour, ISelectHandler, IDeselectHandler
{
	[SerializeField] private Image _myImage; 
	[SerializeField] private Sprite _activeImage;
	[SerializeField] private Sprite _inactiveImage;
	[SerializeField] private GameObject _toggleImage;
	[SerializeField] private string _mapName;

	private bool _active = true;
	private Button _myButton;
	
	public bool Active { get { return _active; } set { _active = value; } }
	public string MapName => _mapName;

	void Start ()
	{
		_myImage.sprite = _activeImage;
		_myButton = GetComponent<Button>();
		_myButton.onClick.AddListener(ChangeMapStatus);
	}
	
	public void ChangeMapStatus()
	{
		_active = !_active;

		if (_active) _myImage.sprite = _activeImage;
		else _myImage.sprite = _inactiveImage;
	}

	public void OnSelect(BaseEventData eventData)
	{
		_toggleImage.SetActive(true);
	}

	public void OnDeselect(BaseEventData eventData)
	{
		_toggleImage.SetActive(false);
	}

	private void OnDisable()
	{
		_toggleImage.SetActive(false);
	}
}
