﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSelection : MonoBehaviour
{
	public static MapSelection Instance { get; private set; }
	
	[SerializeField] private List<MapContainer> _maps;

	public List<MapContainer> Maps { get { return _maps; } set { _maps = value; } }

	private void Awake()
	{
		if(Instance != null)
			Destroy(gameObject);

		Instance = this;
	}
}
