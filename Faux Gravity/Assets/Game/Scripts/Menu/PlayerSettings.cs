﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSettings : MonoBehaviour
{
    [SerializeField] private float timeBetweenMovement;
    [SerializeField] private AudioClip _deniedSound;
    
    private AudioSource _source;

    private int _controllerId;
    private string _selectedCharacterName;
    private bool _status;
    private PlayerSelector _selector;
    private int _currentIndex;
    private bool canMove = true;
    private CharacterInfo _characterInfo;
    private CharacterInfo _actualInfo;
    private int _amountOfWins;
    private bool _lastWinner;

    public int CurrentIndex { get { return _currentIndex; } }
    public int ControllerId { get { return _controllerId; } }
    public string SelectedCharacterName { get { return _selectedCharacterName; } }
    public bool Status { get { return _status; } }
    public PlayerSelector Selector => _selector;
    public CharacterInfo CharacterInfo { get { return _characterInfo; } set { _characterInfo = value; } }
    public Color PlayerColor;
    public int PlayerNumber;
    public int MaxPlayerLifes;
    public int AmountOfWins { get { return _amountOfWins; } set { _amountOfWins = value; } }
    public bool LastWinner { get { return _lastWinner; } set { _lastWinner = value; } }

    private void Start()
    {
        _source = GetComponent<AudioSource>();
    }
    
    public void SetGeneralSettings(int id, PlayerSelector selector)
    {
        _controllerId = id;
        _selector = selector;
        
        if(_actualInfo != null) _actualInfo.OnSelectChange.RemoveListener(ChangeCharacterImage);
        _actualInfo = MainMenuController.Instance.CharacterInfos[0];
        _actualInfo.OnSelectChange.AddListener(ChangeCharacterImage);
        
        PlayerColor = selector.PlayerColor;
        PlayerNumber = selector.PlayerNumber;
        _selector.ChangeCharacter(_actualInfo.CharacterPortrait, _actualInfo.CharacterName, _actualInfo.Selected);
        selector.OpenSelector(id);
    }

    private void ChangeCharacterImage(bool characterStatus)
    {
        if(_actualInfo != _characterInfo) _selector.ChangeCharacterImageAlpha(characterStatus);
    }
    
    public void ChangeStatus(bool forceFalse = false)
    {
        if (forceFalse)
        {
            _status = false;
            _selector.SetPlayerStatus(false);
            MainMenuController.Instance.CharacterInfos[_currentIndex].Selected = false;
            _characterInfo = null;
            return;
        }
        
        //Si no esta seleccionado o es el yo seleccione, puedo cambiar su status
        if (!_actualInfo.Selected || _characterInfo == _actualInfo)
        {
            _status = !_status;
            _selector.SetPlayerStatus(_status);
            _characterInfo = _status ? _actualInfo : null;
            MainMenuController.Instance.CharacterInfos[_currentIndex].Selected = _status;
        }
        else
        {
            _source.clip = _deniedSound;
            _source.Play();
        }
    }

    public void ChangeImage(bool rightMove)
    {
        if (canMove && !_status)
        {
            List<CharacterInfo> info = MainMenuController.Instance.CharacterInfos;
            if (rightMove)
            {
                _currentIndex++;
                if (_currentIndex >= info.Count)
                    _currentIndex = 0;
            }
            else
            {
                _currentIndex--;
                if (_currentIndex < 0)
                    _currentIndex = info.Count - 1;
            }

            if(_actualInfo != null) _actualInfo.OnSelectChange.RemoveListener(ChangeCharacterImage);
            _actualInfo = info[_currentIndex];
            _actualInfo.OnSelectChange.AddListener(ChangeCharacterImage);

            _selector.ChangeCharacter(_actualInfo.CharacterPortrait, _actualInfo.CharacterName, _actualInfo.Selected);
            canMove = false;
            Invoke("Moved",timeBetweenMovement);
        }
    }

    public void Moved()
    {
        canMove = true;
    }

    public void CloseSelector()
    {
        _selector.ChangeCharacter(MainMenuController.Instance.CharacterInfos[0].CharacterPortrait, MainMenuController.Instance.CharacterInfos[0].CharacterName);
        //Si ya estoy seleccionado y cierro el selector, desselecciono ese pj
        if(_status) 
            MainMenuController.Instance.CharacterInfos[_currentIndex].Selected = false;
        
        _selector.CloseSelector();
        Destroy(gameObject);
    }
}
