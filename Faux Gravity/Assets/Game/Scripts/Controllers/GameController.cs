﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    private bool _inGame;
    
    //lista con los parametros de cada Jugador
    private List<PlayerSettings> _playerSettings;
    private List<PlayerSettings> _orderedSettings;
    private List<string> _activeMaps;
    private int _maxPlayerLifes = 5;

    private UnityEvent _onMaxLifesChange;
    
    public bool InGame => _inGame;
    public int MaxPlayerLifes => _maxPlayerLifes;
    public List<PlayerSettings> PlayerSettings { get { return _playerSettings; } }
    public List<PlayerSettings> OrderedSettings => _orderedSettings;
    public UnityEvent OnMaxLifesChange => _onMaxLifesChange;

    void Awake()
    {
        if (Instance != null) Destroy(this);
        else Instance = this;

        _onMaxLifesChange = new UnityEvent();
        _activeMaps = new List<string>();
        _playerSettings = new List<PlayerSettings>();
        _orderedSettings = new List<PlayerSettings>();
        DontDestroyOnLoad(this);
    }

    public void ChangeMaxPlayerLifes()
    {
        _maxPlayerLifes++;
        if (_maxPlayerLifes > 5) _maxPlayerLifes = 1;
        
        _onMaxLifesChange.Invoke();
    }
    
    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void AddPlayers(List<PlayerSettings> players)
    {
        _inGame = true;
        for (int i = 0; i < players.Count; i++)
        {
            players[i].transform.parent = transform;
            _playerSettings.Add(players[i]);
        }
    }
    
    public void StartGame()
    {
        if (CharacterSelection.Instance.MenuPlayers.Count < 2)
        {
            ReturnNotReady();
            return;
        }

        for (int i = 0; i < CharacterSelection.Instance.MenuPlayers.Count; i++)
        {
            if (!CharacterSelection.Instance.MenuPlayers[i].Status)
            {
                ReturnNotReady();
                return;
            }

            CharacterSelection.Instance.MenuPlayers[i].transform.parent = transform;
            _playerSettings.Add(CharacterSelection.Instance.MenuPlayers[i]);
            _playerSettings[i].MaxPlayerLifes = _maxPlayerLifes;
        }

        for (int i = 0; i < MapSelection.Instance.Maps.Count; i++)
        {
            if(MapSelection.Instance.Maps[i].Active) _activeMaps.Add(MapSelection.Instance.Maps[i].MapName); 
        }

        if (_activeMaps.Count < 1)
        {
            ReturnNotReady();
            return;
        }
        
        _inGame = true;
        
        ChangeScene(_activeMaps[Random.Range(0, _activeMaps.Count - 1)]);
    }

    void ReturnNotReady()
    {
        _playerSettings.Clear();
        _inGame = false;
    }
    
    public void PlayAgain()
    {
        ChangeScene(_activeMaps[Random.Range(0, _activeMaps.Count - 1)]);  
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }

    public void OrderPlayerScores()
    {
        _orderedSettings = new List<PlayerSettings>(_playerSettings);
        
        for (int i = 0; i < _orderedSettings.Count; i++)
        {
            for (int j = _orderedSettings.Count - 1; j > 0; j--)
            {
                if (_orderedSettings[j].AmountOfWins > _orderedSettings[j - 1].AmountOfWins)
                {
                    var aux = _orderedSettings[j - 1];
                    _orderedSettings[j - 1] = _orderedSettings[j];
                    _orderedSettings[j] = aux;
                }
            }       
        }
    }
}
