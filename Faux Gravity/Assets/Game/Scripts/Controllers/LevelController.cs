﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using XInputDotNetPure;

[RequireComponent(typeof(AudioSource))]
public class LevelController : MonoBehaviour
{
    public static LevelController Instance;

    [SerializeField] private GameObject _scenary;
    [SerializeField] private List<Player> _players;
    [SerializeField] private List<RespawnPoint> _respawnPoints;
    [SerializeField] private List<LevelInteraction> _levelInteractions;
    
    [Space(5)] [Header("GameOver Options")] 
    [SerializeField] private GameOverMenuController _gameOver;
    [SerializeField] AudioClip _victorySound;
    
    [Space(5)] [Header("Canvas Options")] 
    [SerializeField] private PauseMenuController _pauseCanvas;
    [SerializeField] private GameObject _playerCanvasContainer;
    [SerializeField] private bool _isTutorial;

    private Player _winner;
    //TODO: hacer que 2 personajes no puedan nacer en el mismo punto
    private List<Transform> _usedSpawnPoints = new List<Transform>();
    List<Player> _activePlayers = new List<Player>();
    AudioSource _source;
    private List<RespawnPoint> _closedRespawnPoints;

    private bool _gamePaused;
    private bool _matchOver;

    public bool GamePaused => _gamePaused;

    private void Awake()
    {
        if (Instance != null) Destroy(this);
        else Instance = this;

        if (!_isTutorial) _gameOver.gameObject.SetActive(false);
        
        _closedRespawnPoints = new List<RespawnPoint>();
        _source = GetComponent<AudioSource>();
        
        for (int i = 0; i < _players.Count; i++)
        {
            _players[i].OnPlayerDeath.AddListener(RemovePlayer);
            _players[i].gameObject.SetActive(false);
        }
        if (!_isTutorial)
        {
            for (int i = 0; i < _respawnPoints.Count; i++)
            {
                _respawnPoints[i].OnSafetyChange.AddListener(ChangeRespawnLists);
            }
        }
    }

    void Start()
    {
        if (!_isTutorial) SetPlayers(GameController.Instance.PlayerSettings);
        else SetTutorial();
       
        _pauseCanvas.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void PauseGame(PlayerInput playerId)
    {
        _pauseCanvas.gameObject.SetActive(true);
        _pauseCanvas.ActualPlayerInput.SetIndex(playerId.PlayerIndex);
        
        for (int i = 0; i < _levelInteractions.Count; i++)
        {
            _levelInteractions[i].PauseInteractions();       
        }

        for (int i = 0; i < _players.Count; i++)
        {
            _players[i].Paused = true;
            GamePad.SetVibration((PlayerIndex)(_players[i].Id - 1), 0, 0);
        }

        _gamePaused = true;
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        _pauseCanvas.gameObject.SetActive(false);

        _gamePaused = false;
        Time.timeScale = 1;
        
        for (int i = 0; i < _levelInteractions.Count; i++)
        {
            _levelInteractions[i].ResumeInteractions();     
        }

        for (int i = 0; i < _players.Count; i++)
        {
            _players[i].Paused = false;
        }    
    }

    private void ChangeRespawnLists(RespawnPoint point, bool currentState)
    {
        if(currentState)
        {
            if (_closedRespawnPoints.Contains(point))
            {
                _closedRespawnPoints.Remove(point);
                _respawnPoints.Add(point);
            }
            else Debug.LogWarning("Hubo un error a la hora de mover los spawn Points");
        }
        else
        {
            if (_respawnPoints.Contains(point))
            {
                _respawnPoints.Remove(point);
                _closedRespawnPoints.Add(point);
            }
            else Debug.LogWarning("Hubo un error a la hora de mover los spawn Points");
        }
    }

    private void RemovePlayer(Player player)
    {
        _activePlayers.Remove(player);
        
        if(_isTutorial) ReloadScene();
        
        //TODO: Canvas donde diga que jugador gano / Animacion de victoria
        if (_activePlayers.Count == 1)
        {
            EndMatch();
        }

        LevelCanvasController.Instance.RemovePlayerCanvas(player);
        Debug.Log("removi un jugador, quedan: " + _activePlayers.Count);

    }

    private void SetPlayers(List<PlayerSettings> settings)
    {
        _pauseCanvas.gameObject.SetActive(false);
        
        for (int i = 0; i < settings.Count; i++)
        {
            _players[i].SetPlayer(settings[i]);
            _players[i].gameObject.SetActive(true);
            _activePlayers.Add(_players[i]);
        }

        LevelCanvasController.Instance.SetLevelCanvas(_activePlayers);
    }

    private void EndMatch()
    {
        AudioSource[] sources = GameObject.FindObjectsOfType<AudioSource>();
        
        for (int i = 0; i < sources.Length; i++)
        {
            sources[i].Stop();
            //sources[i].enabled = false;
        }
        
        _matchOver = true;
        _winner = _activePlayers[0];
        
        for (int i = 0; i < GameController.Instance.PlayerSettings.Count; i++)
        {
            GameController.Instance.PlayerSettings[i].LastWinner = false;
            if (_players[i] == _winner)
            {
                GameController.Instance.PlayerSettings[i].LastWinner = true;
                GameController.Instance.PlayerSettings[i].AmountOfWins++;
            }
        }
        
        GameController.Instance.OrderPlayerScores();
        
        _scenary.SetActive(false);
        _playerCanvasContainer.SetActive(false);
        
        _gameOver.gameObject.SetActive(true);
        _gameOver.SetGameOverMenu(_winner.CharacterInfo.CharacterName, _winner.PlayerColor, _winner.CharacterInfo.CharacterVictoryAnimator, _winner.PlayerInput.PlayerIndex);
        
        for (int i = 0; i < _levelInteractions.Count; i++)
        {
            _levelInteractions[i].PauseInteractions();       
        }
        
        for (int i = 0; i < _players.Count; i++)
        {
           _players[i].gameObject.SetActive(false); 
        }

        for (int i = 0; i < _players.Count; i++)
        {
            _players[i].gameObject.SetActive(false);
        }
        
        _source.enabled = true;
        _source.clip = _victorySound;
        _source.Play();   
    }

    //Recursividad ftw
    public Vector3 RespawnPlayer()
    {
        if (_matchOver) return Vector3.zero;
        
        int index = Random.Range(0, _respawnPoints.Count - 1);
        
        if (_respawnPoints[index].CheckSafety())
        {
            return _respawnPoints[index].transform.position;
        }
     
        return RespawnPlayer();
    }

    public void ReloadScene()
    {
        GameController.Instance.ReloadScene();
    }
    
    public void ReloadGame()
    {
        Time.timeScale = 1;
        Destroy(GameController.Instance.gameObject);
        SceneManager.LoadScene("!MainMenu");
    }

    public void SetTutorial()
    {
        _players[0].SetPlayer(null);
        _players[0].gameObject.SetActive(true);
        _activePlayers.Add(_players[0].GetComponent<Player>()); 
        
        LevelCanvasController.Instance.SetLevelCanvas(_activePlayers);
    }

    public void ExitGame()
    {
        GameController.Instance.ExitGame();
    }

}
