﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCanvasController : MonoBehaviour
{
    [SerializeField] private Image _portraitSprite;
    [SerializeField] private Image _portraitCrown;
    [SerializeField] private Image _portraitCross;
    [SerializeField] private Image[] _lifeSprites;

    [Header("First Gun Fields")]
    [SerializeField] private Image _firstWeaponSprite;
    [SerializeField] private Image _firstWeaponBackground;
    [SerializeField] private Text _firstWeaponBullets;
    [SerializeField] private GameObject _firstGunHorizontalLayout;
    Transform[] uiBulletsFirstWeapon;

    [Header("Second Gun Fields")]
    [SerializeField] private Image _secondWeaponSprite;
    [SerializeField] private Image _secondWeaponBackground;
    [SerializeField] private Text _secondWeaponBullets;
    [SerializeField] private GameObject _secondGunHorizontalLayout;
    Transform[] uiBulletsSecondWeapon;

    private int _actualLifesTracker;
    private Player _myPlayer;
    private Weapon _firstWeapon;
    private Weapon _secondWeapon;

    public Player MyPlayer { get { return _myPlayer; } set { _myPlayer = value; }}

    private void Awake()
    {
        for (int i = 0; i < _lifeSprites.Length; i++)
        {
            _lifeSprites[i].gameObject.SetActive(false);
        }
        
        //uiBulletsFirstWeapon = _firstGunHorizontalLayout.GetComponentsInChildren<Transform>();
        //uiBulletsSecondWeapon = _secondGunHorizontalLayout.GetComponentsInChildren<Transform>();

        _firstWeaponSprite.enabled = false;
        //_firstWeaponBackground.enabled = false;
        //_firstWeaponBullets.enabled = false;

        _secondWeaponSprite.enabled = false;
        //_secondWeaponBackground.enabled = false;
        //_secondWeaponBullets.enabled = false;

        //_firstGunHorizontalLayout.SetActive(false);
        //_secondGunHorizontalLayout.SetActive(false);
    }

    public void SetPlayerCanvas()
    {
        _portraitSprite.sprite = _myPlayer.CharacterInfo.CharacterPortrait;
        _portraitCross.enabled = false;
        _actualLifesTracker = _myPlayer.ActualLifes;
        
        _portraitCrown.gameObject.SetActive(_myPlayer.LastWinner);
        //WeaponPickUp
        _myPlayer.MyPlayerAttack.OnWeaponPickUp.AddListener(SetWeaponData);
        
        for (int i = 0; i < _myPlayer.ActualLifes; i++)
        {
            _lifeSprites[i].gameObject.SetActive(true);
            _lifeSprites[i].color = _myPlayer.PlayerColor;
            
            //Esto es si en un futuro cada jugador tiene su propio corazon
            //_lifeSprites[i].GetComponent<Image>().sprite = _myPlayer.LifeSprite;
        }
        _myPlayer.OnLifesChange.AddListener(UpdateLifes);
    }

    private void Update()
    {
        if (_firstWeapon != null) _firstWeaponSprite.fillAmount = _firstWeapon.TimeToCool / _firstWeapon.FireRate;
        
        if (_secondWeapon!= null) _secondWeaponSprite.fillAmount = _secondWeapon.TimeToCool / _secondWeapon.FireRate;
    }

    public void SetWeaponData(Weapon weapon, int index)
    {
        if (weapon == null)
        {
            if (index == 0)
            {
                _firstWeapon = null;
                _firstWeaponSprite.enabled = false;
                /*
                 _firstWeaponBackground.enabled = false;
                _firstWeaponBullets.enabled = false;
                _firstGunHorizontalLayout.gameObject.SetActive(false);
                */
            }
            else
            {
                _secondWeapon = null;
                _secondWeaponSprite.enabled = false;
                /*_secondWeaponBackground.enabled = false;
                _secondWeaponBullets.enabled = false;
                _secondGunHorizontalLayout.gameObject.SetActive(false);
                */
            }
        }
        else
        {
            if(index == 0)
            {
                _firstWeapon = weapon;
                _firstWeaponSprite.enabled = true;
                _firstWeaponSprite.sprite = weapon.GetUIImage();
                weapon.OnBulletsChange.AddListener(UpdateFirstWeapon);
                UpdateFirstWeapon();
                /*
                if (!weapon.UseCustomBullets)
                {
                    //_firstWeaponBackground.enabled = true;
                    _firstWeaponBullets.enabled = true;
                    _firstWeaponBullets.color = weapon.GetBulletsUIColor();
                }
                else
                {
                    Debug.Log("es CustomBullet");
                    _firstGunHorizontalLayout.gameObject.SetActive(true);
                    _firstWeaponBullets.gameObject.SetActive(false);
                }
                */
            }
            else
            {
                _secondWeapon = weapon;
                _secondWeaponSprite.enabled = true;
                _secondWeaponSprite.sprite = weapon.GetUIImage();
                weapon.OnBulletsChange.AddListener(UpdateSecondWeapon);
                UpdateSecondWeapon();
                /*
                if (!weapon.UseCustomBullets)
                {
                    //_secondWeaponBackground.enabled = true;
                    _secondWeaponBullets.enabled = true;
                    _secondWeaponBullets.color = weapon.GetBulletsUIColor();
                }
                else
                {
                    Debug.Log("es CustomBullet");
                    _secondGunHorizontalLayout.SetActive(true);
                    _secondWeaponBullets.gameObject.SetActive(false);
                }
                */
            }
        }
    }

    private void UpdateLifes()
    {
        int actualPlayerLifes = _myPlayer.ActualLifes;
        
        if(_actualLifesTracker > actualPlayerLifes)
            _lifeSprites[actualPlayerLifes].gameObject.SetActive(false);
        else
            _lifeSprites[actualPlayerLifes].gameObject.SetActive(true);

        _actualLifesTracker = actualPlayerLifes;   
    }

    public void UpdateFirstWeapon()
    {
        int bullets = _myPlayer.MyPlayerAttack.Inventory[0].GetBullets();
        if (bullets == 0)
        {
            _firstWeaponSprite.color = new Color(1, 1, 1, 0.25f);
        }
        else
        {
            _firstWeaponSprite.color = new Color(1, 1, 1, 1);
        }
        /*
        if (!_firstWeapon.UseCustomBullets)
        {
            _firstWeaponBullets.text = bullets.ToString();
        }
        else
        {
            List<GameObject> activeUiBullets = new List<GameObject>();
            for (int i = 0; i < uiBulletsFirstWeapon.Length; i++)
            {
                if (uiBulletsFirstWeapon[i].gameObject.activeSelf)
                    activeUiBullets.Add(uiBulletsFirstWeapon[i].gameObject);
            }
            Debug.Log(activeUiBullets.Count);
            int amountToChange = activeUiBullets.Count - bullets;
            Debug.Log(amountToChange);
            if (amountToChange>0)
            {
                for (int i = 0; i < amountToChange; i++)
                {
                    activeUiBullets[i].SetActive(false);
                }
            }
            if (amountToChange < 0)
            {
                int index = 0;
                while (index < Mathf.Abs(amountToChange))
                {
                    for (int i = 0; i < uiBulletsFirstWeapon.Length; i++)
                    {
                        if (!uiBulletsFirstWeapon[i].gameObject.activeSelf)
                        {
                            uiBulletsFirstWeapon[i].gameObject.SetActive(true);
                            index++;
                        }
                    }
                }
              
            }
        }
        */
    }

    public void UpdateSecondWeapon()
    {
        int bullets = _myPlayer.MyPlayerAttack.Inventory[1].GetBullets();
        if (bullets == 0)
        {
            _secondWeaponSprite.color = new Color(1, 1, 1, 0.25f);
        }
        else
        {
            _secondWeaponSprite.color = new Color(1, 1, 1, 1);
        }
        /*
        if (!_secondWeapon.UseCustomBullets)
        {
            _secondWeaponBullets.text = bullets.ToString();
        }
        else
        {
            List<GameObject> activeUiBullets = new List<GameObject>();
            for (int i = 0; i < uiBulletsSecondWeapon.Length; i++)
            {
                if (uiBulletsSecondWeapon[i].gameObject.activeSelf)
                    activeUiBullets.Add(uiBulletsSecondWeapon[i].gameObject);
            }
            int amountToChange = activeUiBullets.Count - bullets;
            if (amountToChange > 0)
            {
                for (int i = 0; i < amountToChange; i++)
                {
                    activeUiBullets[i].SetActive(false);
                }
            }
            if (amountToChange < 0)
            {
                int index = 0;
                while (index < Mathf.Abs(amountToChange))
                {
                    for (int i = 0; i < uiBulletsSecondWeapon.Length; i++)
                    {
                        if (!uiBulletsSecondWeapon[i].gameObject.activeSelf)
                        {
                            uiBulletsSecondWeapon[i].gameObject.SetActive(true);
                            index++;
                        }
                    }
                }
            }
        }
        */
    }

    public void ActivateCross()
    {
        _portraitCross.enabled = true;
    }
}
