﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCanvasController : MonoBehaviour
{
	public static LevelCanvasController Instance { get; private set; }
	
	[SerializeField] private PlayerCanvasController[] _playerCanvas;

	private void Awake()
	{
		if (!Instance) Instance = this;
		else Destroy(this);

		for (int i = 0; i < _playerCanvas.Length; i++)
		{
			//_playerCanvas[i].gameObject.SetActive(false);
		}
	}

	public void SetLevelCanvas(List<Player> activePlayers)
	{
		for (int i = 0; i < activePlayers.Count; i++)
		{
			_playerCanvas[i].gameObject.SetActive(true);
			_playerCanvas[i].MyPlayer = activePlayers[i];
			_playerCanvas[i].SetPlayerCanvas();
		}
	}

    public void RemovePlayerCanvas(Player player)
    {
        for (int i = 0; i < _playerCanvas.Length; i++)
        {
            if (_playerCanvas[i].MyPlayer == player)
            {
                _playerCanvas[i].ActivateCross();
                break;
            }
        }
    }
}
