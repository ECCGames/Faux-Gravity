﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D), typeof(Rigidbody2D))]
public class WeaponPickUp : MonoBehaviour
{

    [SerializeField] private SpriteRenderer _UIHelper;
    [SerializeField] private SpriteRenderer _weaponImage;
    [SerializeField] private BoxCollider2D _terrainCollider;
    [SerializeField] private BoxCollider2D _playerCollider;
    [SerializeField] private Rigidbody2D _rigid;
    [SerializeField] private float _minimumVelocity;
    [SerializeField] private float _timeToStun;
    [SerializeField] private float _timeToDie;


    private Player _owner;
    private Weapon _pickableWeapon;
    private bool _canBePicked;
    private PickUpStand _stand;

    public Player Owner { get { return _owner; } set { _owner = value; } }

    public Weapon PickableWeapon
    {
        get { return _pickableWeapon; }
    }

    public bool CanBePicked
    {
        get { return _canBePicked; }
    }

    public Rigidbody2D Rigid
    {
        get { return _rigid; }
    }

    private void Awake()
    {
        _UIHelper.gameObject.SetActive(false);
        _weaponImage.sprite = null;
    }

    private void Update()
    {
        if (_playerCollider == null) return;

        if (_playerCollider.enabled && _rigid.velocity.magnitude < _minimumVelocity) StopStunningPlayer();
    }

    public void SetWeapon(Weapon weapon, PickUpStand stand = null, float weaponAngle = 0, float timer = 0)
    {
        if (timer != 0) Destroy(gameObject, timer);

        _canBePicked = false;
        Invoke("ChangePicked", 0.1f);
        _weaponImage.sprite = weapon.GetPickUpImage();
        _weaponImage.transform.rotation = Quaternion.Euler(0, 0, weaponAngle);
        //_UIHelper.transform.rotation = Quaternion.identity;
        _pickableWeapon = weapon;

        if (stand != null)
        {
            _stand = stand;
            StopStunningPlayer();
        }
        else
            Destroy(gameObject,_timeToDie);
    }

    public Weapon GetWeapon()
    {
        if (_canBePicked)
        {
            if (_stand != null) _stand.GrabbedWeapon();
            _canBePicked = false;
            Destroy(gameObject, 0.1f);
            return _pickableWeapon;
        }

        return null;
    }

    public void HookWeapon()
    {
        if (_canBePicked && _stand != null)
        {
            _stand.HookedWeapon();
            _stand = null;
            _rigid.bodyType = RigidbodyType2D.Dynamic;
            Destroy(gameObject,_timeToDie);
        }
    }
    
    private void ChangePicked()
    {
        _canBePicked = true;
    }

    private void StopStunningPlayer()
    {
        _terrainCollider.enabled = true;
        _playerCollider.enabled = false;
    }

    private void StartStunningPlayer()
    {
        _terrainCollider.enabled = false;
        _playerCollider.enabled = true; 
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Player playerToStun = collision.gameObject.GetComponent<Player>();
            if(_rigid.velocity.magnitude >= _minimumVelocity && playerToStun != _owner) playerToStun.Stun(_timeToStun);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            _UIHelper.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            _UIHelper.gameObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        if(_stand != null) _stand.GrabbedWeapon();
    }
}
