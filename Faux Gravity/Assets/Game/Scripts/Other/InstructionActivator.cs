﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionActivator : MonoBehaviour
{
	[SerializeField] private GameObject _instruction;
	[SerializeField] private float _timeToHide;

	private void Awake()
	{
		_instruction.SetActive(false);
	}

	private void OnTriggerEnter2D(Collider2D col)
	{
		if (col.CompareTag("Player"))
		{
			_instruction.SetActive(true);
		}
	}

	private void OnTriggerExit2D(Collider2D col)
	{
		if (col.CompareTag("Player"))
		{
			CancelInvoke("TurnOffInstruction");
			Invoke("TurnOffInstruction", _timeToHide);
		}
	}

	void TurnOffInstruction()
	{
		_instruction.SetActive(false);
	}
}
