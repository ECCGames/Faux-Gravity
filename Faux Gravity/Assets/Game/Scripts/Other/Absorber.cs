﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Absorber : MonoBehaviour {

    [SerializeField] private float _absortionStrength;
    [SerializeField] private float _playerAbsortionStrength;

    private List<LavaSpawner> _lavaSpawners;

    private void Start()
    {
        _lavaSpawners = new List<LavaSpawner>();
    }

    private void Update()
    {
        for (int i = 0; i < _lavaSpawners.Count; i++)
        {
            _lavaSpawners[i].SpawnLavaParticle();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (LayerMask.LayerToName(collision.gameObject.layer) == "Lava")
        {
            var lavaSpawner = collision.GetComponent<LavaSpawner>();
            if (lavaSpawner != null && !_lavaSpawners.Contains(lavaSpawner))
            {
                _lavaSpawners.Add(lavaSpawner);
            }
        }
        else if(LayerMask.LayerToName(collision.gameObject.layer) == "SpikeSpawner")
        {
            var spikeSpawner = collision.GetComponent<SpikeSpawner>();
            if(spikeSpawner != null) spikeSpawner.StartSpawnSpike();
        }
        
        Vector2 dist = transform.position - collision.transform.position;
        if (collision.CompareTag("Player"))
        {
            Player player = collision.GetComponent<Player>();
            if (player != null && !player.IsImmune) 
                player.AddForce(dist.normalized * (_playerAbsortionStrength / Mathf.Clamp(dist.magnitude, 1f, Mathf.Infinity)));
        }
        else
        {
            Rigidbody2D collidedObject = collision.GetComponent<Rigidbody2D>();
            if (collidedObject != null)
                collidedObject.AddForce(dist.normalized * (_absortionStrength / Mathf.Clamp(dist.magnitude, 1f, Mathf.Infinity)));
        }
    }
}
