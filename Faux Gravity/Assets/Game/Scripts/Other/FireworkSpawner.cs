﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEngine;

public class FireworkSpawner : MonoBehaviour
{
	[SerializeField] private float _spawnRate;
	[SerializeField] private AudioClip _fireWorkSound;

	private List<Firework> _fireWorks;
	
	public List<Firework> FireWorks
	{
		get { return _fireWorks; }
	}

	private void Awake()
	{
		_fireWorks = new List<Firework>(GetComponentsInChildren<Firework>());
		
		for (int i = 0; i < _fireWorks.Count; i++)
		{
			_fireWorks[i].gameObject.SetActive(false);
			_fireWorks[i].SetAudio(_fireWorkSound);
		}
	}

	public void StartFireWorks()
	{
		InvokeRepeating(nameof(SpawnRandomFirework), 0, _spawnRate);
	}

	public void StopFireWorks()
	{
		CancelInvoke(nameof(SpawnRandomFirework));

		for (int i = 0; i < _fireWorks.Count; i++)
		{
			_fireWorks[i].StopFirework();
		}
	}

	public void SpawnRandomFirework()
	{
		_fireWorks[Random.Range(0, _fireWorks.Count -1)].SpawnFirework();	
	}
}
