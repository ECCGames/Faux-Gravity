﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnTimer : MonoBehaviour
{
	[SerializeField] private float _timeToDestroy;

	private void Awake()
	{
		Invoke("DestroyMe",_timeToDestroy);
	}

	private void DestroyMe()
	{
		Destroy(gameObject);
	}
}
