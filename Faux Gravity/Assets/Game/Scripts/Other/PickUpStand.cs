﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpStand : MonoBehaviour {

    [SerializeField] List<Weapon> _spawnableWeapons;
    [SerializeField] WeaponPickUp pickUpPrefab;

    [Header("Own Fields")]
    [SerializeField] float _reSpawnTime;
    [SerializeField] ParticleSystem _spawnParticleSystem;

    WeaponPickUp _currentWeapon;

    private void Start()
    {
        SpawnWeapon();
    }

    /*
    void Update()
    {
        float pingpong = Mathf.PingPong(Time.time * 0.5f, 1);
        _currentWeapon.transform.position = Vector3.Lerp(transform.position + new Vector3(0, 0.25f, 0),
            transform.position - new Vector3(0, 0.75f, 0), pingpong);
    }
     */
    public void SpawnWeapon()
    {
        if(_spawnParticleSystem != null) _spawnParticleSystem.Play();
        _currentWeapon = Instantiate(pickUpPrefab, transform.position, Quaternion.Euler(0, 0, 45),transform);
        _currentWeapon.SetWeapon(_spawnableWeapons[Random.Range(0, _spawnableWeapons.Count)],this,45);
        _currentWeapon.Rigid.bodyType = RigidbodyType2D.Kinematic;
    }

    public void GrabbedWeapon()
    {
        CancelInvoke("SpawnWeapon");
        Destroy(_currentWeapon.gameObject);
        Invoke("SpawnWeapon", _reSpawnTime);
    }

    public void HookedWeapon()
    {
        CancelInvoke("SpawnWeapon");
        _currentWeapon = null;
        Invoke("SpawnWeapon", _reSpawnTime);
    }
}
