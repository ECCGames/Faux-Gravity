﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnDeathEvent : UnityEvent<Rigidbody2D>{}

[RequireComponent(typeof(Rigidbody2D))]
public class PhysicsObject : MonoBehaviour
{
	//Grado de deslizamiento
	[SerializeField] private float _minGroundNormalY = 0.65f;
	[SerializeField] protected float _gravityModifier = 1f;

	protected Vector2 TargetVelocity;
	protected bool Grounded;
	protected Vector2 GroundNormal;
	protected Vector2 Velocity;
	protected Rigidbody2D Rigid2D;
	protected ContactFilter2D ContactFilter;
	protected RaycastHit2D[] HitBuffer = new RaycastHit2D[16];
	protected List<RaycastHit2D> HitBufferList = new List<RaycastHit2D>(16);
    private OnDeathEvent _onDeath;

	public OnDeathEvent OnDeath { get { return _onDeath; } }
	
	protected const float MinMovedDistance = 0.001f;
	protected const float ShellRadius = 0.01f;

    protected virtual void Awake()
    {
        _onDeath = new OnDeathEvent();
    }

    void OnEnable ()
	{
		Rigid2D = GetComponent<Rigidbody2D>();
	}

	void Start()
	{
		ContactFilter.useTriggers = false;
		ContactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
		ContactFilter.useLayerMask = true;
	}

	public virtual void Update()
	{
		TargetVelocity = Vector2.zero;
		ComputeVelocity();
	}
	
	protected virtual void ComputeVelocity(){}
	
	
	protected virtual void FixedUpdate()
	{
		//Gravedad
		Velocity += _gravityModifier * Physics2D.gravity * Time.deltaTime;
		Velocity.x = TargetVelocity.x;

		//Grounded = false;
		
		Vector2 deltaPosition = Velocity * Time.deltaTime;
		
		Vector2 moveAlongGround = new Vector2(GroundNormal.y, -GroundNormal.x);

		Vector2 move = moveAlongGround * deltaPosition.x;
		
		//Movimiento en X
		Movement(move, false);
		
		move = Vector2.up * deltaPosition.y;
		
		//Movimiento en Y
		Movement(move, true);
	}

	private void Movement(Vector2 move, bool yMovement)
	{
		//Distancia que recorreria en este frame
		float distance = move.magnitude;

		if (distance > MinMovedDistance)
		{
			int count = Rigid2D.Cast(move, ContactFilter, HitBuffer, distance + ShellRadius);
			HitBufferList.Clear();
			for (int i = 0; i < count; i++)
			{
				HitBufferList.Add(HitBuffer[i]);
			}

			for (int i = 0; i < HitBufferList.Count; i++)
			{
				Vector2 currentNormal = HitBufferList[i].normal;
				if (currentNormal.y > _minGroundNormalY)
				{
					//Grounded = true;
					if (yMovement)
					{
						GroundNormal = currentNormal;
						currentNormal.x = 0;
					}
				}
				
				//Substrae la velocidad al chocar con una superficie 
				float projection = Vector2.Dot(Velocity, currentNormal);
				if (projection < 0)
				{
					Velocity -= projection * currentNormal;
				}

				//Usamos una segunda distancia para evitar overlapear con otros objetos
				float modifiedDistance = HitBufferList[i].distance - ShellRadius;
				distance = modifiedDistance < distance ? modifiedDistance : distance;
			}
		}
		
		//Debug.Log((gameObject.name + ": " + Rigid2D.position));
		Rigid2D.position += move.normalized * distance;
		//Debug.Log((gameObject.name + ": " + Rigid2D.position));
	}

	private void OnDestroy()
	{
		_onDeath.Invoke(Rigid2D);
	}
}
