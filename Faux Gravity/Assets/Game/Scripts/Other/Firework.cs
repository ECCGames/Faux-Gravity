﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Firework : MonoBehaviour
{
	[SerializeField] private float _fireworkDuration = 1.7f;

	private SpriteRenderer _spriteRenderer;
	private Animator _fireworkAnimator;
	private AudioSource _audioSource;

	public AudioSource AudioSource { get { return _audioSource; } set { _audioSource = value; } }

	private void Awake()
	{
		_spriteRenderer = GetComponent<SpriteRenderer>();
		_fireworkAnimator = GetComponent<Animator>();
	}

	public void SetAudio(AudioClip clip)
	{
		_audioSource = GetComponent<AudioSource>();
		_audioSource.clip = clip;
	}
	public void SpawnFirework()
	{
		gameObject.SetActive(true);
		_fireworkAnimator.SetBool("fire", true);
		Invoke(nameof(StopFirework), _fireworkDuration);
	}

	public void StopFirework()
	{
		_spriteRenderer.sprite = null;
		_audioSource.Play();
		_fireworkAnimator.SetBool("fire", false);
		//gameObject.SetActive(false);
	}
}
