﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class VelocityMovement : MonoBehaviour
{
	[SerializeField] Vector2 inertia;
	[SerializeField] float speed;

	[SerializeField] float gravity;

	[SerializeField] float easing;

	[SerializeField] float jumpEasing;

	[SerializeField] float jumpForce;

	[SerializeField] ColliderChecker gc;
	[SerializeField] ColliderChecker _ceilingChecker;

	[SerializeField] private bool _usingKeyboard;
	
	float verticalVelocity;
	private float horizontalVelocity;
	float tempJumpForce;
	Rigidbody2D rb;
	private PlayerInput _playerInput;
	private bool _jump;
	private bool _jumpWithAxis;
	
	void Awake()
	{
		_playerInput = GetComponent<PlayerInput>();
		rb = GetComponent<Rigidbody2D>();
	}

	void Update ()
	{
		if (gc.IsColliding)
		{
			verticalVelocity = 0;
			tempJumpForce = jumpForce;
			_jumpWithAxis = false;
		}

		if (_ceilingChecker.IsColliding) verticalVelocity = 0;
		
		verticalVelocity -= gravity * Time.deltaTime;
		
		if (_usingKeyboard)
		{
			horizontalVelocity = Input.GetAxis("Horizontal") * speed;

			
			if (gc.IsColliding && Input.GetKeyDown(KeyCode.Space)) _jump = true;
			else if (Input.GetKeyUp(KeyCode.Space)) _jump = false;
			
		}
		else
		{
			horizontalVelocity = _playerInput.GetLeftStickValues().x * speed;
						
			if (gc.IsColliding && _playerInput.GetJumpState() == PlayerInput.ActionButtonState.ButtonDown) _jump = true;
			else if (_playerInput.GetJumpState() == PlayerInput.ActionButtonState.ButtonUp) _jump = false;

			if (gc.IsColliding && _playerInput.GetLeftStickValues().y > .6f)
			{
				_jump = true;
				_jumpWithAxis = true;
			}
			else if (_jumpWithAxis && _playerInput.GetLeftStickValues().y <= .6f) _jump = false;
		}

		if (_jump)
		{
			verticalVelocity += tempJumpForce;
			tempJumpForce *= jumpEasing;
		}
		
		if (Input.GetKey(KeyCode.Q))
			AddForce(new Vector2(10,10));
		
		if (Input.GetKey(KeyCode.E))
			AddForce(new Vector2(-5,5));
	}

	private void FixedUpdate()
	{
		Vector2 velocity = Vector2.zero;
		velocity.x = horizontalVelocity;
		velocity.y = verticalVelocity;
		rb.velocity = velocity + inertia;
        		
		inertia *= easing;
	}

	public void AddForce(Vector2 force)
	{
		verticalVelocity = 0;
		inertia += force;
	}
}
