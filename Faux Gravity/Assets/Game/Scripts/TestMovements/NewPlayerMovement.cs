﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayerMovement : MonoBehaviour
{
	[SerializeField] private Vector2 _inertia;
	[SerializeField] private float _gravityForce;
	[SerializeField] private float _movementSpeed;
	[SerializeField] private float _easing;
	[SerializeField] private float _jumpForce;
	[SerializeField] private float _jumpEasing;

	private CharacterController _cc;
	private Vector3 _moveVector;
	private bool _canJump;
	private float _tempJumpForce;
	private float _verticalVelocity;

	void Awake()
	{
		_cc = GetComponent<CharacterController>();
		_cc.detectCollisions = true;
	}
	
	void Update()
	{
		_verticalVelocity -= _gravityForce;
		
		if (_cc.isGrounded)
		{
			_verticalVelocity = 0;
			_tempJumpForce = _jumpForce;
			_moveVector.x = Input.GetAxis("Horizontal") * _movementSpeed;
		}

		if (Input.GetKey(KeyCode.Space))
		{
			_verticalVelocity += _tempJumpForce;
			_tempJumpForce *= _jumpEasing;
		}

		_moveVector.y = _verticalVelocity;
		_moveVector.z = 0;
		
		_cc.Move((_moveVector + (Vector3)_inertia) * Time.deltaTime);

		_inertia *= _easing;
	}

}
