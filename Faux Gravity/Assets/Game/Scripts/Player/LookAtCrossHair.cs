﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCrossHair : MonoBehaviour
{

	[SerializeField] private GameObject _crossHair;
	//[SerializeField] private GameObject _aimer;
	[SerializeField] private Transform _bulletOrigin;
	//[SerializeField] private LayerMask _raycastLayers;
	[SerializeField] private float _laserLenght;
	[SerializeField] private Vector3 _offsetFixer;
	
	private SpriteRenderer _weaponSprite;
	private LineRenderer _laserSight;

	private Color _playerColor;
	private RaycastHit2D _hit2D;
	
	private bool _flipped;

	private void Start()
	{
		_weaponSprite = GetComponent<SpriteRenderer>();
		_laserSight = GetComponent<LineRenderer>();

		_playerColor = GetComponentInParent<Player>().PlayerColor;
		_laserSight.startColor = _playerColor;
		_laserSight.endColor = new Color(_playerColor.r, _playerColor.g, _playerColor.b, 0);
		transform.position -= _offsetFixer;
	}

	void Update ()
	{
		Vector3 dir = _crossHair.transform.position - transform.position;
		dir.Normalize();
		float rot = Mathf.Atan2(dir.y, dir.x);
		transform.rotation = Quaternion.Euler(0 ,0 ,rot * Mathf.Rad2Deg);
		if (_crossHair.transform.localPosition.x <= 0 && !_flipped)
		{
			transform.position += _offsetFixer * 2;
			_weaponSprite.flipY = true;
			_flipped = true;
		}
		else if (_crossHair.transform.localPosition.x > 0 && _flipped)
		{
			transform.position -= _offsetFixer * 2;
			_weaponSprite.flipY = false;
			_flipped = false;
		}
		
		_laserSight.SetPosition(0, _bulletOrigin.position);
		
		_laserSight.SetPosition(1, transform.position + (_bulletOrigin.transform.position - transform.position).normalized * _laserLenght);
		/*
		_hit2D = Physics2D.Raycast(_bulletOrigin.position, (_crossHair.transform.position - _bulletOrigin.transform.position).normalized, 200f, _raycastLayers);

		if(_hit2D != null) _aimer.transform.position = _hit2D.transform.position;    */
	}

	private void FlipWeapon()
	{
		
	}
}
