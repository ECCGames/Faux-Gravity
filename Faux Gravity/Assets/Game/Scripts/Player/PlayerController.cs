﻿ using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Debug = UnityEngine.Debug;

public class PlayerController : MonoBehaviour, IResettable
{
    private bool _usingKeyboard;

    private Player _myPlayer;
    private PlayerMovement _myPlayerMovement;
    private PlayerAttack _myPlayerAttack;
    private PlayerInput _playerInput;
    private PlayerPush _playerPush;

    private bool _currentLtState;
    private bool _currentRtState;

    private float _dirX;
    
    private Vector2 _aimVector;
    private Camera _mainCamera;
    
    public int Id { get { return (((int)_playerInput.PlayerIndex) + 1) ; } }
    public PlayerInput PlayerInput { get { return _playerInput; } }
    public bool UsingKeyboard { get { return _usingKeyboard; } }


    private void Awake()
    {
        _mainCamera = Camera.main;
        _myPlayer = GetComponent<Player>();
        _myPlayerMovement = GetComponent<PlayerMovement>();
        _myPlayerAttack = GetComponent<PlayerAttack>();
        _playerInput = GetComponent<PlayerInput>();
        _playerPush = _myPlayer.GetComponentInChildren<PlayerPush>();
    }

    private void Start()
    {
        Debug.Log(Id);
        if (Id == 0) _usingKeyboard = true;
        _myPlayer.OnLifesChange.AddListener(ResetValues);
    }

    private void Update()
    {
        if (_myPlayer.Paused) return;
        
        #region Controller
        if (!_usingKeyboard)
        {
            if (_playerInput.GetStartState() == PlayerInput.ActionButtonState.ButtonDown)
            {
                LevelController.Instance.PauseGame(_playerInput);
            }
            if (_myPlayer.IsStunned || _myPlayer.IsDying || _myPlayer.Paused) return;
            
            //Apuntar y disparar
            _aimVector = Vector2.zero;
            _aimVector = _playerInput.GetRightStickValues();
            _myPlayerAttack.Aim(_aimVector);
            
            if (_playerInput.RightTriggerValue() > 0.1f)
            {
                _myPlayerAttack.StartShootingPrimaryWeapon();
                _currentRtState = true;
            }
            else if(_currentRtState)
            {
                _myPlayerAttack.ReleasePrimaryWeapon();
                _currentRtState = false;
            }

            if (_playerInput.LeftTriggerValue() > 0.1f)
            {
                _myPlayerAttack.StartShootingSecondaryWeapon();
                _currentLtState = true;
            }
            else if(_currentLtState)
            {
                _myPlayerAttack.ReleaseSecondaryWeapon();
                _currentLtState = false;
            }
            
            if(_playerInput.GetChangePrimaryWeaponState() == PlayerInput.ActionButtonState.ButtonDown)
                _myPlayerAttack.ThrowWeapon(0);
            
            if(_playerInput.GetChangeSecondaryWeaponState() == PlayerInput.ActionButtonState.ButtonDown)
                _myPlayerAttack.ThrowWeapon(1);
            
            //Empuje
            if (_playerInput.GetPushState() == PlayerInput.ActionButtonState.ButtonDown)
                _playerPush.PushPlayers();
            
            if (!_myPlayer.CanMove) return;
            //Movimiento
            _myPlayerMovement.DirX = _playerInput.GetLeftStickValues().x;
            
            if (_playerInput.GetJumpState() == PlayerInput.ActionButtonState.ButtonDown)
                _myPlayerMovement.StartJumping();

            else if (_playerInput.GetJumpState() != PlayerInput.ActionButtonState.ButtonPressed)
                _myPlayerMovement.StopJumping();

            if (_playerInput.GetLeftStickValues().y > .6f)
                _myPlayerMovement.StartJumping(true);
            else if(_playerInput.GetLeftStickValues().y <= .6f)
                _myPlayerMovement.StopJumpingWithAxis();
        }
        #endregion
        #region KeyBoard
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape)) LevelController.Instance.PauseGame(_playerInput);
            
            if (_myPlayer.IsStunned || _myPlayer.IsDying) return;
            //Apuntar y disparar
            _aimVector = Vector2.zero;
            _aimVector = _mainCamera.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            _myPlayerAttack.Aim(_aimVector.normalized);
           
            if(Input.GetMouseButton(0))
                _myPlayerAttack.StartShootingPrimaryWeapon();
            else if(Input.GetMouseButtonUp(0))
                _myPlayerAttack.ReleasePrimaryWeapon();
            
            if(Input.GetMouseButton(1))
                _myPlayerAttack.StartShootingSecondaryWeapon();
            else if(Input.GetMouseButtonUp(1))
                _myPlayerAttack.ReleaseSecondaryWeapon();
            
            //Empuje
            if (Input.GetKeyDown(KeyCode.LeftControl))
                _playerPush.PushPlayers();
            
            if(Input.GetKeyDown(KeyCode.Q))
                _myPlayerAttack.ThrowWeapon(0);
            
            if(Input.GetKeyDown(KeyCode.E))
                _myPlayerAttack.ThrowWeapon(1);
            
            if (!_myPlayer.CanMove) return;
            //Movimiento
            if (Input.GetKey(KeyCode.A) && Input.GetKeyDown(KeyCode.D)) _dirX = 0;
            else if (Input.GetKey(KeyCode.A)) _dirX = -1;
            else if (Input.GetKey(KeyCode.D)) _dirX = 1;
            else _dirX = 0;

            _myPlayerMovement.DirX = _dirX;
            
            if (Input.GetKeyDown(KeyCode.Space) )
                _myPlayerMovement.StartJumping();
            if(Input.GetKeyUp(KeyCode.Space))
                _myPlayerMovement.StopJumping();
        }
        #endregion
    }

    //Aca se agarra el arma del piso
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("PickUp"))
        {
            WeaponPickUp auxPickUp = collision.GetComponent<WeaponPickUp>();
            if(auxPickUp== null || !auxPickUp.CanBePicked) return;
            
            PlayerInput.ActionButtonState primary = _playerInput.GetChangePrimaryWeaponState();
            PlayerInput.ActionButtonState secondary = _playerInput.GetChangeSecondaryWeaponState();
            if (!_usingKeyboard)
            {
                if (primary == PlayerInput.ActionButtonState.ButtonDown)
                {
                    _myPlayerAttack.PickWeapon(auxPickUp, 0);
                }

                if (secondary == PlayerInput.ActionButtonState.ButtonDown)
                {
                    _myPlayerAttack.PickWeapon(auxPickUp, 1);
                }
            }
            else
            {
                if(Input.GetKeyDown(KeyCode.Q))
                    _myPlayerAttack.PickWeapon(auxPickUp, 0);
                
                if(Input.GetKeyDown(KeyCode.E))
                    _myPlayerAttack.PickWeapon(auxPickUp, 1);
            }
        }
    }

    public void ResetValues()
    {
        _currentLtState = false;
        _currentRtState = false;
    }
}
