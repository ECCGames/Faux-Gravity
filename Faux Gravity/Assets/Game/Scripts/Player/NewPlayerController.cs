﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayerController : PhysicsObject 
{

    [SerializeField] private LayerMask _layerToHit;
    [SerializeField] private int _maxLifes;
    [SerializeField] private float _jumpTakeOffSpeed = 250f;
    [SerializeField] private float _jumpDecayRate = 25f;
    [SerializeField] private ColliderChecker _colliderChecker;
    [SerializeField] private float _movementSpeed = 8f;
    [SerializeField] private GameObject _crossHair;
    [SerializeField] private float _aimResistance;
    [SerializeField] private SpriteRenderer _activeWeaponSprite;
    [SerializeField] private Sprite _portrait;
    [SerializeField] private Sprite _lifeSprite;
    [SerializeField] private float _crossHairMaxRange;
    [SerializeField] private Weapon[] _inventory;
    [SerializeField] private ParticleSystem _spawnParticles;
    [SerializeField] private WeaponPickUp pickUpPrefab;
    [SerializeField] private float _throwForce;
    [SerializeField] private float _immuneTime;

    private CharacterInfo _characterInfo;
    private float _actualJumpSpeed;
    bool _usingKeyboard;
    private int _actualLifes;
    public int ActualLifes { get { return _actualLifes; } }
    private float _previousPositionY;
    private SpriteRenderer _spriteRenderer;
    private Animator _animator;
    private Vector2 _aimVector;
    private Vector2 _move;
    private int _dir;
    private bool _jump;
    private bool _stopJump;
    private bool _jumpedWithAxis;
    private int _weaponCounter;
    private bool _isImmune;
    
    private bool previousLtState;
    private bool currentLtState;
    private bool previousRtState;
    private bool currentRtState;
    private bool isShootingPrimary;
    private bool isShootingSecondary;

    private PlayerInput _playerInput;

    public CharacterInfo CharacterInfo { get { return _characterInfo; } set { _characterInfo = value; } }

    public int Id { get { return (((int)_playerInput.PlayerIndex) + 1) ; } }
    public Sprite Portrait { get { return _portrait; } set { _portrait = value; } }
    public bool IsImmune { get { return _isImmune; } set { _isImmune = value; } }
	
    protected override  void Awake()
    {
        base.Awake();
        
        _playerInput = GetComponent<PlayerInput>();

        Rigid2D = GetComponent<Rigidbody2D>();
        //_respawnPosition = transform.position;
        _actualLifes = _maxLifes;
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        //_crossHair.transform.position = transform.position + new Vector3(1,0);
                  /*
        for (int i = 0; i < _inventory.Length; i++)
        {
            if (_inventory[i] != null) _inventory[i].SetPlayer(this);
        }
        */
    }

    private void Start()
    {
        if (Id == 0) _usingKeyboard = true;
    }

    public override void Update()
    {
        base.Update();

        Grounded = _colliderChecker.IsColliding;
        
        Debug.Log(Grounded);
        
        if (_isImmune)
        {
            _spriteRenderer.color = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, Mathf.Lerp(0, 1, Mathf.PingPong(Time.time * 2 , 1)));
            _activeWeaponSprite.color = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, Mathf.Lerp(0, 1, Mathf.PingPong(Time.time * 2, 1)));
        }
        #region Controller
        if (!_usingKeyboard)
        {
            _move.x = _playerInput.GetLeftStickValues().x * _movementSpeed;
            _aimVector = Vector2.zero;
            _aimVector = _playerInput.GetRightStickValues();
            Vector2 crossHairPosition = _aimVector.normalized * _crossHairMaxRange;


            for (int i = 0; i < _inventory.Length; i++)
            {
                if (_inventory[i] != null)
                {
                    /*
                    Debug.Log("isShootingPrimary : " + isShootingPrimary);
                    Debug.Log("isShootingSecondary : " + isShootingSecondary);
                    Debug.Log("TriggerValue 1 : " + _playerInput.TriggerValue(true));
                    Debug.Log("TriggerValue 2 : " + _playerInput.TriggerValue(false));
                    */
                    //Disparo primario arma
                    if ((i == 0 ? !isShootingSecondary : !isShootingPrimary) &&
                        (_playerInput.TriggerValue(i == 0) > 0.1f))
                    {
                        _inventory[i].PressFire(((Vector2)_crossHair.transform.position - (Vector2)transform.position).normalized);
                        if (i == 0)
                        {
                            currentRtState = true;
                            isShootingPrimary = true;
                        }
                        if (i == 1)
                        {
                            currentLtState = true;
                            isShootingSecondary = true;
                        }
                        //ChangeActiveWeaponSprite(_inventory[i]);
                    }
                    else
                    {
                        if (i == 0) currentRtState = false;
                        if (i == 1) currentLtState = false;
                    }

                    //Si solto el diparo 
                    if (i==0 ? !currentRtState  && previousRtState : !currentLtState && previousLtState)
                    {
                        _inventory[i].ReleaseFire(((Vector2)_crossHair.transform.position - (Vector2)transform.position).normalized);
                        if (i == 0) isShootingPrimary = false;
                        if (i == 1) isShootingSecondary = false;
                    }

                    //seteo los estados del gatillo
                    if (i == 0) previousRtState = currentRtState;
                    if (i == 1) previousLtState = currentLtState;
                }
            }

            if (Grounded && (_playerInput.GetJumpState() == PlayerInput.ActionButtonState.ButtonDown))
                _jump = true;
            else if (_playerInput.GetJumpState() == PlayerInput.ActionButtonState.ButtonUp)
                _jump = false;

            if (Grounded && _playerInput.GetLeftStickValues().y > .6f)
            {
                _jump = true;
                _jumpedWithAxis = true;
            }
            else if(_jumpedWithAxis && _playerInput.GetLeftStickValues().y <= .6f)
                _jump = false;

            //Reinicio el nivel
            /*if (_playerInput.GetStartState() == PlayerInput.ActionButtonState.ButtonPressed)
                GameController.Instance.ReloadScene();
                */

        }
        #endregion
        #region KeyBoard
        else
        {
            if (Input.GetKey(KeyCode.A) && Input.GetKeyDown(KeyCode.D)) _dir = 0;
            else if (Input.GetKey(KeyCode.A)) _dir = -1;
            else if (Input.GetKey(KeyCode.D)) _dir = 1;
            else _dir = 0;

            _move.x = _dir * _movementSpeed * Time.deltaTime;
            _move.y = transform.position.y;
            
            _aimVector = Vector2.zero;
            _aimVector = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;

            Vector2 crossHairPosition = _aimVector.normalized * _crossHairMaxRange;

            if (_aimVector != Vector2.zero)
            {
                _crossHair.transform.position = (Vector2)transform.position + crossHairPosition;
                _spriteRenderer.flipX = (_crossHair.transform.localPosition.x < 0);
            }

            if (_inventory != null)
            {
                if (!isShootingSecondary && Input.GetMouseButton(0))
                {
                    _inventory[0].PressFire(((Vector2)_crossHair.transform.position - (Vector2)transform.position).normalized);
                    isShootingPrimary = true;
                }
                //Suelto el click Izquierdo
                else if(Input.GetMouseButtonUp(0))
                {
                    _inventory[0].ReleaseFire(((Vector2)_crossHair.transform.position - (Vector2)transform.position).normalized);
                    isShootingPrimary = false;
                }

                if (!isShootingPrimary && Input.GetMouseButton(1))
                {
                    _inventory[1].PressFire(((Vector2)_crossHair.transform.position - (Vector2)transform.position).normalized);
                    isShootingSecondary = true;
                }
                //Suelto el click Izquierdo
                else if (Input.GetMouseButtonUp(1))
                {
                    _inventory[1].ReleaseFire(((Vector2)_crossHair.transform.position - (Vector2)transform.position).normalized);
                    isShootingSecondary = false;
                }
            }

            if (Grounded && Input.GetKey(KeyCode.Space) )
                _jump = true;
            else if(Input.GetKeyUp(KeyCode.Space))
                _jump = false;

            //Reinicio el nivel
            /*
            if (Input.GetKeyDown(KeyCode.Escape))
                GameController.Instance.ReloadScene();
                */
        }
        #endregion
        _animator.SetBool("falling", _previousPositionY > transform.position.y && !Grounded);
        _animator.SetBool("grounded", Grounded);
        _animator.SetFloat("velocityX", Mathf.Abs(Rigid2D.velocity.x) / _movementSpeed);
        _animator.SetFloat("velocityY", Rigid2D.velocity.y);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (Grounded) transform.position += (Vector3) _move;
        else transform.position += (Vector3) _move / 2;
        //Rigid2D.AddForce(_move, ForceMode2D.Impulse);
        if (_jump && _actualJumpSpeed > 0)
        {
            Rigid2D.AddForce(new Vector2(0, _actualJumpSpeed),ForceMode2D.Force);
            _actualJumpSpeed -= _jumpDecayRate;
        }
        else
        {
            _jump = false;
            _actualJumpSpeed = _jumpTakeOffSpeed;
        }
        _previousPositionY = transform.position.y;

    }
}
