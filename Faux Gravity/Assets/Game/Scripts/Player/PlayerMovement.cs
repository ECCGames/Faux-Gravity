﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour, IResettable 
{
	[SerializeField] private float _gravityForce;
	[SerializeField] private float _jumpSpeed;
	[SerializeField] private float _jumpEasing;
	[SerializeField] private ColliderChecker _groundedChecker;
	[SerializeField] private ColliderChecker _ceilingChecker;
	[SerializeField] private float _movementSpeed;
	[SerializeField] private float _easing;
	[SerializeField] private AudioClip _jumpSfx;

	private Player _myPlayer;
	private Rigidbody2D _rigid2D;
	private AudioSource _audioSource;
	
	private float _horizontalVelocity;
	private float _verticalVelocity;
	private float _dirX;
	private bool _jump;
	private bool _jumpedWithAxis;
	private float _actualJumpSpeed;
	
	private Vector2 _inertia;
	private Vector2 _velocity;

	public float MovementSpeed => _movementSpeed;
	public float DirX { get { return _dirX; } set { _dirX = value; } }
	public bool Jump => _jump;
    public Rigidbody2D Rigid { get { return _rigid2D; } }
	
	void Awake() 
	{
		_myPlayer = GetComponent<Player>();
		_rigid2D = GetComponent<Rigidbody2D>();
		_audioSource = GetComponent<AudioSource>();
		_velocity = Vector2.zero;
		_actualJumpSpeed = _jumpSpeed;
	}

	void Start()
	{
		_myPlayer.OnLifesChange.AddListener(ResetValues);
	}
	
	private void FixedUpdate()
	{
		if(_myPlayer.IsDying) return;
		
		_horizontalVelocity = _dirX * _movementSpeed;
		
		if (_groundedChecker.IsColliding)
		{
			_myPlayer.CanMove = true;
			_verticalVelocity = -_gravityForce;
			_actualJumpSpeed = _jumpSpeed;
		}

		if (_ceilingChecker.IsColliding)
		{
			_verticalVelocity = 0;
			_actualJumpSpeed = _jumpSpeed;
			_jumpedWithAxis = false;
			_jump = false;
		}
        
		if (_jump)
		{
			_horizontalVelocity += 3 * _dirX;
			_verticalVelocity += _actualJumpSpeed;
			_actualJumpSpeed *= _jumpEasing;
        }
        
		_verticalVelocity -= _gravityForce;
        
		_velocity.x = _horizontalVelocity;
		_velocity.y = _verticalVelocity;
		_rigid2D.velocity = _velocity + _inertia;
        		
		_inertia *= _easing;
	}
	
	public void StartJumping(bool axis = false)
	{
		if (_groundedChecker.IsColliding)
		{
			_audioSource.PlayOneShot(_jumpSfx);
			_jump = true;
			if (axis) _jumpedWithAxis = true;
		}
	}

	public void StopJumping()
	{
		if(!_jumpedWithAxis) _jump = false;
	}

	public void StopJumpingWithAxis()
	{
		if (_jumpedWithAxis)
		{
			_jump = false;
			_jumpedWithAxis = false;
		}
	}

    public void AddForce(Vector2 force)
    {
        if (!_myPlayer.IsDying)
        {
            _verticalVelocity = 0;
            _inertia += force;
        }
    }

	public bool GetGroundCollision()
	{
		return _groundedChecker.IsColliding;
	}

	public Vector2 GetVelocity()
	{
		return _rigid2D.velocity;
	}

	public void StopVelocity()
	{
		_dirX = 0;
	}
	
	public void ResetValues()
	{
		_verticalVelocity = 0;
		_horizontalVelocity = 0;
		_inertia = Vector2.zero;
		_velocity = Vector2.zero;
	}
}
