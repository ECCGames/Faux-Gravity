﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderChecker : MonoBehaviour
{
	[SerializeField] private string[] _layersToHit;
	
	private bool _isColliding;
	public bool IsColliding { get { return _isColliding; } }

	private Collider2D _actualCollider;
	
	private void OnTriggerStay2D(Collider2D col)
    {   
        string colLayer = LayerMask.LayerToName(col.gameObject.layer);
		for (int i = 0; i < _layersToHit.Length; i++)
		{
			if (_layersToHit[i] == colLayer && !col.isTrigger)
			{
				_isColliding = true;
				_actualCollider = col;
			}
		}

    }
	
	private void OnTriggerExit2D(Collider2D col)
	{
		string colLayer = LayerMask.LayerToName(col.gameObject.layer);
        for (int i = 0; i < _layersToHit.Length; i++)
        {
            if (_layersToHit[i] == colLayer)
            {
                _isColliding = false;
                _actualCollider = null;
            }
        }
	}

	private void Update()
	{
		if (_actualCollider == null || (_actualCollider != null && !_actualCollider.enabled))
            _isColliding = false;
	}
}
