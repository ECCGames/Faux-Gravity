﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Player : MonoBehaviour, IResettable {

	[SerializeField] private int _maxLifes;
    [SerializeField] private SpriteRenderer _activeWeaponSprite;
    [SerializeField] private ParticleSystem _spawnParticles;
    [SerializeField] private ParticleSystem _stunParticles;
    [SerializeField] private float _pushCooldown;
    [SerializeField] private float _immuneTime;

    [Header("WorldSpaceCanvasFields")]
    [SerializeField] Image[] _hearths;
    [SerializeField] Text _noAmmoText;
    [SerializeField] Text _firstWeaponBullets;
    [SerializeField] Text _secondWeaponBullets;
    [SerializeField] private Image _spawnPointer;
    [SerializeField] Text _playerText;
    [SerializeField] [Range(0.1f, 1f)] float _canvasTimeToFade;

    [Header("Character Info (only in tutorial)")]
    [SerializeField] private CharacterInfo _characterInfo;
    private float _timeToRespawn;
    private Color _playerColor;
    private int _playerNumber;
    private int _actualLifes;
    private float _previousPositionY;
    private SpriteRenderer _spriteRenderer;
    private Animator _animator;
    private bool _isImmune;
    private bool _isStunned;
    private bool _canMove = true;
    private bool _showPointer;
    private bool _isDying;
    private bool _canPush = true;
    private bool _isPushing;
    private bool _paused;

    private int _wins;
    private bool _lastWinner;

    private bool _hasFiredPrimaryWeapon;
    private bool _hasFiredSecondaryWeapon;
    private bool _hasNoAmmo;
    private bool _hasWeapon;
    
    private PlayerInput _playerInput;
    private UnityEvent _onLifesChange;
    private UnityEvent _onPauseChange;
    private PlayerDeathEvent _onPlayerDeath;
    private PlayerMovement _myPlayerMovement;
    private PlayerAttack _myPlayerAttack;

    public int ActualLifes { get { return _actualLifes; } }
    public CharacterInfo CharacterInfo { get { return _characterInfo; } set { _characterInfo = value; } }
    public PlayerAttack MyPlayerAttack => _myPlayerAttack;
    public PlayerInput PlayerInput { get { return _playerInput; } }
    public PlayerMovement MyPlayerMovement => _myPlayerMovement;
    public Color PlayerColor { get { return _playerColor; } }
    public int PlayerNumber => _playerNumber;
    public int Id { get { return (((int)_playerInput.PlayerIndex) + 1) ; } }
    public UnityEvent OnLifesChange { get { return _onLifesChange; } }
    public PlayerDeathEvent OnPlayerDeath { get { return _onPlayerDeath; } }
    public bool IsImmune { get { return _isImmune; } set { _isImmune = value; } }
    public bool IsStunned => _isStunned;
    public bool CanMove { get { return _canMove; } set { _canMove = value; } }
    public bool IsDying => _isDying;
    public bool CanPush => _canPush;
    public bool IsPushing { get { return _isPushing; } set { _isPushing = value; } }
    public bool Paused { get { return _paused; } set { _paused = value;} }

    public int Wins { get { return _wins; } set { _wins = value; } }
    public bool LastWinner { get { return _lastWinner; } set { _lastWinner = value; } }

    public GameObject GetWeaponPivot()
    {
        return _activeWeaponSprite.gameObject;
    }

    private void Awake()
    {
        _myPlayerMovement = GetComponent<PlayerMovement>();
        _myPlayerAttack = GetComponent<PlayerAttack>();
        
        _spawnPointer.gameObject.SetActive(false);
        _playerInput = GetComponent<PlayerInput>();

        _onPauseChange = new UnityEvent();
        _onLifesChange = new UnityEvent();
        _onPlayerDeath = new PlayerDeathEvent();
        
        for (int i = 0; i < _hearths.Length; i++)
        {
            _hearths[i].gameObject.SetActive(false);
        }
        
        //World Space Canvas Fields
        _firstWeaponBullets.gameObject.SetActive(false);
        _secondWeaponBullets.gameObject.SetActive(false);
        _noAmmoText.gameObject.SetActive(false);
        _hearths[0].transform.parent.gameObject.SetActive(false);
        
        //_respawnPosition = transform.position;
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if(_showPointer) 
            _playerText.color = new Color(_playerText.color.r, _playerText.color.g, _playerText.color.b, Mathf.Lerp(0, 1, Mathf.PingPong(Time.time * 3 , 1)));
        
        if (_isImmune)
        {
            Color _alphaLerper = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, Mathf.Lerp(0, 1, Mathf.PingPong(Time.time * 3 , 1)));
            _spriteRenderer.color = _alphaLerper;
            _activeWeaponSprite.color = _alphaLerper;
        }

        if (_hasWeapon) _spriteRenderer.flipX = (_myPlayerAttack.CrossHair.transform.localPosition.x <= 0);
        else
        {
            if(_myPlayerMovement.DirX < 0) 
                _spriteRenderer.flipX = true;
            else if (_myPlayerMovement.DirX > 0)
                _spriteRenderer.flipX = false;
        }
        
        //HOTFIX CABEZA PARA EL FINAL
        if(!_isImmune && !_spawnParticles.gameObject.activeSelf)
            _spawnPointer.gameObject.SetActive(true);
        
        if (_isDying) return;
        
        _animator.SetBool("falling", _previousPositionY > transform.position.y && !_myPlayerMovement.GetGroundCollision());
        _animator.SetBool("grounded", _myPlayerMovement.GetGroundCollision());
        _animator.SetFloat("velocityX", Mathf.Abs(_myPlayerMovement.GetVelocity().x) / _myPlayerMovement.MovementSpeed);
        _animator.SetFloat("velocityY", _myPlayerMovement.GetVelocity().y);
        _animator.SetBool("pushing", _isPushing);
    }
    
    private void FixedUpdate()
    {
        _previousPositionY = transform.position.y;
    }
    
    public void Push()
    {
        if (!_canPush) return;

        _animator.runtimeAnimatorController = _characterInfo.CharacterHandAnimator;
        _activeWeaponSprite.gameObject.SetActive(false);
        _isPushing = true;
        _canPush = false;
        
        Invoke("ResetPushing", 0.5f);
        Invoke("ResetCanPush", _pushCooldown);
    }

    private void ResetPushing()
    {
        ChangeActiveWeaponSprite(_myPlayerAttack.Inventory[0]);
        _isPushing = false;
    }

    private void ResetCanPush()
    {
        _canPush = true;
    }
    
    public void KillPlayer()
    {
        if (!_isDying)
        {
            _isDying = true;
            _actualLifes--;
            _onLifesChange.Invoke();
            _animator.runtimeAnimatorController = _characterInfo.CharacterDeathAnimator;
            _myPlayerMovement.ResetValues();

            Invoke("ResetValues", _timeToRespawn);
        }
    }

    public void ResetImmune()
    {
        _hearths[0].transform.parent.gameObject.SetActive(false);
        _isImmune = false;
        _playerText.gameObject.SetActive(false);
        _showPointer = false;
        _spawnPointer.gameObject.SetActive(true);
        
        _playerText.color = new Color(_playerText.color.r, _playerText.color.g, _playerText.color.b, 1f);
        _spriteRenderer.color = new Color(_spriteRenderer.color.r, _spriteRenderer.color.g, _spriteRenderer.color.b, 1f);
        _activeWeaponSprite.color = new Color(_activeWeaponSprite.color.r, _activeWeaponSprite.color.g, _activeWeaponSprite.color.b, 1f);
    }

    public void ResetStun()
    {
        _isStunned = false;
        _canMove = true;
    }

    public void ResetMove()
    {
        _canMove = true;
    }
    
    //TODO: Cambiar esto para futuro
    public void SetPlayer(PlayerSettings settings)
    {
        if (settings != null)
        {
            _playerInput.SetIndex((XInputDotNetPure.PlayerIndex) (settings.ControllerId - 1));
            _characterInfo = settings.CharacterInfo;
            //Obtengo el sprite del arma que tiene actualmente asignada
            ChangeActiveWeaponSprite(_myPlayerAttack.Inventory[0]);
            _maxLifes = settings.MaxPlayerLifes;
            _actualLifes = _maxLifes;
            _timeToRespawn = _characterInfo.TimeToRespawn;
            _playerColor = settings.PlayerColor;
            _playerNumber = settings.PlayerNumber;

            _wins = settings.AmountOfWins;
            _lastWinner = settings.LastWinner;
        }
        else
        {
            _timeToRespawn = 0.1f;
            _playerColor = Color.cyan;
            _playerNumber = 1;
        }
        _spawnPointer.color = _playerColor;

        for (int i = 0; i < _maxLifes; i++)
        {
            _hearths[i].gameObject.SetActive(true);
            _hearths[i].color = PlayerColor;
        }
        
        _spawnParticles.GetComponent<ParticleSystemRenderer>().material.SetColor("_Color", _playerColor);

        _playerText.text = "P" + _playerNumber;
        _playerText.color = _playerColor;
        
        
        _spawnPointer.gameObject.SetActive(false);
        _playerText.gameObject.SetActive(true);
        _showPointer = true;
        Invoke(nameof(ResetImmune), _immuneTime);
    }

    public void AddForce(Vector2 force, bool ignoreImmune = false)
    {
        if(!IsDying || !_isImmune || ignoreImmune) _myPlayerMovement.AddForce(force);
        if (!ignoreImmune)
        {
            _canMove = false;
            Invoke("ResetMove", 1f);
        }
    }

    public void Stun(float stunTime)
    {
        //TODO: hacer diminishing return del stun
        if (_isStunned) return;
        
        _isStunned = true;
        _myPlayerMovement.StopVelocity();
        
        var main = _stunParticles.main;
        _stunParticles.Stop();
        main.duration = stunTime;
        _stunParticles.Play();
        Invoke("ResetStun", stunTime);
    }
    
    public void FiredFirstWeapon()
    {
        if (_myPlayerAttack.Inventory[0] != null)
        {
            int bullets = _myPlayerAttack.Inventory[0].GetBullets();
            if (bullets == 0)
                DisplayNoAmmo();
            else
            {
                DisplayFirstWeaponBullets();
                _firstWeaponBullets.text = bullets.ToString();
            }
        }
    }

    public void ChangeFirstWeaponFireStatus()
    {
        _hasFiredPrimaryWeapon = false;
        _firstWeaponBullets.gameObject.SetActive(false);
    }

    public void FiredSecondWeapon()
    {
        if (_myPlayerAttack.Inventory[1] != null)
        {
            int bullets = _myPlayerAttack.Inventory[1].GetBullets();
            if (bullets == 0)
                DisplayNoAmmo();
            else
            {
                DisplaySecondWeaponBullets();
                _secondWeaponBullets.text = bullets.ToString();
            }
        }
    }

    public void ChangeSecondWeaponFireStatus()
    {
        _hasFiredSecondaryWeapon = false;
        _secondWeaponBullets.gameObject.SetActive(false);
    }

    public void DisplayNoAmmo()
    {
        CancelInvoke("ChangeNoAmmoStatus");
        _noAmmoText.gameObject.SetActive(true);
        Invoke("ChangeNoAmmoStatus", 1f);
    }

    public void DisplayFirstWeaponBullets()
    {
        CancelInvoke("ChangeFirstWeaponFireStatus");
        _firstWeaponBullets.gameObject.SetActive(true);
        _hasFiredPrimaryWeapon = true;
        Invoke("ChangeFirstWeaponFireStatus", _canvasTimeToFade);
    }

    public void DisplaySecondWeaponBullets()
    {
        CancelInvoke("ChangeFirstWeaponFireStatus");
        _secondWeaponBullets.gameObject.SetActive(true);
        _hasFiredSecondaryWeapon = true;
        Invoke("ChangeSecondWeaponFireStatus", _canvasTimeToFade);
    }

    public void ChangeNoAmmoStatus()
    {
        _noAmmoText.gameObject.SetActive(false);
    }

    public void ChangeActiveWeaponSprite(Weapon newWeapon)
    {
        if (CharacterInfo != null)
        {
            if (newWeapon == null)
            {
                _activeWeaponSprite.gameObject.SetActive(false);
                _animator.runtimeAnimatorController = _characterInfo.CharacterHandAnimator;
                _hasWeapon = false;
            }
            else
            {
                if (!_activeWeaponSprite.gameObject.activeSelf)
                {
                    _activeWeaponSprite.gameObject.SetActive(true);
                    _animator.runtimeAnimatorController = _characterInfo.CharacterHandlessAnimator;
                    _hasWeapon = true;
                }
                var weaponHands = _characterInfo.GetWeaponHands(newWeapon.WeaponName);
                if (weaponHands != null) _activeWeaponSprite.sprite = weaponHands.weaponHandSprite;
                else Debug.LogWarning("Falta asignar las manos con el arma al personaje");
            }
        }        
    }

    /// <summary>
    /// Called on Player´s death
    /// </summary>
    public void ResetValues()
    { 
        if (_actualLifes <= 0)
        {
            _onPlayerDeath.Invoke(this);
            gameObject.SetActive(false);
        }
        else
        {
            CancelInvoke("ResetMove");
            CancelInvoke("ResetImmune");
            _hearths[_actualLifes].gameObject.SetActive(false);
            _spawnParticles.Stop();
            _spawnParticles.Play();
            _myPlayerMovement.Rigid.velocity = Vector2.zero;
            transform.position = LevelController.Instance.RespawnPlayer();
            _isImmune = true;
            _spawnPointer.gameObject.SetActive(false);
            _playerText.gameObject.SetActive(true);
            _showPointer = true;
            _canMove = false;
            _isDying = false;
            Invoke("ResetMove", .7f);
            Invoke("ResetImmune", _immuneTime);

            _hearths[0].transform.parent.gameObject.SetActive(true);
            ChangeActiveWeaponSprite(_myPlayerAttack.Inventory[0]);
        }
    }
}

public class PlayerDeathEvent : UnityEvent<Player> { }

public interface IResettable
{
    void ResetValues();
}