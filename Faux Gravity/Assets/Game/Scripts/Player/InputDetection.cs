﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputDetection : MonoBehaviour {
    
    [SerializeField] int id;

    //despues se pueden hacer unity events cuando apretas cada tecla

    [HideInInspector] public bool pressedA;
    bool previousPressedA;
    [HideInInspector] public bool GetADown; 

    [HideInInspector] public bool pressedB;
    [HideInInspector] public bool pressedX;
    [HideInInspector] public bool pressedY;
    //R1
    [HideInInspector] public bool pressedR;
    //L1
    [HideInInspector] public bool pressedL;
    //Start
    [HideInInspector] public bool pressedS;

    //Left Joystick
    [HideInInspector] public float primaryXAxis;
    [HideInInspector] public float primaryYAxis;
    //Right Joystick
    [HideInInspector] public float secondaryXAxis;
    [HideInInspector] public float secondaryYAxis;
    //R2 L2
    [HideInInspector] public float RightTrigger;
    [HideInInspector] public float LeftTrigger;

    // Update is called once per frame
    void Update () {

        previousPressedA = pressedA;
        pressedA = Input.GetButton(id + "A");
        GetADown = !previousPressedA && pressedA;
        pressedB = Input.GetButton(id + "B");
        pressedX = Input.GetButton(id + "X");
        pressedY = Input.GetButton(id + "Y");
        pressedR = Input.GetButton(id + "R");
        pressedL = Input.GetButton(id + "L");
        pressedS = Input.GetButton(id + "S");
        primaryXAxis = Input.GetAxis(id + "H");
        primaryYAxis = Input.GetAxis(id + "V");
        secondaryXAxis = Input.GetAxis(id + "SH");
        secondaryYAxis = Input.GetAxis(id + "SV");
        RightTrigger = Input.GetAxis(id + "RT");
        LeftTrigger = Input.GetAxis(id + "LT");

    }
    
}
