﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerAttack : MonoBehaviour, IResettable 
{
	[SerializeField] private GameObject _crossHair;
	[SerializeField] private Transform _bulletOrigin;
	[SerializeField] private float _aimResistance;
	[SerializeField] private Vector2 _crosshairOffset;
	[SerializeField] private float _crossHairMaxRange;
	[SerializeField] private WeaponPickUp _pickUpPrefab;
	[SerializeField] private float _throwForce;
    [SerializeField] private float _addTorque;
	[SerializeField] private PickUpFloatingText _floatingTextPrefab;
    [SerializeField] private float _minMovMagnitude = 0.1f;
    [SerializeField] private float _aimVelocity = .05f;

	private Vector2 _aimVector;
	private Vector2 _crossHairPos;
	private Vector2 _prevDir;
	private float _timer;
	private float _maxTimer;
	private float _lerpTimer;
	private float _distance;

	private bool _isShootingPrimary;
	private bool _isShootingSecondary;
	
	private Weapon[] _inventory;
	private Player _myPlayer;
	
	private WeaponPickUpEvent _onWeaponPickUp;

	public GameObject CrossHair => _crossHair;
	public Transform BulletOrigin => _bulletOrigin;
	public Weapon[] Inventory { get { return _inventory; } }
	public WeaponPickUpEvent OnWeaponPickUp { get { return _onWeaponPickUp; } }
	
	void Awake () 
	{
		_myPlayer = GetComponent<Player>();
		_inventory = new Weapon[2];
		_crossHair.transform.position = transform.position + new Vector3(2,0);
		
		for (int i = 0; i < _inventory.Length; i++)
		{
			if (_inventory[i] != null) _inventory[i].SetPlayer(_myPlayer);
		}
		
		_onWeaponPickUp = new WeaponPickUpEvent();
	}

	void Start()
	{
		_myPlayer.OnLifesChange.AddListener(ResetValues);	
	}

	public void Aim(Vector2 dir)
    {
        _timer += Time.deltaTime;
        if (dir.magnitude < _aimResistance) return;

		_aimVector = (Vector2)transform.position + dir.normalized * _crossHairMaxRange + _crosshairOffset;
        _distance = Vector3.Distance(_crossHair.transform.position, _aimVector);

        if (dir != _prevDir)
        {
            _timer = 0;
            _timer += Time.deltaTime;
            _maxTimer = _distance / _aimVelocity;
        }

        _lerpTimer = _timer / _maxTimer;
        _crossHair.transform.position = Vector2.Lerp(_crossHair.transform.position, _aimVector, _lerpTimer);
        _prevDir = dir;

    }
	
	public void StartShootingPrimaryWeapon()
	{
		if (!_isShootingSecondary && _inventory[0] != null)
		{
			_inventory[0].PressFire(((Vector2) _bulletOrigin.transform.position - (Vector2) _myPlayer.GetWeaponPivot().transform.position).normalized);
			_isShootingPrimary = true;
			_myPlayer.ChangeActiveWeaponSprite(_inventory[0]);
		}
	}

	public void ReleasePrimaryWeapon()
	{
		if (!_isShootingSecondary && _isShootingPrimary)
		{
			_inventory[0].ReleaseFire(((Vector2) _bulletOrigin.transform.position - (Vector2) _myPlayer.GetWeaponPivot().transform.position).normalized);
			_isShootingPrimary = false;
		}
	}
	
	public void StartShootingSecondaryWeapon()
	{
		if (!_isShootingPrimary && _inventory[1] != null)
		{
			_inventory[1].PressFire(((Vector2) _bulletOrigin.transform.position - (Vector2) _myPlayer.GetWeaponPivot().transform.position).normalized);
			_isShootingSecondary = true;
			_myPlayer.ChangeActiveWeaponSprite(_inventory[1]);
		}
	}

	public void ReleaseSecondaryWeapon()
	{
		if (!_isShootingPrimary && _isShootingSecondary)
		{
			_inventory[1].ReleaseFire(((Vector2) _bulletOrigin.transform.position - (Vector2) _myPlayer.GetWeaponPivot().transform.position).normalized);
			_isShootingSecondary = false;
		}
	}

	public void PickWeapon(WeaponPickUp pickUp, int index)
	{
		//Si tengo un arma en esa posicion del inventario, creo un pickup
		if (_inventory[index] != null) ThrowWeapon(index);

		_inventory[index] = Instantiate(pickUp.GetWeapon(), transform.position, Quaternion.identity, transform);
		_inventory[index].SetPlayer(_myPlayer);
		
		PickUpFloatingText floatingText = Instantiate(_floatingTextPrefab, transform.position, Quaternion.identity);
		floatingText.FloatingText.text = "+" + _inventory[index].FloatingText;
		floatingText.FloatingText.color = _inventory[index].GetBulletsUIColor();
		
		if (index == 0)
		{
			_inventory[index].OnBulletsChange.AddListener(_myPlayer.FiredFirstWeapon);
			//_firstWeaponBullets.color = _inventory[index].GetBulletsUIColor();
		}
		else
		{
			_inventory[index].OnBulletsChange.AddListener(_myPlayer.FiredSecondWeapon);
			//_secondWeaponBullets.color = _inventory[index].GetBulletsUIColor();
		}

		_myPlayer.ChangeActiveWeaponSprite(_inventory[index]);
		_onWeaponPickUp.Invoke(_inventory[index], index);
	}
	
	public void ThrowWeapon(int i)
	{
		if(_inventory[i] == null) return;
		
		WeaponPickUp pickUp = Instantiate(_pickUpPrefab, _crossHair.transform.position , Quaternion.identity);
		pickUp.SetWeapon(_inventory[i]);
		pickUp.Owner = _myPlayer;
		pickUp.Rigid.AddForce(((Vector2)_crossHair.transform.position - (Vector2)transform.position).normalized * _throwForce);
        pickUp.Rigid.AddTorque(_addTorque);
		_inventory[i].transform.parent = pickUp.transform;
		
		_inventory[i] = null;
		
		_onWeaponPickUp.Invoke(_inventory[i], i);
		_myPlayer.ChangeActiveWeaponSprite(_inventory[i]);
	}
	
	public Vector3 GetCrossHairPosition()
	{
		return (_crossHair.transform.position - transform.position).normalized;
	}

	public void ResetValues()
	{
		for (int i = 0; i < _inventory.Length; i++)
		{
			if (_inventory[i] != null)
			{
				//Detengo cualquier vibracion que haya quedado en juego
				_inventory[i].ResetWeapon();
				ThrowWeapon(i);
				_inventory[i] = null;
				_onWeaponPickUp.Invoke(_inventory[i],i);
			}
		}
		
		_isShootingPrimary = false;
		_isShootingSecondary = false;
	}
}

public class WeaponPickUpEvent : UnityEvent<Weapon, int> { }
