﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class PlayerPush : MonoBehaviour {

    [SerializeField] float _pushForce;
    [SerializeField] private Vector3 _xOffSet;

    private Player _myPlayer;
    private SpriteRenderer _playerSprite;
    List<Player> _players;
    private bool _flipped;
    
    private void Awake()
    {
        _myPlayer = GetComponentInParent<Player>();
        _playerSprite = GetComponentInParent<SpriteRenderer>();
        
        _players = new List<Player>();
        GetComponent<BoxCollider2D>().isTrigger = true;
        transform.position += _xOffSet;
    }

    private void Update()
    {
        if (_playerSprite.flipX && !_flipped)
        {
            transform.position -= _xOffSet * 2;
            _flipped = true;
        }
        else if (!_playerSprite.flipX && _flipped)
        {
            transform.position += _xOffSet * 2;
            _flipped = false;
        }
    }
    
    public void PushPlayers()
    {
        if (!_myPlayer.CanPush) return;
        
        _myPlayer.Push();
        for (int i = 0; i < _players.Count; i++)
        {
            if (_players != null) _players[i].AddForce((_players[i].transform.position - _myPlayer.transform.position).normalized * _pushForce);
            else _players.Remove(_players[i]);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            var aux = collision.GetComponent<Player>();
            if (!_players.Contains(aux)) _players.Add(aux);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            var aux = collision.GetComponent<Player>();
            if (_players.Contains(aux)) _players.Remove(aux);
        }
    }
}
