﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionNode : MonoBehaviour {

    [SerializeField] PositionNode nextNode;
    [SerializeField] PositionNode previousNode;
    public PositionNode NextNode { get { return nextNode; } }
    public PositionNode PreviousNode { get { return previousNode; } }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        if (nextNode == null || previousNode == null) Gizmos.color = Color.magenta;

        Debug.DrawLine(transform.position, nextNode.transform.position);
        Debug.DrawLine(transform.position, previousNode.transform.position);
    }

}
