﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] Platform empty;

    PixelStatusWrapper[,] pixelArray;

    public PixelStatusWrapper[,] PixelArray { get { return pixelArray; } set { pixelArray = value; } }

    public void DestroyPixels(List<GameObject> destroyedPixels)
    {
        List<PixelStatusWrapper> alivePixels = new List<PixelStatusWrapper>();
        int counter = 0;
        for (int x = 0; x < pixelArray.GetLongLength(0); x++)
        {
            for (int y = 0; y < pixelArray.GetLongLength(1); y++)
            {
                for (int i = 0; i < destroyedPixels.Count; i++)
                {
                    //optimizacion?
                    if (counter == destroyedPixels.Count) break;
                    //busco los pixeles y los seteo como muertos
                    if (pixelArray[x, y].pixel == destroyedPixels[i])
                    {
                        pixelArray[x, y].alive = false;
                        MonoBehaviour.Destroy(destroyedPixels[i].gameObject);
                        counter++;
                        break;
                    }
                }
                //Agrego los vivos
                if (pixelArray[x, y].alive)
                    alivePixels.Add(pixelArray[x, y]);
            }
        }
        CheckPlatformStatus(alivePixels);
    }

    void CheckPlatformStatus(List<PixelStatusWrapper> alivePixels)
    {
        //los que hay que recorrer
        List<PixelStatusWrapper> openPixels = new List<PixelStatusWrapper>();
        //los que ya fueron recorridos
        List<PixelStatusWrapper> closePixels = new List<PixelStatusWrapper>();

        PixelStatusWrapper currentPixel = alivePixels[0];
        openPixels.Add(currentPixel);

        while (openPixels.Count > 0)
        {
            int x = (int)currentPixel.position.x;
            int y = (int)currentPixel.position.y;
            //Agrego los aledaños
            //tienen que estar vivos, no tienen que estar en la lista de abiertos ni cerrados y tienen que estar dentro del array
            if (!closePixels.Contains(currentPixel))
            {
                //izquierda
                if (x - 1 >= 0 &&
                    pixelArray[x - 1, y].alive &&
                    !openPixels.Contains(pixelArray[x - 1, y]) &&
                    !closePixels.Contains(pixelArray[x - 1, y]))
                    openPixels.Add(pixelArray[x - 1, y]);
                //derecha
                if (x + 1 < pixelArray.GetLongLength(0) &&
                    pixelArray[x + 1, y].alive &&
                    !openPixels.Contains(pixelArray[x + 1, y]))
                    openPixels.Add(pixelArray[x + 1, y]);
                //arriba
                if (y - 1 >= 0 &&
                    pixelArray[x, y - 1].alive &&
                    !openPixels.Contains(pixelArray[x, y - 1]))
                    openPixels.Add(pixelArray[x, y - 1]);
                //abajo
                if (y + 1 < pixelArray.GetLongLength(1) &&
                    pixelArray[x, y + 1].alive &&
                    !openPixels.Contains(pixelArray[x, y + 1]))
                    openPixels.Add(pixelArray[x, y + 1]);

                closePixels.Add(currentPixel);
            }
            openPixels.Remove(currentPixel);
            if (openPixels.Count > 0) currentPixel = openPixels[0];
        }

        if (closePixels.Count == alivePixels.Count) Debug.Log("esta entero");
        //llamo de manera recursiva el metodo para subdividir la estructura
        if (closePixels.Count < alivePixels.Count)
        {
            Debug.Log("esta partido");
            Platform newplatform = Instantiate(empty, transform.position, Quaternion.identity);
            //newplatform.name = "New Platform";
            for (int i = 0; i < closePixels.Count; i++)
            {
                closePixels[i].pixel.transform.parent = newplatform.transform;
                alivePixels.Remove(closePixels[i]);
            }
            //CheckPlatformStatus(alivePixels);
        }
    }

}