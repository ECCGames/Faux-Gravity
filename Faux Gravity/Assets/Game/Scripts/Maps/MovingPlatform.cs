﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    [SerializeField] PositionNode _actualNode;
    [SerializeField] [Range(0.1f,10)] float _timeBetweenNodes;
    [SerializeField] bool _direction;

    PositionNode _nextNode;
    float _timer;

    private void Start()
    {
        _nextNode = _direction ? _actualNode.NextNode : _actualNode.PreviousNode;
    }

    void Update () {

        _timer += Time.deltaTime;
        transform.position = Vector3.Lerp(_actualNode.transform.position, _nextNode.transform.position, _timer / _timeBetweenNodes);

        if(_timer >= _timeBetweenNodes)
        {
            transform.position = _nextNode.transform.position;
            _actualNode = _nextNode;
            _nextNode = _direction? _actualNode.NextNode : _actualNode.PreviousNode;
            _timer = 0;
        }
	}
}
