﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGeneration : MonoBehaviour {

    [SerializeField] List<Sprite> PlatformSprites;
    [SerializeField] int width, height;
    [SerializeField] GameObject prefab;
    [SerializeField] string destructibleTag;
    [SerializeField] Platform platform;
    float spriteWidth, spriteHeight;

    private void Start()
    {
        GeneratePlatform();
    }

    public void GeneratePlatform()
    {
        if (PlatformSprites.Count != width * height) Debug.LogWarning("No es correcta la cantidad de sprites asignados con el ancho y el largo");

        PixelStatusWrapper[,] pixelArray = new PixelStatusWrapper[width, height];
        spriteWidth = PlatformSprites[0].bounds.size.x;
        spriteHeight = PlatformSprites[0].bounds.size.y;
        Vector3 offsetX = new Vector3(spriteWidth, 0 ,0);
        Vector3 offsetY = new Vector3(0, spriteHeight, 0);
        Vector3 YTallestPoint = offsetY * height;

        Platform parent = Instantiate(platform, transform.position, Quaternion.identity);
        parent.gameObject.name = "Platform";
        int sprCounter = 0;
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                GameObject spr = Instantiate(prefab,transform.position + offsetX * x + (YTallestPoint - offsetY * y) , Quaternion.identity, parent.transform);
                spr.name = x + ":" + y;
                spr.tag = destructibleTag;
                spr.GetComponent<SpriteRenderer>().sprite = PlatformSprites[sprCounter];
                spr.AddComponent<BoxCollider2D>();
                //lo agrego al array
                pixelArray[x, y] = new PixelStatusWrapper(spr, true, new Vector2(x,y));
                sprCounter++;
            }
        }
        parent.PixelArray = pixelArray;
    }
}

[System.Serializable]
public class PixelStatusWrapper
{
    public GameObject pixel;
    public bool alive;
    public Vector2 position;

    public PixelStatusWrapper(GameObject pixel, bool status, Vector2 position)
    {
        this.pixel = pixel;
        alive = status;
        this.position = position;
    }
}