﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewTerrainGenerator : MonoBehaviour {

    public enum TerrainGeneratorComplexity { Simple, Complex, Complex2, Image}

    [Header("Left sprites")]
    [SerializeField] private List<Sprite> LeftTopCorner;
    [SerializeField] private List<Sprite> LeftMiddleCenter;
    [SerializeField] private List<Sprite> LeftBottomCorner;
    [Header("Right sprites")]
    [SerializeField] private List<Sprite> RightTopCorner;
    [SerializeField] private List<Sprite> RightMiddleCenter;
    [SerializeField] private List<Sprite> RightBottomCorner;
    [Header("Middle sprites")]
    [SerializeField] private List<Sprite> MiddleTopCenter;
    [SerializeField] private List<Sprite> MiddleBottomCenter;
    [Header("Fill sprite")]
    [SerializeField] private List<Sprite> MiddleFiller;

    [Space(10)][Header("Generation Values")]
    [SerializeField] private GameObject prefab;
    [SerializeField] private TerrainGeneratorComplexity complexity;
    [SerializeField] private int width;
    [SerializeField] private int height;

    public TerrainGeneratorComplexity Complexity { get { return complexity; } }

    public void GeneratePlatform(int[,] array = null)
    {
        //Podria agarrar cualquier imagen y es lo mismo, todas deberian medir lo mismo
        float spriteWidth = LeftTopCorner[0].bounds.size.x;
        float spriteHeight = LeftTopCorner[0].bounds.size.y;
        Vector3 offsetX = new Vector3(spriteWidth, 0, 0);
        Vector3 offsetY = new Vector3(0, spriteHeight, 0);
        Vector3 YTallestPoint = offsetY * height;

        if (complexity == TerrainGeneratorComplexity.Simple)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    GameObject spr = Instantiate(prefab, transform.position + offsetX * x + (YTallestPoint - offsetY * y), Quaternion.identity, transform);
                    spr.name = x + ":" + y;
                    spr.tag = "Destructible";
                    spr.GetComponent<SpriteRenderer>().sprite = GetPositionImage(x,y);
                    spr.AddComponent<BoxCollider2D>();
                }
            }
        }
        if(complexity == TerrainGeneratorComplexity.Complex)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (array[x, y] == 1)
                    {
                        GameObject spr = Instantiate(prefab, transform.position + offsetX * x + (YTallestPoint - offsetY * y), Quaternion.identity, transform);
                        spr.name = x + ":" + y;
                        spr.tag = "Destructible";
                        spr.GetComponent<SpriteRenderer>().sprite = GetPositionImage(x, y);
                        spr.AddComponent<BoxCollider2D>();
                    }
                }
            }
        }
        if (complexity == TerrainGeneratorComplexity.Complex2)
        {
            for (int y = 0; y < height; y++)
            {
                //Debug.Log(array[width, height - 1]);
                DrawLine(array, y, offsetY, offsetX, YTallestPoint, array[width, height-1]);
            }
        }
    }

    void DrawLine(int[,] array, int index, Vector3 offsetY, Vector3 offsetX,Vector3 YTallestPoint, float repeatAmount = 1)
    {
        for (int i = 0; i < repeatAmount; i++)
        {
            for (int x = 0; x < width; x++)
            {
                if (array[x,index] == 1)
                {
                    GameObject spr = Instantiate(prefab, transform.position + offsetX * x + (YTallestPoint - offsetY * index), Quaternion.identity, transform);
                    spr.name = x + ":" + index;
                    spr.tag = "Destructible";
                    spr.GetComponent<SpriteRenderer>().sprite = GetPositionImage(x, index);
                    spr.AddComponent<BoxCollider2D>();
                }
            }
        }
    }

    Sprite GetPositionImage(int x, int y)
    {
        if (x == 0 && y == 0)
            return LeftTopCorner[Random.Range(0, LeftTopCorner.Count)];
        if (x == width - 1 && y == 0)
            return RightTopCorner[Random.Range(0, RightTopCorner.Count)];
        if (x == 0 && y == height - 1)
            return LeftBottomCorner[Random.Range(0, LeftBottomCorner.Count)];
        if (x == width - 1 && y == height - 1)
            return RightBottomCorner[Random.Range(0, RightBottomCorner.Count)];
        if (x == 0)
            return LeftMiddleCenter[Random.Range(0, LeftMiddleCenter.Count)];
        if (y == 0)
            return MiddleTopCenter[Random.Range(0, MiddleTopCenter.Count)];
        if (x == width - 1)
            return RightMiddleCenter[Random.Range(0, RightMiddleCenter.Count)];
        if (y == height - 1)
            return MiddleBottomCenter[Random.Range(0, MiddleTopCenter.Count)];
        else
            return MiddleFiller[Random.Range(0, MiddleFiller.Count)];
    }
}
