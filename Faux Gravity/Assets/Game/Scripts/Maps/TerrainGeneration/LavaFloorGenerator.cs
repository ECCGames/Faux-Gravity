﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaFloorGenerator : MonoBehaviour {

    [SerializeField] List<Lava> upperLavaTiles;
    [SerializeField] Lava underneathLavaTile;
    [SerializeField] int width;
    [SerializeField] int heigth;

    float upperLavaSpriteWidth;
    float upperLavaSpriteHeigth;
    float underneathLavaSpriteWidth;
    float underneathLavaSpriteHeigth;

    public void CreateLavaFloor()
    {
        GameObject parent = Instantiate(new GameObject(),transform.position,Quaternion.identity);
        parent.name = "LavaFloor";

        upperLavaSpriteWidth = upperLavaTiles[0].GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        upperLavaSpriteHeigth = upperLavaTiles[0].GetComponent<SpriteRenderer>().sprite.bounds.size.y;
        underneathLavaSpriteWidth = underneathLavaTile.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        underneathLavaSpriteHeigth = underneathLavaTile.GetComponent<SpriteRenderer>().sprite.bounds.size.y;

        Vector3 lavaTopOffsetX = new Vector3(upperLavaSpriteWidth, 0, 0);
        Vector3 lavaTopOffsetY = new Vector3(0, upperLavaSpriteHeigth, 0);
        Vector3 lavaUnderneathOffsetX = new Vector3(underneathLavaSpriteWidth, 0, 0);
        Vector3 lavaUnderneathOffsetY = new Vector3(0, underneathLavaSpriteHeigth, 0);
        Vector3 xOffset = new Vector3(0.05f, 0, 0);
        Vector3 yOffset = new Vector3(0, 0.05f, 0);
        int index = 0;
        //Upper Lava
        for (int i = 0; i < width; i++)
        {
            Lava spr = Instantiate(upperLavaTiles[index], transform.position + lavaTopOffsetX * i, Quaternion.identity,parent.transform);
            index++;
            if (index == upperLavaTiles.Count) index = 0;
        }
        //Underneath Lava
        for (int i = 0; i < heigth; i++)
        {
            for (int j = 0; j < width; j++)
            {
                Lava spr = Instantiate(underneathLavaTile,
                                                transform.position - lavaTopOffsetY  - lavaUnderneathOffsetY * i + lavaUnderneathOffsetX * j,
                                                Quaternion.identity, parent.transform);
            }
            
        }
    }
}
