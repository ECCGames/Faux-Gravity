﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewTerrainGenerator2 : MonoBehaviour
{
    enum Position { Left, Middle, Right}

    [Header("Prefab")]
    [SerializeField] GameObject terrainPrefab;

    [Header("Sprites")]
    [SerializeField] Sprite _leftCornerSprite;
    [SerializeField] Sprite _rightCornerSprite;
    [SerializeField] Sprite _middleSprite;

    [Header("Size")]
    [SerializeField] private int width;

    public void GenerateTerrain()
    {
        float spriteWidthLeft = _leftCornerSprite.bounds.size.x;
        float spriteWidthMiddle = _middleSprite.bounds.size.x;
        Vector3 offsetXLeft = new Vector3(spriteWidthLeft, 0, 0);
        Vector3 offsetXMiddle = new Vector3(spriteWidthMiddle, 0, 0);

        //El de arriba
        GeneratePrefab(Position.Left, Vector3.zero, "Left");
        //Los del medio
        for (int i = 0; i < width; i++)
        {
            GeneratePrefab(Position.Middle, offsetXLeft + offsetXMiddle * i,"Middle " + i.ToString());
        }
        GeneratePrefab(Position.Right, offsetXLeft + offsetXMiddle * width, "Right");
    }

    void GeneratePrefab(Position pos, Vector3 offset, string name)
    {
        GameObject spr = Instantiate(terrainPrefab,
                                     transform.position + offset,
                                     Quaternion.identity,
                                     transform);
        spr.name = name;
        spr.tag = "Destructible";
        spr.layer = LayerMask.NameToLayer("Terrain");
        spr.AddComponent<DestructibleTerrain>();
        switch (pos)
        {
            case Position.Left:
                spr.GetComponent<SpriteRenderer>().sprite = _leftCornerSprite;
                break;
            case Position.Middle:
                spr.GetComponent<SpriteRenderer>().sprite = _middleSprite;
                break;
            case Position.Right:
                spr.GetComponent<SpriteRenderer>().sprite = _rightCornerSprite;
                break;
            default:
                break;
        }
        spr.AddComponent<BoxCollider2D>();
    }
}
