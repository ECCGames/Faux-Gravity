﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

[RequireComponent(typeof(TrailRenderer)),RequireComponent(typeof(Rigidbody2D))]
public class LavaParticle : Lava
{
	[SerializeField] private float _sizeDecayRate;
	[SerializeField] private float _minSize;
	[SerializeField] private float _decayDelay;

	private bool _isDecaying;

	void Start()
	{
		Invoke(nameof(StartDecaying), _decayDelay);
	}
	
	void Update ()
	{
		if (!_isDecaying) return;
		
		transform.localScale *= _sizeDecayRate;
		
		if(transform.localScale.x < _minSize) Destroy(gameObject);
	}

	void StartDecaying()
	{
		_isDecaying = true;
	}

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            col.GetComponent<Player>().KillPlayer();
        }
        else if (LayerMask.LayerToName(col.gameObject.layer) == "Terrain" && !col.CompareTag("Indestructible"))
        {
            var terrain = col.gameObject.GetComponent<Terrain>();

            if (terrain != null) terrain.DestroyTerrain();
        }
        else if (!col.CompareTag("Indestructible") && !col.CompareTag("Absorber"))
        {
            Destroy(col.gameObject);
        }
    }
}
