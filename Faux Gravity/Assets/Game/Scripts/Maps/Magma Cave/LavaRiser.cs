﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class LavaRiser : LevelInteraction
{
    [SerializeField] private List<Animator> _alarms;
    [Range(0f, 1f)] [SerializeField] private float _minVolume;
    [Range(0f, 1f)] [SerializeField] private float _maxVolume;
    [SerializeField] private float _minTimeToAlarm;
    [SerializeField] private float _maxTimeToAlarm;
    [SerializeField] private float _lavaTimer;
    [SerializeField] private float _rangeToRise;
    [SerializeField] private float _riseSpeed;
    [SerializeField] private int _timesToRise = 3;
    [Header("Lava goes back")] [SerializeField] private bool _lavaGoesDown;

    private bool _goingDown;
    private AudioSource _alarmSound;
    private bool _lavaIsRising;
    private float _lastYPosition;

    private bool _lerpingSound;
    private bool _incremental;

    private int _timesRisen;

    private void Awake()
    {
        _alarmSound = GetComponent<AudioSource>();
        _alarmSound.volume = 0f;

        //Invoco la alarma cada x tiempo
        InvokeRepeating("SoundAlarm", Random.Range(_minTimeToAlarm, _maxTimeToAlarm), Random.Range(_minTimeToAlarm, _maxTimeToAlarm));
    }

    public void SoundAlarm()
    {
        if (Paused) return;

        if (_lavaGoesDown)
        {
            if (_goingDown)
            {
                if (_timesRisen <= 0) _goingDown = !_goingDown;
            }
            else
            {
                if (_timesRisen >= _timesToRise) _goingDown = !_goingDown;
            }
        }


        for (int i = 0; i < _alarms.Count; i++)
        {
            if(_alarms[i] != null) _alarms[i].SetBool("alarmOn", true);
        }
        _alarmSound.Play();
        _lerpingSound = true;
        _incremental = true;

        //Invoco la lava con un float porque soy un pete que no sabe medir animaciones
        Invoke("RiseLava", _lavaTimer);
    }

    public void RiseLava()
    {
        _lavaIsRising = true;
        _lastYPosition = transform.position.y;
    }

    public void StopRisingLava()
    {
        _lavaIsRising = false;
        _lerpingSound = true;
        _incremental = false;
    }

    private void Update()
    {
        //if (_timesRisen >= _timesToRise) return;
        if (Paused) return; 
        if (_lerpingSound)
        {
            if (_incremental)
            {
                _alarmSound.volume = Mathf.Lerp(_alarmSound.volume, _maxVolume, 0.02f);

                if (_alarmSound.volume >= _maxVolume - 0.05f)
                {
                    _alarmSound.volume = _maxVolume;
                    _lerpingSound = false;
                }
            }
            else
            {
                _alarmSound.volume = Mathf.Lerp(_alarmSound.volume, _minVolume, 0.02f);

                if (_alarmSound.volume <= _minVolume + 0.05f)
                {
                    _alarmSound.volume = _minVolume; for (int i = 0; i < _alarms.Count; i++)
                    {
                        if(_alarms[i] != null) _alarms[i].SetBool("alarmOn", false);
                    }
                    
                    if(_goingDown) _timesRisen--;
                    else _timesRisen ++;
                    
                    _alarmSound.Stop();
                    _lerpingSound = false;
                }
            }
        }

        if (!_lavaIsRising) return;

        transform.Translate(_goingDown ? -transform.up * _riseSpeed * Time.deltaTime : transform.up * _riseSpeed * Time.deltaTime);

        if (Mathf.Abs(_lastYPosition - transform.position.y) >= _rangeToRise) StopRisingLava();
    }
    
    public override void PauseInteractions()
    {
        _alarmSound.Pause();
        base.PauseInteractions(); 
    }

    public override void ResumeInteractions()
    {
        _alarmSound.UnPause();
        base.ResumeInteractions();
    }
}
