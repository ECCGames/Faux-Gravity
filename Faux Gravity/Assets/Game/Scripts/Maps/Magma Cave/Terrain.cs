﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Terrain : MonoBehaviour
{
	[SerializeField] private float _respawnTimer = 1;
	
	public void DestroyTerrain()
	{
		gameObject.SetActive(false);
		Invoke(nameof(Respawn), _respawnTimer);
	}

	public void Respawn()
	{
        //overlapsphere aca
        //si choca vuelve a invocar respawn
        //si no choca hace set active true
        //va?
        //Esto es un invoke repeating con mas control

        //
        if(Physics2D.OverlapCircle(transform.position, 1, 1 << LayerMask.NameToLayer("Lava")))
        {
            Invoke("Respawn", 2);
        }
        else
        {
            gameObject.SetActive(true);
        }
        //Invoke(nameof(TrueRespawn), 0.5f);
	}

    void TrueRespawn()
    {
        gameObject.SetActive(true);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}
