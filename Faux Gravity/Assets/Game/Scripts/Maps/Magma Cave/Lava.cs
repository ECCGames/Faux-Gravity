﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
	protected virtual void OnTriggerEnter2D(Collider2D col)
	{
        if (col.CompareTag("Player"))
        {
            col.GetComponent<Player>().KillPlayer();
        }
        else if (LayerMask.LayerToName(col.gameObject.layer) == "Terrain")
        {
	        var terrain = col.gameObject.GetComponent<Terrain>();
	        
	        if (terrain != null) terrain.DestroyTerrain();
        }
		else if (!col.CompareTag("Indestructible") && !col.CompareTag("Absorber"))
        {
	        Destroy(col.gameObject);
        }
	}
}
