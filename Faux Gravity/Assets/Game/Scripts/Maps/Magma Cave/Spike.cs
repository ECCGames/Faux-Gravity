﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource), typeof(Rigidbody2D), typeof(PolygonCollider2D))]
public class Spike : Activable
{
    [SerializeField] int _destructivePower;
    [SerializeField] bool _dieAfterPlayerContact;
    [SerializeField] private float _pushRadius;
    [SerializeField] private float _pushStrength;
    [SerializeField] AudioClip _hitSound;
    [SerializeField] AudioClip _deathSound;

    ParticleSystem _deathAnim;
    AudioSource _source;
    Rigidbody2D _rigidB;
    bool alreadyDead;

    private Collider2D[] _results;
    
    private void Awake()
    {
        _rigidB = GetComponent<Rigidbody2D>();
        _source = GetComponent<AudioSource>();
        _deathAnim = GetComponent<ParticleSystem>();
        _deathAnim.Stop();
    }

    public override void Activate()
    {
        _rigidB.bodyType = RigidbodyType2D.Dynamic;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Player auxPlayer = collision.gameObject.GetComponent<Player>();
            
            if (auxPlayer == null) return;
            if(auxPlayer.IsImmune) return;
            
            auxPlayer.KillPlayer();
            
            if (_dieAfterPlayerContact) _destructivePower = 0;
            else _destructivePower--;
            
            if (_destructivePower <= 0 && !alreadyDead) Death();
        }
        if (collision.CompareTag("Destructible"))
        {
            DestructibleTerrain terrain = collision.GetComponent<DestructibleTerrain>();
            if (terrain != null)
                terrain.Hit();
            else
            {
                Destroy(collision.transform.gameObject);
            }
            _destructivePower--;
            if (_destructivePower <= 0 && !alreadyDead) Death();
        }
        else if(collision.CompareTag("Indestructible"))
        {
            Death();
        }
    }

    public void Death()
    {
        alreadyDead = true;
        _source.clip = _deathSound;
        _source.Play();
        _deathAnim.Play();
        
        _results = Physics2D.OverlapCircleAll(transform.position, _pushRadius);
		
        for (int i = 0; i < _results.Length; i++)
        {
            if(_results[i].gameObject == gameObject) continue;
			
            var dist = _results[i].transform.position - transform.position;
			
            if (_results[i].gameObject.CompareTag("Player"))
            {
                Player player = _results[i].GetComponent<Player>();
				
                if (player == null || player.IsImmune) continue;
				
                player.AddForce(dist.normalized * (_pushStrength / dist.magnitude));
            }
            else
            {
                Rigidbody2D objectToPush = _results[i].GetComponent<Rigidbody2D>();
                if(objectToPush!=null)
                    objectToPush.AddForce(dist.normalized * (_pushStrength / dist.magnitude));
            }
        }
        
        this.GetComponent<PolygonCollider2D>().enabled = false;
        this.GetComponent<SpriteRenderer>().enabled = false;
        Destroy(gameObject, _deathAnim.main.duration);
    }
}
