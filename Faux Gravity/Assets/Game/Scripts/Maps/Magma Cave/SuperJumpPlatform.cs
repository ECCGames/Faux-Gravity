﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperJumpPlatform : MonoBehaviour {

    [SerializeField] private float _propulsorForce;
    [SerializeField] private AudioClip _superJumpSfx;
    
    private BoxCollider2D _collider;
    private AudioSource _audioSource;
    
    private void Awake()
    {
        _collider = GetComponent<BoxCollider2D>();
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = _superJumpSfx;
        _collider.isTrigger = true;
    }

    private Vector2 GetForceToAdd(Vector2 playerPos, float multiplier = 1)
    {
        float zAngleRad = transform.rotation.eulerAngles.z * Mathf.Deg2Rad;
        Vector2 direction = new Vector2(Mathf.Sin(zAngleRad) * -1, Mathf.Cos(zAngleRad) * 1).normalized;
        return direction * _propulsorForce * multiplier;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            var auxPlayer = collision.GetComponent<Player>();
            if (auxPlayer.MyPlayerMovement.Jump)
            {
                auxPlayer.AddForce(GetForceToAdd(collision.transform.position), true);
                _audioSource.Play();
            }
        }
    }
}
