﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleTerrain : Terrain {

    [Header("ReSpawn Fields")]
    [SerializeField] float _deathDuration;
    [SerializeField] float _regenerationDuration;
    [SerializeField] private bool _respawns = true;
    [SerializeField] private ColliderChecker _respawnChecker;

    [Header("Tile Renderers")]
    [SerializeField] SpriteRenderer _emptyTile;
    [SerializeField] SpriteRenderer _realTile;

    Collider2D _collider;
    ContactFilter2D _filter;
    Collider2D[] _results;

    bool _isRegenerating;
    Vector3 _originalScale;
    float _timer;
    bool regenerateOrder;
    bool canRegenerate = true;

    private void Awake()
    {
        _collider = GetComponent<Collider2D>();
        _filter.SetLayerMask(LayerMask.NameToLayer("Player"));
        _results = new Collider2D[4];
        if (_emptyTile == null)
            GetComponent<SpriteRenderer>();
        if (_emptyTile.sprite == null)
            _emptyTile.sprite = Resources.Load<Sprite>("Resources/Sprites/EmptyTile2");
        _emptyTile.enabled = false;
        _realTile.enabled = true;
        _originalScale = _realTile.transform.localScale;
    }

    private void Update()
    {
        canRegenerate = !_respawnChecker.IsColliding;

        if (_isRegenerating)
        {
            _timer += Time.deltaTime;
            _realTile.transform.localScale =
                Vector3.Lerp(_realTile.transform.localScale, _originalScale, _timer / _regenerationDuration);
            if (_realTile.transform.localScale == _originalScale)
            {
                _isRegenerating = false;
            }
        }
        if(regenerateOrder && canRegenerate)
        {
            RespawnTerrain();
        }
    }

    public void Hit()
    {
        if (!_isRegenerating)
        {
            _realTile.enabled = false;
            _collider.enabled = false;
            _realTile.transform.localScale = Vector3.zero;
            if (_respawns)
            {
                _emptyTile.enabled = true;
                Invoke("ActivateRegenerateOrder", _deathDuration);
            }
        }
    }

    void ActivateRegenerateOrder()
    {
        regenerateOrder = true;
    }

    void RespawnTerrain()
    {
        _emptyTile.enabled = false;
        _realTile.enabled = true;
        _collider.enabled = true;
        _isRegenerating = true;
        _timer = 0;
        regenerateOrder = false;
    }
}
