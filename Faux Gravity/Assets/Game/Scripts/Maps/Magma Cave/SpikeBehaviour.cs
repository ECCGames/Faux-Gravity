﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeBehaviour : LevelInteraction
{

    [SerializeField] int _amountToActivate;
    [SerializeField] float _activationTimer;
    [SerializeField] float _activationDelay;

    List<SpikeSpawner> spikes;
    int[] random;

    private void Awake()
    {
        spikes = new List<SpikeSpawner>(GetComponentsInChildren<SpikeSpawner>());
        random = new int[_amountToActivate];
        InvokeRepeating("ThrowSpike", _activationTimer, _activationTimer);
    }

    void ThrowSpike()
    {
        //Me fijo cuales activar
        for (int i = 0; i < random.Length; i++)
        {
            int aux = Random.Range(0, spikes.Count);
            while (System.Array.IndexOf(random,aux) == -1)
            {
                aux = Random.Range(0, spikes.Count);
            }
            random[i] = Random.Range(0, spikes.Count);
        }
        //Las activo
        for (int i = 0; i < random.Length; i++)
        {
            if(!Paused) StartCoroutine(ActivateSpike(spikes[random[i]], Random.Range(0f, _activationDelay)));
        }
    }

    IEnumerator ActivateSpike(SpikeSpawner spawner, float time)
    {
        yield return new WaitForSeconds(time);
        spawner.StartSpawnSpike();
    }

    public override void PauseInteractions()
    {
        CancelInvoke("ThrowSpike");
        base.PauseInteractions(); 
    }

    public override void ResumeInteractions()
    {
        InvokeRepeating("ThrowSpike", _activationTimer, _activationTimer); 
        base.ResumeInteractions();
    }
}
