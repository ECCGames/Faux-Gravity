﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropulsorPlatform : MonoBehaviour {

    [SerializeField] private AudioClip _superJumpSfx;
    [SerializeField] float _propulsorForce;
    [SerializeField] AnimationCurve _curve;
    [SerializeField] float _maxDistanceForce;
    [SerializeField] [Range(1,5f)] float _enterMultiplier;
    
    BoxCollider2D _collider;

    private AudioSource _audioSource;
    
    private void Awake()
    {
        _collider = GetComponent<BoxCollider2D>();
        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = _superJumpSfx;
        _collider.isTrigger = true;
    }

    private Vector2 GetForceToAdd(Vector2 playerPos, float multiplier = 1)
    {
        float zAngleRad = transform.rotation.eulerAngles.z * Mathf.Deg2Rad;
        Vector2 direction = new Vector2(Mathf.Sin(zAngleRad) * -1, Mathf.Cos(zAngleRad) * 1).normalized;
        return direction *
            _curve.Evaluate(Mathf.Clamp01(Vector2.Distance(playerPos, transform.position) / _maxDistanceForce)) *
            _propulsorForce *
            multiplier;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            _audioSource.Play();
            collision.GetComponent<Player>().AddForce(GetForceToAdd(collision.transform.position, _enterMultiplier), true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<Player>().AddForce(GetForceToAdd(collision.transform.position), true);
        }
    }
}
