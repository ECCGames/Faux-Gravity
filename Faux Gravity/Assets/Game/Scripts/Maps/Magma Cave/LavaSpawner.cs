﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaSpawner : Lava 
{
	[SerializeField] private LavaParticle _lavaparticle;
	[SerializeField] private float _spawnRate;
	private BoxCollider2D _collider2D;

	private bool _canSpawn = true;

	public bool CanSpawn => _canSpawn;

	private void Start()
	{
		_collider2D = GetComponent<BoxCollider2D>();
	}

	public void StopSpawing()
	{
		_canSpawn = true;
	}
	
	public void SpawnLavaParticle()
	{
		if (_canSpawn)
		{
			Instantiate(_lavaparticle,
				transform.position + new Vector3(Random.Range(-_collider2D.size.x / 2, _collider2D.size.x / 2), 0),
				Quaternion.identity);

			_canSpawn = false;
			Invoke(nameof(StopSpawing), _spawnRate);
		}
	}
}
