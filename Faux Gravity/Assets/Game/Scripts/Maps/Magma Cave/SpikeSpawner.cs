﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SpikeSpawner : MonoBehaviour {

    [SerializeField] Spike _spikePrefab;
    [SerializeField] AudioClip _activationSound;
    [SerializeField] private float _maxLerpRange;
    [SerializeField] private float _lerpDuration;
    [SerializeField] private float _lerpSpeed;
    [SerializeField] private float _respawnTime;

    private AudioSource _source;
    private SpriteRenderer _sprite;
    private bool _isLerping;
    private Vector3 _startPosition;
    
    private void Awake()
    {
        _source = GetComponent<AudioSource>();
        _sprite = GetComponent<SpriteRenderer>();
        //_sprite.sprite = _spikePrefab.GetComponent<SpriteRenderer>().sprite;
        _startPosition = transform.position;
    }

    public void StartSpawnSpike()
    {
        if (_isLerping || !_sprite.enabled) return;
        
        _isLerping = true;
        _source.clip = _activationSound;
        _source.Play();
        Invoke("SpawnSpike", _lerpDuration);
    }

    private void Update()
    {
        if (_isLerping && !LevelController.Instance.GamePaused) transform.position += (Vector3) new Vector2(Mathf.Sin(Time.time * _lerpSpeed) * _maxLerpRange, 0);
    }
    
    public void SpawnSpike()
    {
        _sprite.enabled = false;
        
        _isLerping = false;
        Spike spike = Instantiate(_spikePrefab, transform.position, Quaternion.identity);
        spike.transform.localScale = transform.lossyScale;
        spike.Activate();
        transform.position = _startPosition;
        Invoke("RespawnSprite", _respawnTime);
    }

    public void RespawnSprite()
    {
        _sprite.enabled = true;
    }
}
