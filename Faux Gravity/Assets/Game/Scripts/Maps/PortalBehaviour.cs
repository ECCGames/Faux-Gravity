﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PortalBehaviour : MonoBehaviour
{
	[SerializeField] private PortalBehaviour _destination;
	[SerializeField] private Transform _portalSpawn;
	[SerializeField] private AudioClip _portalAudio;
    [SerializeField] private float _playerTimerOffset;

	private AudioSource _audioSource;
    private Dictionary<GameObject, float> _recentObjects;
    private List<GameObject> _objectsToRemove;
    private List<int> _objectsToRemoveByIndex;

    public PortalBehaviour Destination { get { return _destination; } set { _destination = value; } }
    public Dictionary<GameObject, float> RecentPlayers { get { return _recentObjects; } set { _recentObjects = value; } }
    private void Start ()
	{
        _recentObjects = new Dictionary<GameObject, float>();
        _objectsToRemove = new List<GameObject>();
        _objectsToRemoveByIndex = new List<int>();
        _audioSource = GetComponent<AudioSource>();
		_audioSource.clip = _portalAudio;
	}

    private void Update()
    {
        _objectsToRemove.Clear();
        _objectsToRemoveByIndex.Clear();
        for (int i = 0; i < _recentObjects.Count; i++)
        {
            var element = _recentObjects.ElementAt(i);
            if(element.Key == null)
            {
                _objectsToRemoveByIndex.Add(i);
                continue;
            }
            _recentObjects[element.Key] = element.Value + Time.deltaTime;
            //Debug.Log(element.Key + " : " +element.Value);
            if (element.Value > _playerTimerOffset) _objectsToRemove.Add(element.Key);
        }
        for (int i = 0; i < _objectsToRemove.Count; i++)
        {
            _recentObjects.Remove(_objectsToRemove[i]);
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
	{
        Teleport(col);
	}

    void Teleport(Collider2D col)
    {
        if (_destination != null && !_recentObjects.ContainsKey(col.gameObject) && !_destination.RecentPlayers.ContainsKey(col.gameObject))
        {
            _destination.RecentPlayers.Add(col.gameObject, 0);
            _destination.TpToDestination(col.transform);
        }
    }

	public void TpToDestination(Transform objectToTp)
	{
		objectToTp.position = _portalSpawn.position;
		_audioSource.PlayOneShot(_portalAudio);
	}

    public void SetDeathTimer(float time)
    {
        Destroy(gameObject, time);
    }

    public void SetDestination(PortalBehaviour portal)
    {
        _destination = portal;
        portal.Destination = this;
    }

    public void ChangeColor(Color newColor)
    {
        GetComponent<Renderer>().material.color = newColor;
    }
}
