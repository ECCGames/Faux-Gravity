﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RespawnPoint : MonoBehaviour {


    [SerializeField] float _safetyOffset;
    
    private bool _isSafe;
    public bool IsSafe { get { return _isSafe; } set { _isSafe = value; _onSafetyChange.Invoke(this, _isSafe); } }
    private SafetyChangeEvent _onSafetyChange;

    public SafetyChangeEvent OnSafetyChange { get { return _onSafetyChange; } }

    private void Awake()
    {
        _onSafetyChange = new SafetyChangeEvent();
    }
    
    //Si ya está seguro, vuelve porque no hace falta chequear si salió de lava
    //Si no está seguro, chequea hasta que esté seguro
    private void Update()
    {
        if (_isSafe) return;
        
        if (!IsInvoking(nameof(SetSpawnSafe)) && !Physics2D.OverlapCircle(transform.position, 1, 1 << LayerMask.NameToLayer("Lava")))
            Invoke(nameof(SetSpawnSafe), _safetyOffset);
    }
    
    /* OLD UPDATE
        if (Physics2D.OverlapCircle(transform.position, 1, 1 << LayerMask.NameToLayer("Lava")))
        {
            IsSafe = false;
            CancelInvoke("SetSpawnSafe");
        }
        else if(!IsSafe && !IsInvoking("SetSpawnSafe"))
            Invoke("SetSpawnSafe", _safetyOffset);
    */
    
    public bool CheckSafety()
    {
        if (!gameObject.activeInHierarchy || Physics2D.OverlapCircle(transform.position, 1, 1 << LayerMask.NameToLayer("Lava")))
            _isSafe = false;
        else
            _isSafe = true;
        
        return _isSafe;
    }
    
    public void SetSpawnSafe()
    {
        _isSafe = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, 1f);
    }
}

public class SafetyChangeEvent : UnityEvent<RespawnPoint,bool> { }