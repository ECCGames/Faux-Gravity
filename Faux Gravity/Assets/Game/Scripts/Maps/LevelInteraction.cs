﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInteraction : MonoBehaviour
{
	protected bool Paused;

	public virtual void PauseInteractions()
	{
		Paused = true;
	}

	public virtual void ResumeInteractions()
	{
		Paused = false;
	}
}
