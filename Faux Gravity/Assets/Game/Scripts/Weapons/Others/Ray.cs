﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Ray : MonoBehaviour
{
    [SerializeField] float width;
    [SerializeField] Gradient gradient;

    LineRenderer _lineRenderer;
    Vector3[] positions;
    List<GameObject> gosToDestroy;

    Vector3 direction;
    float length;
    int totalAmountToDestroy;
    float timeToLerp;
    float timer;
    bool active;

    List<GameObject> gosToDestroyCurrentFrame;
    Vector3 rendererCurrentPos;

    private void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        gosToDestroy = new List<GameObject>();
        gosToDestroyCurrentFrame = new List<GameObject>();
        positions = new Vector3[2];

        _lineRenderer.positionCount = 2;
        _lineRenderer.SetPositions(new Vector3[2] { Vector3.zero, Vector3.zero });
        _lineRenderer.colorGradient = gradient;
        _lineRenderer.startWidth = width;
        _lineRenderer.endWidth = width;
        _lineRenderer.enabled = false;
    }

    public void ActivateRay(Vector3 startPos, Vector3 direction, float raysLength, float lerpTime, List<GameObject> objectToDestroy)
    {
        //Debug.Log("Empezo con " + objectToDestroy.Count + " objetos para romper");
        active = true;
        positions[0] = startPos;
        positions[1] = startPos + (direction * raysLength);

        this.direction = direction;
        length = raysLength;
        totalAmountToDestroy = objectToDestroy.Count;
        gosToDestroy = objectToDestroy;

        _lineRenderer.SetPosition(0, startPos);
        _lineRenderer.SetPosition(1, startPos);

        timer = 0;
        timeToLerp = lerpTime;
        _lineRenderer.enabled = true;
    }

    private void Update()
    {
        if (active)
        {
            timer += Time.deltaTime;
            _lineRenderer.SetPosition(1, Vector3.Lerp(_lineRenderer.GetPosition(0), positions[1], timer / timeToLerp));
            rendererCurrentPos = _lineRenderer.GetPosition(1);
            DestroyPixels();
            if (Vector3.Distance(rendererCurrentPos, positions[1]) < 0.1f)
            {
                _lineRenderer.enabled = false;
                active = false;
                //Debug.Log("termino con " + gosToDestroy.Count + " objetos para romper");
            }
        }
    }

    public void DestroyPixels()
    {
        gosToDestroyCurrentFrame.Clear();
        for (int i = 0; i < gosToDestroy.Count; i++)
        {
            //Debug.Log(Vector3.Distance(rendererCurrentPos, gosToDestroy[i].transform.position));
            if (Vector3.Distance(rendererCurrentPos, gosToDestroy[i].transform.position) <= _lineRenderer.endWidth * 2 + .1f)
                gosToDestroyCurrentFrame.Add(gosToDestroy[i]);
        }
        //Debug.Log(gosToDestroyCurrentFrame.Count);
        for (int i = 0; i < gosToDestroyCurrentFrame.Count; i++)
        {
            gosToDestroy.Remove(gosToDestroyCurrentFrame[i]);
            Destroy(gosToDestroyCurrentFrame[i]);
        }

        //calculo la nueva posicion final
        positions[1] = positions[0] + direction * length * (gosToDestroy.Count / totalAmountToDestroy);
    }
    
    public bool IsActive()
    {
        return active;
    }

    public void SetRendererWidth(float newWidth)
    {
        _lineRenderer.startWidth = width;
        _lineRenderer.endWidth = width;
    }
}
