﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayBehaviour : Projectile {

    enum SpeedMode { Constant, SlowsOverTime }

    [SerializeField] private AudioClip _bounceHitClip;
    [SerializeField] private AudioClip _rockHitClip;
    [SerializeField] private string[] _triggerTags;
    [SerializeField] private SpeedMode _speedMode;

    private ContactPoint2D[] _contactPoints;
    private int _destructionPower;

    private int DestructionPower { get { return _destructionPower; } set { _destructionPower = value; if (_destructionPower <= 0) Destroy(gameObject); } }

    public override void Awake()
    {
        base.Awake();
        _contactPoints = new ContactPoint2D[3];
        _trailRenderer.AddPosition(transform.position);
    }

    public void SetPower(int power)
    {
        DestructionPower = power;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Spike"))
        {
            collision.GetComponent<Spike>().Death();
            Destroy(gameObject);
        }
        
        if (collision.gameObject.layer == LayerMask.NameToLayer("SpikeSpawner"))
        {
            collision.GetComponent<SpikeSpawner>().StartSpawnSpike();
            Destroy(gameObject);
        }
        
        for (int i = 0; i < _triggerTags.Length; i++)
        {
            if (collision.CompareTag(_triggerTags[i]) && !collision.isTrigger)
            {
                DestructibleTerrain destructible = collision.GetComponent<DestructibleTerrain>();
                if (destructible != null)
                    destructible.Hit();

                if (_audioSource != null || _audioSource.isActiveAndEnabled)
                {
                    _audioSource.clip = _rockHitClip;
                    _audioSource.Play();
                }

                //Destroy(collision.gameObject);

                DestructionPower--;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Indestructible") || collision.gameObject.CompareTag("Spawner"))
        {
            collision.GetContacts(_contactPoints);
            Vector2 reflection = Vector2.Reflect(_rigid.velocity, _contactPoints[0].normal).normalized;
            _rigid.velocity = reflection * speed;
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(reflection.y, reflection.x) * Mathf.Rad2Deg);
            _trailRenderer.AddPosition(transform.position);
        }
    }
    
}

