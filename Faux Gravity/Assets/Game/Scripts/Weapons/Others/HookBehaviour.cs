﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class HookBehaviour : Projectile
{
    public UnityEvent onDeath;
    public UnityEvent onArrival;

    private Transform _originPosition;
    string[] tags;
    private Player _owner;
    private BoxCollider2D _boxCollider2D;
    private LineRenderer _lineRenderer;
    
    public override void Awake()
    {
        base.Awake();
        onDeath = new UnityEvent();
        onArrival = new UnityEvent();
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _lineRenderer = GetComponent<LineRenderer>();
    }

    protected void Update()
    {
        if (_originPosition == null) return;
        
        _lineRenderer.SetPosition(0, _originPosition.position);
        _lineRenderer.SetPosition(1, transform.position);
    }

    public override void Activate(Vector2 dir, float raysWidth, float duration, Material material = null, float rayDuration = 0, Transform origin = null)
    {
        transform.localScale = new Vector3(raysWidth, raysWidth, 0);
        transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg);
        _spawnedTime = Time.time;
        Destroy(gameObject, duration);
        //Material Setting
        if(material !=null) _lineRenderer.material = material;
        _rigid.velocity = dir.normalized * speed;
        if (origin != null) _originPosition = origin;
    }

    public void SetTags(string[] tagsToHook, Player player)
    {
        tags = tagsToHook;
        _owner = player;
        //gameObject.layer = 1 << LayerMask.NameToLayer("BlackHole");
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(_owner != null && col.gameObject == _owner.gameObject) return;
        
        //Si tiene asignado tags especificos para colisionar
        if(tags != null)
        {
            for (int i = 0; i < tags.Length; i++)
            {
                if (LayerMask.LayerToName(col.gameObject.layer) == tags[i])
                { 
                    _rigid.velocity = Vector2.zero;
                    _rigid.isKinematic = true;
                    transform.parent = col.transform;
                    _boxCollider2D.isTrigger = true;
                    onArrival.Invoke();
                    return;
                }
            }
            //Si colisiono con algo y no es un tag con los que puede colisionar se muere
            Destroy(gameObject);
        }
        else
        {
            _rigid.velocity = Vector2.zero;
            _rigid.isKinematic = true;
            transform.parent = col.transform;
            onArrival.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(_owner != null && col.gameObject == _owner.gameObject) return;
        
        //Si tiene asignado tags especificos para colisionar
        if(tags != null)
        {
            for (int i = 0; i < tags.Length; i++)
            {
                if (LayerMask.LayerToName(col.gameObject.layer) == tags[i])
                { 
                    _rigid.velocity = Vector2.zero;
                    _rigid.isKinematic = true;
                    transform.parent = col.transform;
                    _boxCollider2D.isTrigger = true;
                    onArrival.Invoke();
                    return;
                }
            }
            //Si colisiono con algo y no es un tag con los que puede colisionar se muere
            Destroy(gameObject);
            Debug.Log(col.gameObject.name);
        }
        else
        {
            _rigid.velocity = Vector2.zero;
            _rigid.isKinematic = true;
            transform.parent = col.transform;
            onArrival.Invoke();
        }
    }

    private void OnDestroy()
    {
        onDeath.Invoke();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, .5f);
    }
}