﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource),typeof(Rigidbody2D))]
public abstract class Projectile : MonoBehaviour {

    protected Rigidbody2D _rigid;
    protected AudioSource _audioSource;
    protected TrailRenderer _trailRenderer;

    [SerializeField] protected float speed;
    protected float _spawnedTime;
    protected bool _activated;

    public virtual void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _rigid = GetComponent<Rigidbody2D>();
        _trailRenderer = GetComponent<TrailRenderer>();
    }

    public virtual void Activate(Vector2 dir, float raysWidth, float duration, Material material = null, float rayDuration = 0, Transform origin = null)
    {
        transform.localScale = new Vector3(raysWidth, raysWidth, 0);
        _trailRenderer.startWidth = raysWidth;
        _trailRenderer.endWidth = raysWidth;
        transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg);
        _spawnedTime = Time.time;
        Destroy(gameObject, duration);
        //Material Setting
        if(material !=null) _trailRenderer.material = material;
        if (rayDuration != 0) _trailRenderer.time = rayDuration;
        _rigid.velocity = dir.normalized * speed;
    }
    
    public virtual void Activate(Vector2 dir, float raysWidth, float duration, Gradient gradient = null, float rayDuration = 0, Transform origin = null)
    {
        transform.localScale = new Vector3(raysWidth, raysWidth, 0);
        _trailRenderer.startWidth = raysWidth;
        _trailRenderer.endWidth = raysWidth;
        transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg);
        _spawnedTime = Time.time;
        Destroy(gameObject, duration);
        //Gradient Setting
        if (gradient != null) _trailRenderer.colorGradient = gradient;
        if (rayDuration != 0) _trailRenderer.time = rayDuration;
        _rigid.velocity = dir.normalized * speed;
    }
}
