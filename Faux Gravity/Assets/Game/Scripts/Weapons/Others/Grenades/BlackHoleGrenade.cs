﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleGrenade : Grenade
{
	[SerializeField] private GameObject _blackHolePrefab;
    [SerializeField] AudioClip explodeSound;

	public override void Explode()
	{
		Instantiate(_blackHolePrefab, transform.position, Quaternion.identity);
        Source.clip = explodeSound;
        Source.Play();
		base.Explode();
	}
}
