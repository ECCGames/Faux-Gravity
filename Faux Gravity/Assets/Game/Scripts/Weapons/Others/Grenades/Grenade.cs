﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Grenade : MonoBehaviour
{
	[SerializeField] private float _timeToExplode;
	[SerializeField] private bool _explodeOnContact;
	[SerializeField] private bool _explodeOnPlayerContact;
	[SerializeField] private ParticleSystem _particlePrefab;

	protected Rigidbody2D Rigidbody2D;
    protected AudioSource Source;
	private bool _exploded;

    protected virtual void Awake()
    {
        Source = GetComponent<AudioSource>();
	    Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    protected virtual void Start()
	{
		if(!_explodeOnContact && !_explodeOnPlayerContact) Invoke("Explode", _timeToExplode);
	}

	protected void OnCollisionEnter2D(Collision2D col)
	{
		if (LayerMask.LayerToName(col.gameObject.layer) == "Portal") return;
		
		if (_exploded) return;
		
		if(_explodeOnContact && _explodeOnPlayerContact)
			Explode();
		else if(_explodeOnPlayerContact && col.gameObject.CompareTag("Player"))
				Explode();
		else if (_explodeOnContact && !col.gameObject.CompareTag("Player"))
			Explode();
	}

	public virtual void Explode()
	{
		_exploded = true;
		Rigidbody2D.velocity = Vector2.zero;
		if (_particlePrefab != null)
		{
			_particlePrefab.gameObject.SetActive(true);
			this.GetComponent<SpriteRenderer>().enabled = false;
			this.GetComponent<CircleCollider2D>().enabled = false;
			Invoke("DestroyMe", _particlePrefab.main.duration);
		}
		else
			Destroy(gameObject);
	}

	private void DestroyMe()
	{
		Destroy(gameObject);
	}
}
