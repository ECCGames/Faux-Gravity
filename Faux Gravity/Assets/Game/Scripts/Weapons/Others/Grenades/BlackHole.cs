﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
	[SerializeField] private float _deathTimer;
	[SerializeField] private float _destroyTimer = 1.15f;
    [SerializeField] private AudioClip _activationSound;

    private AudioSource _audioSource;
	private Animator _anim;

    private void Awake()
    {
        Invoke("Death", _deathTimer);
        _anim = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        if (_audioSource != null && _activationSound != null)
        {
            _audioSource.clip = _activationSound;
            _audioSource.Play();
        }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (!col.CompareTag("Player") && !col.CompareTag("Indestructible") && !col.CompareTag("Spawner") && !col.CompareTag("Absorber"))
        {
            if (col.CompareTag("Destructible"))
            {
                DestructibleTerrain terrain = col.GetComponent<DestructibleTerrain>();
                if (terrain != null)
                    terrain.Hit();
                else
                    Destroy(col.gameObject);
            }
            else Destroy(col.gameObject);
        }
    }

	private void Death()
	{
		_anim.SetBool("Done", true);
		Invoke("DestroyMe", _destroyTimer);
	}

	public void DestroyMe()
	{
		Destroy(gameObject);
	}
}
