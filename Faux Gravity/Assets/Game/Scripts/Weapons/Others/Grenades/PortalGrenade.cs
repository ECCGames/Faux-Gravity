﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PortalGrenade : Grenade {

    [SerializeField] private PortalBehaviour _portalPrefab;
    [SerializeField] AudioClip explodeSound;
    [SerializeField] float _deathTimer;

    private OnPortalInstance _instance;
    private UnityEvent _onDeath;
    private PortalBehaviour _nextPortal;
    private Color _color;
    
    public OnPortalInstance PortalInstance { get { return _instance; } }
    public UnityEvent OnDeath { get { return _onDeath; } }

    protected override void Awake()
    {
        base.Awake();
        _instance = new OnPortalInstance();
        _onDeath = new UnityEvent();
    }

    public override void Explode()
    {
        var portal = Instantiate(_portalPrefab, transform.position, Quaternion.identity);
        //if(_color != null)portal.ChangeColor(_color);
        portal.SetDeathTimer(_deathTimer);
        if (_nextPortal != null) portal.SetDestination(_nextPortal);
        Source.clip = explodeSound;
        Source.Play();
        _instance.Invoke(portal);
        base.Explode();
    }

    public void SetColor(Color color)
    {
        _color = color;
    }

    public void SetPortalConection(PortalBehaviour portal)
    {
        Debug.Log("Im in: ", portal);
        if(portal != null)_nextPortal = portal;
    }

    private void OnDestroy()
    {
        _onDeath.Invoke();
    }
}

public class OnPortalInstance : UnityEvent<PortalBehaviour> { }
