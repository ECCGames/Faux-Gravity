﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpulseGrenade : Grenade
{
    [SerializeField] private float _impulseStrength;
	[SerializeField] private float _playerImpulseStrength;
    [SerializeField] private float _explosionRadius;
    [SerializeField] AudioClip _explodeSound;

    private Collider2D[] _results;

    public override void Explode()
	{
        //Debug.Log(_anim.GetCurrentAnimatorStateInfo(0).length);
		_results = Physics2D.OverlapCircleAll(transform.position, _explosionRadius);
		
		for (int i = 0; i < _results.Length; i++)
		{
			if(_results[i].gameObject == gameObject) continue;
			
			var dist = _results[i].transform.position - transform.position;
			
			if (_results[i].gameObject.CompareTag("Player"))
			{
				Player player = _results[i].GetComponent<Player>();
				
				if (player == null || player.IsImmune) continue;

				float pushForce = _playerImpulseStrength / dist.magnitude;
				
				player.AddForce(dist.normalized * (pushForce > _playerImpulseStrength ? _playerImpulseStrength : pushForce));
			}
			else
			{
				Rigidbody2D objectToPush = _results[i].GetComponent<Rigidbody2D>();
				if (objectToPush != null)
				{
					float pushForce = _impulseStrength / dist.magnitude;
					if (objectToPush.gameObject.CompareTag("PickUp"))
					{
						Debug.Log(objectToPush.gameObject.name);
						objectToPush.GetComponentInParent<Rigidbody2D>().AddForce(dist.normalized * (pushForce > _impulseStrength ? _impulseStrength : pushForce));
					}
					objectToPush.AddForce(dist.normalized * (pushForce > _impulseStrength ? _impulseStrength : pushForce));
				}
			}
		}
		
		Source.clip = _explodeSound;
        Source.Play();
        base.Explode();
	}
}
