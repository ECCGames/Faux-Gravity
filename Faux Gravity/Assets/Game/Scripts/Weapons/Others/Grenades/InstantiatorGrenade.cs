﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiatorGrenade<T> : Grenade {

    [SerializeField] private T _prefab;
    [SerializeField] AudioClip explodeSound;

    public override void Explode()
    {
        //Instantiate(_prefab, transform.position, Quaternion.identity);
        Source.clip = explodeSound;
        Source.Play();
        base.Explode();
    }
}
