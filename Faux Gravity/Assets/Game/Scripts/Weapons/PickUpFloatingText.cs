﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpFloatingText : MonoBehaviour
{
	[SerializeField] private float _timeToDisappear;
	[SerializeField] private float _floatingSpeed;
	[SerializeField] private Text _floatingText;

	private float _timer;

	public Text FloatingText => _floatingText;

	void Awake()
	{
		Destroy(gameObject, _timeToDisappear);
	}

	void Update()
	{
		_timer += Time.deltaTime;
		
		_floatingText.color = new Color(_floatingText.color.r, _floatingText.color.g, _floatingText.color.b, Mathf.Lerp(1, 0, _timer / _timeToDisappear));
		
		transform.Translate(transform.up * _floatingSpeed * Time.deltaTime);
	}
	
	
}
