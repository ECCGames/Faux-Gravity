﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalWeapon : ActivatebleChargingWeapon<PortalGrenade> {

    private PortalGrenade _secondGrenadeInstance;
    private PortalBehaviour _firstPortal;
    private bool _activatedFirstShot;

    protected override void Shoot(Vector3 direction)
    {
        if (!_activatedFirstShot)
        {
            //explota la granada instanciada
            if (_grenadeInstance != null)
            {
                _activatedFirstShot = true;
                //La instancia de la granada puede morir por otros medios 
                if (_grenadeInstance != null)
                {
                    _grenadeInstance.Explode();
                }
                CoolWeapon();
            }
            //si no tengo una bala instanciada y tengo balas, la creo
            if (_grenadeInstance == null && Bullets > 0)
            {
                PlayClip(_fireSound, true);
                _grenadeInstance = Instantiate(_grenadePrefab, BulletOrigin.position, Quaternion.identity);
                _grenadeInstance.OnDeath.AddListener(ActivatedFirstShot);
                //_grenadeInstance.SetColor(_player.PlayerColor);
                _grenadeInstance.GetComponent<Rigidbody2D>().AddForce(direction * _grenadeForce * (Mathf.Clamp01(GetChargedPower(true) + _baseForce)));
                //Aca es de donde saco su referencia para pasarsela al segundo portal
                _grenadeInstance.PortalInstance.AddListener(SetFirstPortal);
                Bullets = Bullets - _fireConsumption < 0 ? 0 : Bullets - _fireConsumption;
                ResetCurrentPower();
            }
        }
        else
        {
            //explota la granada instanciada
            if (_secondGrenadeInstance != null)
            {
                _activatedFirstShot = true;
                //La instancia de la granada puede morir por otros medios 
                if (_secondGrenadeInstance != null)
                {
                    _secondGrenadeInstance.Explode();
                }
                CoolWeapon();
            }
            //si no tengo una bala instanciada y tengo balas, la creo
            if (_secondGrenadeInstance == null && Bullets > 0)
            {
                PlayClip(_fireSound, true);
                _secondGrenadeInstance = Instantiate(_grenadePrefab, BulletOrigin.position, Quaternion.identity);
                //_grenadeInstance.SetColor(_player.PlayerColor);
                _secondGrenadeInstance.GetComponent<Rigidbody2D>().AddForce(direction * _grenadeForce * (Mathf.Clamp01(GetChargedPower(true) + _baseForce)));
                _secondGrenadeInstance.SetPortalConection(_firstPortal);
                Bullets = Bullets - _fireConsumption < 0 ? 0 : Bullets - _fireConsumption;
                ResetCurrentPower();
            }
        }
        ResetCurrentPower();
    }

    private void SetFirstPortal(PortalBehaviour portal)
    {
        Debug.Log("asdd");
        _firstPortal = portal;
    }

    private void ActivatedFirstShot()
    {
        _activatedFirstShot = true;
    }
}
