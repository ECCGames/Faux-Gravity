﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatebleChargingWeapon<T> : ChargingWeapon
    where T : Grenade
{

    [Header("ActivatebleChargingWeapon Fields")]
    [SerializeField][Range(0,1f)] protected float _baseForce;
    [SerializeField] protected float _grenadeForce;
    [SerializeField] protected T _grenadePrefab;

    protected T _grenadeInstance;

    protected override void Shoot(Vector3 direction)
    {
        //si no tengo una bala instanciada y tengo balas, la creo
        if (_grenadeInstance == null && Bullets > 0)
        {
            PlayClip(_fireSound, true);
            _grenadeInstance = Instantiate(_grenadePrefab, BulletOrigin.position, Quaternion.identity);
            _grenadeInstance.GetComponent<Rigidbody2D>().AddForce(direction * _grenadeForce * (Mathf.Clamp01(GetChargedPower(true)+ _baseForce)));
            Bullets = Bullets - _fireConsumption < 0 ? 0 : Bullets - _fireConsumption;
        }
        //explota la granada instanciada
        else
        {
            //La instancia de la granada puede morir por otros medios 
            if (_grenadeInstance != null)
            {
                _grenadeInstance.Explode();
                _grenadeInstance = null;
            }
            CoolWeapon();
        }
        ResetCurrentPower();
    }
}
