﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookInWeapon : Weapon {

    [Header("Hook In Fields")]
    [SerializeField] private float _slingForce;
    [SerializeField] private HookBehaviour _hookPrefab;
    [SerializeField] private float _hookWidth;
    [SerializeField] private float _hookDuration;
    [SerializeField] private Material _hookMaterial;

    private bool _canShoot = true;
    private bool _hasArrived;
    HookBehaviour _instance;

    protected override void Shoot(Vector3 direction)
    {
        if (_canShoot)
        {
            if (_instance == null)
            {
                PlayClip(_fireSound);
                RaycastHit2D hit = Physics2D.Raycast(BulletOrigin.position + direction, direction);
                if (hit)
                {
                    _instance = Instantiate(_hookPrefab, BulletOrigin.position, Quaternion.identity);
                    _instance.Activate(direction, _hookWidth, _hookDuration, _hookMaterial, 0, BulletOrigin);
                    _instance.onDeath.AddListener(Reset);
                    _instance.onArrival.AddListener(Arrived);
                    _canShoot = false;
                }
            }
        }
        if (_hasArrived)
        {
            _player.AddForce((_instance.transform.position - _player.transform.position).normalized * _slingForce);
        }
    }

    void Arrived()
    {
        _hasArrived = true;
    }

    void Reset()
    {
        _canShoot = true;
        _hasArrived = false;
        ReleaseFire();
    }

    public override void PressFire(Vector3 direction)
    {
        if(!_isCooling)
            Shoot(direction);
    }

    public override void ReleaseFire()
    {
        base.ReleaseFire();
        Destroy(_instance);
    }
}
