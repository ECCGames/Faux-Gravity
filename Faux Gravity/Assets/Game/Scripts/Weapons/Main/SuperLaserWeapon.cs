﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperLaserWeapon : ChargingWeapon {

    [Header("ChargingLaser Fields")]
    [SerializeField] RayBehaviour _laserPrefab;
    [SerializeField] float _raysWidth;
    [SerializeField] float _raysDuration;
    [SerializeField] int _raysPower;
    [SerializeField] int PushBackForce;
    [SerializeField] Gradient _gradient;

    Vector3 _direction;

    public override void ResetWeapon()
    {
        _pitchSource.Stop();
        _pitchSource.pitch = 1;
        _pitchSource.volume = 1;
    }

    protected override void Shoot(Vector3 direction)
    {
        if (Bullets > 0 && GetChargedPower(true) == 1)
        {
            RaycastHit2D hit = Physics2D.Raycast(_player.transform.position, direction, 5f, LayerMask.NameToLayer("Terrain"));
            Debug.DrawLine(_player.transform.position, _player.transform.position + direction * 10, Color.red, 5f);
            if (hit && hit.collider.CompareTag("Indestructible"))
            {
                Debug.Log("asd");
                Vector2 reflection = Vector2.Reflect(direction, hit.normal).normalized;
                RayBehaviour rayBehaviour = Instantiate(_laserPrefab, reflection, Quaternion.identity);
            }
            else
            {
                RayBehaviour rayBehaviour = Instantiate(_laserPrefab, transform.position, Quaternion.identity);
                rayBehaviour.SetPower(_raysPower);
                rayBehaviour.Activate(direction, _raysWidth, _raysDuration, _gradient, _raysDuration/2);
                PlayClip(_fireSound, true);
                Bullets -= _fireConsumption;
            }
            _player.AddForce(-_direction * PushBackForce);
        }
        ResetCurrentPower();
    }
}
