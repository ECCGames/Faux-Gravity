﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Events;

public enum Rarity {Common, Rare, Epic}

[RequireComponent(typeof(AudioSource))]
public abstract class Weapon : MonoBehaviour
{
    [Header("Basic Weapon Properties")]
    [SerializeField] protected WeaponName _weaponName;
    [SerializeField] protected Rarity _rarityLevel;
    [SerializeField] private string _floatingText;
    [SerializeField] protected AudioClip _noBulletsSound;
    [SerializeField] protected AudioClip _fireSound;
    [SerializeField] protected int _bullets;
    [SerializeField] protected float _fireRate;
    [SerializeField] protected int _fireConsumption;
    

    [Header("UI Settings")]
    [SerializeField] protected Sprite _weaponPickUpSprite;
    [SerializeField] protected Sprite _weaponUISprite;
    [SerializeField] protected bool _useCustomBullets;
    [SerializeField] protected Color _bulletsUIColor;

    /// <summary>
    /// Private Fields
    /// </summary>
    protected AudioSource _mainAudioSource;
    protected Player _player;
    protected UnityEvent _onBulletsChange = new UnityEvent();
    protected bool _hasFired;
    protected bool _isCooling;
    protected bool ceaseFire = true;
    protected Transform BulletOrigin;

    [HideInInspector] public float TimeToCool;
    public float FireRate => _fireRate;
    public bool CeaseFire { get { return ceaseFire; } set { ceaseFire = value; } }
    public Player Player { get { return _player; } }
    public UnityEvent OnBulletsChange { get { return _onBulletsChange; } }
    public WeaponName WeaponName { get { return _weaponName; } }
    public string FloatingText => _floatingText;
    public bool UseCustomBullets { get { return _useCustomBullets; } }
    protected int Bullets { get { return _bullets; } set { _bullets = value; _onBulletsChange.Invoke(); } }
    protected abstract void Shoot(Vector3 direction);

    protected virtual void Start()
    {
        _mainAudioSource = GetComponent<AudioSource>();
        _mainAudioSource.loop = false;
    }

    public virtual void ResetWeapon()
    {
        _mainAudioSource.Stop();
    }

    public int GetPlayerId()
    {
        return _player.Id;
    }

    protected virtual void Update()
    {
        if (_isCooling) TimeToCool += Time.deltaTime;    
    }
    
    protected void PlayClip(AudioClip clip, bool overlapSound = false)
    {
        if(!_mainAudioSource.isPlaying || overlapSound )
        {
            _mainAudioSource.clip = clip;
            _mainAudioSource.Play();
        }
    }

    public void SetPlayer(Player player)
    {
        TimeToCool = _fireRate;
        _player = player;
        BulletOrigin = player.MyPlayerAttack.BulletOrigin;
    }

    public int GetBullets()
    {
        return _bullets;
    }

    public Sprite GetPickUpImage()
    {
        return _weaponPickUpSprite;
    }

    public Sprite GetUIImage()
    {
        return _weaponUISprite;
    }

    public Color GetBulletsUIColor()
    {
        return _bulletsUIColor;
    }

    public virtual void PressFire(Vector3 direction)
    {
        if (_bullets <= 0)
        {
            PlayClip(_noBulletsSound);
            _player.DisplayNoAmmo();
        }

        if (!_isCooling && !_hasFired && _bullets > 0)
        {
            Shoot(direction);
            _hasFired = true;
        }
    }

    public virtual void ReleaseFire()
    {
        if (_bullets <= 0 || _isCooling) return;
        ActivateCooling();
    }

    public virtual void ReleaseFire(Vector3 direction)
    {
        if(_bullets <= 0 || _isCooling) return;
        ActivateCooling();
    }

    protected void ActivateCooling()
    {
        CoolWeapon();
        _isCooling = true;
        _hasFired = false;
        TimeToCool = 0;
    }
    
    protected void CoolWeapon()
    {
        Invoke("ChangeIsCooling", _fireRate);
    }

    private void ChangeIsCooling()
    {
        _isCooling = false;
        TimeToCool = _fireRate;
    }

    private void OnDestroy()
    {
        ResetWeapon();
    }
}