﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPistolWeapon : Weapon {

    [Header("Laser Pistol Fields")]
    [SerializeField] RayBehaviour _laserPrefab;
    [SerializeField] float _raysWidth;
    [SerializeField] float _raysDuration;
    [SerializeField] int _power;
    [SerializeField] Gradient rayGradient;
    
    protected override void Shoot(Vector3 direction)
    {
        RayBehaviour _rayBehaviour = Instantiate(_laserPrefab, BulletOrigin.position , Quaternion.identity);
        _rayBehaviour.SetPower(_power);
        _rayBehaviour.Activate(direction, _raysWidth, _raysDuration, rayGradient);
        PlayClip(_fireSound, true);
        Bullets -= _fireConsumption;
    }
}
