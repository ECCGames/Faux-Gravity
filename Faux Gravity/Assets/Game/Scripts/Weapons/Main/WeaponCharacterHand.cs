﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponName { None, LaserPistol, ChargingLaser, SuperLaser, BlackHoleWeapon, ImpulseGrenade, HookIn, AtractingHook, PortalGun}

[System.Serializable]
public class WeaponCharacterHand{

    public Sprite weaponHandSprite;
    public WeaponName weaponName;
}
