﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class ChargingLaserWeapon : ChargingWeapon {

    [Header("ChargingLaser Fields")]
    [SerializeField] RayBehaviour _laserPrefab;
    [SerializeField] float _coolOffRate;
    [SerializeField] float _raysWidth;
    [SerializeField] float _raysDuration;
    [SerializeField] int _raysPower;
    [SerializeField] Vector2 _raysoffset;
    [SerializeField] int PushBackForce;
    [SerializeField] Gradient _gradient;

    Vector3 _direction;
    float timer;
    bool _internalCooling;
    bool _canShoot = true;

    protected override void Update()
    {
        //Cool Weapon
        base.Update();

        //After the player stopped pressing the Fire Trigger
        if (_internalCooling)
        {
            Substract();
            if (_currentPower <= 0)
            {
                _internalCooling = false;
                _canShoot = true;
            }
        }
    }
    
    public void Substract()
    {
        //if (!_pitchSource.isPlaying) _pitchSource.Play();
        _currentPower = _currentPower - _coolOffRate * Time.deltaTime < 0 ? 0 : _currentPower - _coolOffRate * Time.deltaTime;
        //_mainAudioSource.pitch = 1 + GetChargedPower(true) * _maxPitchAmount;

        //si no es el teclado, activo la vibracion
        if (_player.Id != 0)
        {
            GamePad.SetVibration((PlayerIndex)(_player.Id - 1),
                                 Mathf.Clamp(_currentPower / _maxPower, 0, _maxVibration),
                                 Mathf.Clamp(_currentPower / _maxPower, 0, _maxVibration));
        }
    }

    public override void ResetWeapon()
    {
        _pitchSource.Stop();
        _pitchSource.pitch = 1;
        _pitchSource.volume = 1;
        StopVibration();
    }

    public override void PressFire(Vector3 direction)
    {
        if (_canShoot && _bullets > 0)
        {
            base.PressFire(direction);
            if (_currentPower >= _maxPower) Shoot(direction);
        }
        if(_bullets <= 0)
        {
            if (_canShoot && _mainAudioSource != null && _noBulletsSound != null)
                PlayClip(_noBulletsSound, true);
            _internalCooling = true;
            _canShoot = false;
        }
    }

    public override void ReleaseFire(Vector3 direction)
    {
        _internalCooling = true;
        _canShoot = false;
    }

    protected override void Shoot(Vector3 direction)
    {
        if (!_isCooling)
        {
            _direction = direction;
            InstantiateRay();
            ActivateCooling();
            _bullets -= _fireConsumption; 
        }
    }
    void InstantiateRay()
    {
        if (Bullets > 0)
        {
            RayBehaviour _rayBehaviour = Instantiate(_laserPrefab, BulletOrigin.position, Quaternion.identity);
            _rayBehaviour.SetPower(_raysPower);
            _rayBehaviour.Activate(_player.MyPlayerAttack.GetCrossHairPosition() + new Vector3(Random.Range(-_raysoffset.x, _raysoffset.x), Random.Range(-_raysoffset.y, _raysoffset.y)),
                                  _raysWidth,
                                  _raysDuration,
                                  _gradient);
            _player.AddForce(-_direction * PushBackForce, true);
            Bullets = Bullets - _fireConsumption < 0 ? 0 : Bullets - _fireConsumption;
        }
    }
}
