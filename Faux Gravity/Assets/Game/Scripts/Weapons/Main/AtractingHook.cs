﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtractingHook : Weapon {

    [Header("AtractingHook Fields")]
    [SerializeField] string[] tags;
    [SerializeField] private float _playerAttractionForce;
    [SerializeField] private float _weaponPickUpAttractionForce;
    [SerializeField] private HookBehaviour _hookPrefab;
    [SerializeField] private float _hookWidth;
    [SerializeField] private float _hookDuration;
    [SerializeField] private Material _hookMaterial;

    private bool _canShoot = true;
    private bool _hasArrived;

    Rigidbody2D hookedObject;
    Rigidbody2D rigidPickUp;
    Player hookedPlayer;

    HookBehaviour _instance;

    protected override void Shoot(Vector3 direction)
    {
        if (_canShoot)
        {
            if (_instance == null)
            {
                PlayClip(_fireSound);
                RaycastHit2D hit = Physics2D.Raycast(BulletOrigin.position + direction, direction);
                if (hit)
                {
                    _instance = Instantiate(_hookPrefab, BulletOrigin.position, Quaternion.identity);
                    _instance.SetTags(tags, _player);
                    _instance.Activate(direction, _hookWidth, _hookDuration, _hookMaterial, 0, BulletOrigin);
                    _instance.onDeath.AddListener(InstanceDeath);
                    _instance.onArrival.AddListener(Arrived);
                    _canShoot = false;
                }
            }
        }
        if (_hasArrived)
        {
            float distanceToHook = (_player.transform.position - _instance.transform.position).magnitude;
            if (hookedPlayer != null)
            {
                if (distanceToHook > 1f)
                {
                    //_instance.transform.Translate((_player.transform.position - _instance.transform.position).normalized *_playerAttractionForce * Time.deltaTime);
                    hookedPlayer.AddForce((_player.transform.position - hookedPlayer.transform.position).normalized *_playerAttractionForce);
                }
            }
            else if(hookedObject != null && rigidPickUp!=null)
            {
                Debug.Log(hookedObject.name);
                //rigidPickUp.bodyType = RigidbodyType2D.Dynamic;
                if (distanceToHook > 1f)
                {
                    rigidPickUp.velocity = (_player.transform.position - hookedObject.transform.position).normalized * _weaponPickUpAttractionForce;
                    if (hookedObject.gameObject.CompareTag("PickUp"))
                    {
                        hookedObject.GetComponent<WeaponPickUp>().HookWeapon();
                    }
                    else if (LayerMask.LayerToName(hookedObject.gameObject.layer) == "SpikeSpawner")
                    {
                        hookedObject.GetComponent<SpikeSpawner>().StartSpawnSpike();
                    }
                }
            }
        }
    }

    void Arrived()
    {
        hookedPlayer = _instance.transform.parent.GetComponent<Player>();
        if (hookedPlayer == null)
        {
            hookedObject = _instance.transform.parent.GetComponent<Rigidbody2D>();
            if(hookedObject != null) rigidPickUp = hookedObject.GetComponent<Rigidbody2D>();
        }
        _hasArrived = true;
    }

    void InstanceDeath()
    {
        _canShoot = true;
        //Reinicio los campos internos
        hookedPlayer = null;
        hookedObject = null;
        if(rigidPickUp != null)rigidPickUp.velocity = Vector3.zero;
        rigidPickUp = null;
        _hasArrived = false;
        //Por favor cambiame ya
        base.ReleaseFire();
    }

    public override void PressFire(Vector3 direction)
    {
        if (!_isCooling)
            Shoot(direction);
    }

    public override void ReleaseFire(Vector3 direction)
    {
        Debug.Log("release atracting hook");
        base.ReleaseFire(direction);
        if(_instance != null) Destroy(_instance.gameObject);
    }
}
