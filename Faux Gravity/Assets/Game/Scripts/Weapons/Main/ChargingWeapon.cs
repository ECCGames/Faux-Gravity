﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public abstract class ChargingWeapon : Weapon {

    public enum VibrationType { noVibration, vibratePerCharge, vibrateOnlyAtFull, AnimationCurve }
    public enum ChargeType { CoolOff, HeroToZero }

    [Space(5)]
    [Header("Charging Fire Properties")]
    [SerializeField] protected float _maxPower;
    [SerializeField] protected float _chargeRate;
    [SerializeField] protected VibrationType _vibrationType;
    [SerializeField] protected ChargeType _chargeType;
    [SerializeField] [Range(0f, 1f)] protected float _maxVibration;
    [SerializeField] protected AnimationCurve _curve;
    [SerializeField] protected AudioClip _pitchSound;
    [SerializeField] [Range(0.5f, 2)] protected float _maxPitchAmount;
    [SerializeField] protected AudioSource _pitchSource;

    protected float _currentPower;

    public virtual void Awake()
    {
        _pitchSource.playOnAwake = false;
        _pitchSource.loop = true;
        _pitchSource.clip = _pitchSound;
    }


    public override void ResetWeapon()
    {
        base.ResetWeapon();
        ResetCurrentPower();
    }

    /// <summary>
    /// returns currentPower if percentage is false, else returns current/Max power. 
    /// </summary>
    /// <param name="percentage"></param>
    /// <returns></returns>
    public float GetChargedPower(bool percentage = false)
    {
        if (!percentage) return _currentPower;
        else return _currentPower / _maxPower;
    }

    public void ResetCurrentPower()
    {
        _currentPower = 0;
        _pitchSource.Stop();
        StopVibration();
    }

    public virtual void ChargePower(int playerId)
    {
        //if (!_pitchSource.isPlaying) _pitchSource.Play();
        _currentPower = _currentPower + _chargeRate > _maxPower ? _maxPower : _currentPower + _chargeRate;
        //_mainAudioSource.pitch = 1 + GetChargedPower(true) * _maxPitchAmount;

        //si no es el teclado, activo la vibracion
        if (playerId != 0)
        {
            switch (_vibrationType)
            {
                case VibrationType.noVibration:
                    break;
                case VibrationType.vibratePerCharge:
                    GamePad.SetVibration((PlayerIndex)(playerId - 1),
                                         Mathf.Clamp(_currentPower / _maxPower, 0, _maxVibration),
                                         Mathf.Clamp(_currentPower / _maxPower, 0, _maxVibration));
                    break;
                case VibrationType.vibrateOnlyAtFull:
                    if (_currentPower >= _maxPower)
                        GamePad.SetVibration((PlayerIndex)(playerId - 1), _maxVibration, _maxVibration);
                    break;
                case VibrationType.AnimationCurve:
                    GamePad.SetVibration((PlayerIndex)(playerId - 1),
                                         _curve.Evaluate(Mathf.Clamp01(_currentPower / _maxPower)),
                                         _curve.Evaluate(Mathf.Clamp01(_currentPower / _maxPower)));
                    break;
                default:
                    break;
            }
        }
    }
    
    protected void StopVibration()
    {
        GamePad.SetVibration((PlayerIndex)(_player.Id - 1), 0, 0);
    }

    public override void PressFire(Vector3 direction)
    {
        if (Bullets == 0)
        {
            PlayClip(_noBulletsSound);
            _player.DisplayNoAmmo();
        }
        if (!_isCooling && Bullets > 0) ChargePower(_player.Id);
    }

    public override void ReleaseFire(Vector3 direction)
    {
        base.ReleaseFire(direction);
        Shoot(direction);
    }
}
