﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(TerrainGeneration))]
public class TerrainGenerationEditor : Editor {
    
    private void OnEnable()
    {

    }

    public override void OnInspectorGUI()
    {
        base.DrawDefaultInspector();
        TerrainGeneration generator = (TerrainGeneration)target;
        if(GUILayout.Button("Generar Plataforma"))
        {
            generator.GeneratePlatform();
            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }
    }
}
