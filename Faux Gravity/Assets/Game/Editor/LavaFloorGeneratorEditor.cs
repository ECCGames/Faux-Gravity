﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LavaFloorGenerator))]
public class LavaFloorGeneratorEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        LavaFloorGenerator generator =(LavaFloorGenerator) target;
        GUILayout.Label("", GUI.skin.horizontalSlider);
        if(GUILayout.Button("Generate Lava Floor"))generator.CreateLavaFloor();
    }
}
