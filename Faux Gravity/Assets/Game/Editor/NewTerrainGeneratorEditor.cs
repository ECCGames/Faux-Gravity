﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(NewTerrainGenerator))]
public class NewTerrainGeneratorEditor : Editor {

    int x = 5, y = 5;
    int [,] matrix = new int[200,200];
    public override void OnInspectorGUI()
    {
        NewTerrainGenerator generator = (NewTerrainGenerator)target;
        base.OnInspectorGUI();
        if (generator.Complexity == NewTerrainGenerator.TerrainGeneratorComplexity.Complex)
        {
            GUILayout.Space(5);

            GUILayout.BeginHorizontal("Box");
            GUILayout.Label("Width", EditorStyles.centeredGreyMiniLabel, GUILayout.Width(Screen.width / 4));
            x = EditorGUILayout.IntField(x, GUILayout.Width(Screen.width / 4));
            GUILayout.Label("Height", EditorStyles.centeredGreyMiniLabel, GUILayout.Width(Screen.width / 4));
            y = EditorGUILayout.IntField(y, GUILayout.Width(Screen.width / 4));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginVertical("Window");
            #region FirstFillMatrixButtons
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginHorizontal("Box");
            if (GUILayout.Button("1", GUILayout.Width(Screen.width / (x + 2))))
            {
                for (int k = 0; k < y; k++)
                {
                    matrix[0, k] = 1;
                }
                for (int l = 0; l < x; l++)
                {
                    matrix[l, 0] = 1;
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal("Box");
            for (int j = 0; j < x; j++)
            {
                if (j != -1 && j != x && GUILayout.Button("1", GUILayout.Width(Screen.width / (x + 2))))
                {
                    for (int k = 0; k < y; k++)
                    {
                        matrix[j, k] = 1;
                    }
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal("Box");
            if (GUILayout.Button("1", GUILayout.Width(Screen.width / (x + 2))))
            {
                for (int k = 0; k < y; k++)
                {
                    matrix[x - 1, k] = 1;
                }
                for (int l = 0; l < x; l++)
                {
                    matrix[l, 0] = 1;
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndHorizontal();
            #endregion
            for (int i = 0; i < y; i++)
            {
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("1"))
                {
                    for (int j = 0; j < x; j++)
                    {
                        matrix[j, i] = 1;
                    }
                }
                for (int j = 0; j < x; j++)
                {
                    matrix[j, i] = EditorGUILayout.IntField(matrix[j, i], GUILayout.Width(Screen.width / (x + 2)));
                }
                if (GUILayout.Button("0"))
                {
                    for (int j = 0; j < x; j++)
                    {
                        matrix[j, i] = 0;
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            #region SecondFillMatrixButtons
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginHorizontal("Box");
            if (GUILayout.Button("0", GUILayout.Width(Screen.width / (x + 2))))
            {
                for (int k = 0; k < y; k++)
                {
                    matrix[0, k] = 0;
                }
                for (int l = 0; l < x; l++)
                {
                    matrix[l, y-1] = 0;
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal("Box");
            for (int j = 0; j < x; j++)
            {
                if (j != -1 && j != x && GUILayout.Button("0", GUILayout.Width(Screen.width / (x + 2))))
                {
                    for (int k = 0; k < y; k++)
                    {
                        matrix[j, k] = 0;
                    }
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal("Box");
            if (GUILayout.Button("0", GUILayout.Width(Screen.width / (x + 2))))
            {
                for (int k = 0; k < y; k++)
                {
                    matrix[x - 1, k] = 0;
                }
                for (int l = 0; l < x; l++)
                {
                    matrix[l, y - 1] = 0;
                }
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndHorizontal();
            #endregion
            EditorGUILayout.EndVertical();

            if(GUILayout.Button("Generar Nuevo Terreno Complejo"))
            {
                generator.GeneratePlatform(matrix);
                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
            }
        }
        if (generator.Complexity == NewTerrainGenerator.TerrainGeneratorComplexity.Complex2)
        {
            GUILayout.BeginHorizontal("Box");
            GUILayout.Label("Width", EditorStyles.centeredGreyMiniLabel, GUILayout.Width(Screen.width / 5));
            x = EditorGUILayout.IntField(x, GUILayout.Width(Screen.width / 5));
            GUILayout.Label("Height", EditorStyles.centeredGreyMiniLabel, GUILayout.Width(Screen.width / 5));
            y = EditorGUILayout.IntField(y, GUILayout.Width(Screen.width / 5));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginVertical("Box");
            for (int i = 0; i < y; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.BeginHorizontal("Box");
                for (int j = 0; j < x; j++)
                {
                    matrix[j, i] = EditorGUILayout.IntField(matrix[j, i], GUILayout.Width(Screen.width / (x + 2)));
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal("Box");
                EditorGUILayout.LabelField("X Times", GUILayout.Width(Screen.width / (x + 1)));
                matrix[x, i] = EditorGUILayout.IntField(matrix[x, i], GUILayout.Width(Screen.width / (x + 1)));
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();

            if (GUILayout.Button("Generar Nuevo Terreno Complejo"))
            {
                for (int i = 0; i < y; i++)
                {
                    for (int j = 0; j < x +1; j++)
                    {
                        Debug.Log(matrix[j, i]);
                    }
                }
                generator.GeneratePlatform(matrix);
                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
            }
        }
        if (generator.Complexity == NewTerrainGenerator.TerrainGeneratorComplexity.Simple)
        {
            if (GUILayout.Button("Generar Nuevo Terreno Simple"))
            {
                generator.GeneratePlatform();
                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
            }
        }
    }
}
