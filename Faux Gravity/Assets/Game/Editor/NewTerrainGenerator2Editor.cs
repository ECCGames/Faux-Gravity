﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NewTerrainGenerator2))]
public class NewTerrainGenerator2Editor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        NewTerrainGenerator2 aux = (NewTerrainGenerator2)target;
        GUILayout.Label("", GUI.skin.horizontalSlider);
        if (GUILayout.Button("Generar Plataforma"))
            aux.GenerateTerrain();
    }
}
